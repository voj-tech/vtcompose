CREATE TABLE web_request_handlers (
	id serial,
	path_regex text NOT NULL,
	http_method http_method,
	host text,
	rank integer,
	class_name text NOT NULL,
	
	PRIMARY KEY (id),
	CHECK (host ~ '^(?!-)[-0-9A-Za-z]{0,62}[0-9A-Za-z](?:\.(?!-)[-0-9A-Za-z]{0,62}[0-9A-Za-z])*$'),
	CHECK (class_name ~ '^[^\\]+(?:\\[^\\]+)*$')
);

CREATE UNIQUE INDEX web_request_handlers_path_regex_http_method_host_idx
	ON web_request_handlers (path_regex, http_method, host)
	WHERE http_method IS NOT NULL AND host IS NOT NULL;

CREATE UNIQUE INDEX web_request_handlers_path_regex_http_method_idx
	ON web_request_handlers (path_regex, http_method)
	WHERE http_method IS NOT NULL AND host IS NULL;

CREATE UNIQUE INDEX web_request_handlers_path_regex_host_idx
	ON web_request_handlers (path_regex, host)
	WHERE http_method IS NULL AND host IS NOT NULL;

CREATE UNIQUE INDEX web_request_handlers_path_regex_idx
	ON web_request_handlers (path_regex)
	WHERE http_method IS NULL AND host IS NULL;
