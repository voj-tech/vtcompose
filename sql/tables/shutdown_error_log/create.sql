CREATE TABLE shutdown_error_log (
	id serial,
	message text NOT NULL,
	severity integer NOT NULL,
	filename text NOT NULL,
	line_number integer NOT NULL,
	time timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
	
	PRIMARY KEY (id),
	CHECK (isfinite(time))
);
