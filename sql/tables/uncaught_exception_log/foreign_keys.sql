ALTER TABLE uncaught_exception_log ADD CONSTRAINT uncaught_exception_log_inner_exception_id_fkey
	FOREIGN KEY (inner_exception_id) REFERENCES uncaught_exception_log (id);
