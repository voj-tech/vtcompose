CREATE TABLE uncaught_exception_log (
	id serial,
	class_name text NOT NULL,
	message text NOT NULL,
	code text NOT NULL,
	filename text NOT NULL,
	line_number integer NOT NULL,
	stack_trace text NOT NULL,
	inner_exception_id integer,
	time timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
	
	PRIMARY KEY (id),
	CHECK (class_name ~ '^[^\\]+(?:\\[^\\]+)*$'),
	CHECK (isfinite(time))
);
