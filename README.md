vtCompose
=========

Synopsis
--------

vtCompose is a low-level PHP framework which wraps around the PHP programming API providing more coherent and
object oriented API.

Code Example
------------

```php
<?php

require_once 'VTCompose/Autoloading/Autoloader.php';

use VTCompose\Autoloading\Autoloader;
use VTCompose\Http\Request;
use VTCompose\Http\Response;
use VTCompose\Http\StatusCode;
use VTCompose\Web\Application;
use VTCompose\Web\Context;
use VTCompose\Web\IRequestHandler;
use VTCompose\Web\IRequestHandlerFactory;

(new Autoloader())->register();

class MyRequestHandler implements IRequestHandler {
	public function handle(Request $request, Context $context) {
		$response = new Response(StatusCode::OK);
		$response->setBody(<<<HTML
<!DOCTYPE html>
<head>
	<meta charset=UTF-8>
	<title>My Page</title>
</head>
<body>
	<h1>Hello World!</h1>
	<p>Request URI: {$request->getUri()}</p>
</body>
HTML
			);
		return $response;
	}
}

class MyRequestHandlerFactory implements IRequestHandlerFactory {
	public function createHandler($path, $httpMethod, $host) {
		// return an instance of MyRequestHandler for any HTTP request URI path, HTTP method and host
		return new MyRequestHandler();
	}
}

// run the application, i.e. process the web request
(new Application(new MyRequestHandlerFactory()))->run();

?>
```

Motivation
----------

With every new version PHP is becoming more and more type-safe. Programmers can write type-safe and object
oriented code with PHP these days however they are forced to use the same old, incoherent and mostly
procedural programming API. vtCompose is meant to tackle this issue.

vtCompose was written over the course of developing [www.voj-tech.net](http://www.voj-tech.net/) with the aim
of having any direct PHP function call or superglobal variable access wrapped within the object oriented layer
of the framework. When designing the class model inspiration was taken from the .NET Framework and Zend
Framework. By no means vtCompose is a complete PHP framework and it should be mentioned there are certain
inconsistencies in its API as well. It is merely a proof of concept showing what it takes to write robust and
relatively type-safe PHP code in a way that resembles writing code against a proper systematic API such as the
.NET programming API.

Installation
------------

### Prerequisites

The supported PHP version is 8.1.

Depending on which features of vtCompose you use there will be a set of required PHP extensions. The complete
list of PHP extensions vtCompose might currently require is as follows:

- date
- dom
- hash
- libxml
- mbstring
- pcre
- pgsql
- Reflection
- SPL
- xsl

Note that by default these extensions are part of the PHP core except for dom, libxml, mbstring, pgsql and
xsl.

vtCompose does not have any other dependencies and sits directly on top of PHP.

There are a few notable `php.ini` directives which impact a vtCompose installation and its behaviour.

- `error_reporting:` This directive is particularly relevant when an instance of the
  `VTCompose\ErrorHandling\ErrorHandler` class is registered to turn PHP errors into exceptions. It is
  advisable to set the directive to `E_ALL` for development purposes and to `E_ALL & ~E_DEPRECATED &
  ~E_STRICT` in production environments. See [Error
  Handling](http://www.voj-tech.net/vtcompose/error-handling/) for more information.
- `output_buffering:` If you intend to use the vtCompose built-in implementation of the
  `VTCompose\ErrorHandling\IExceptionHandlerImplementor` and
  `VTCompose\ErrorHandling\IShutdownErrorHandlerImplementor` interfaces which is the
  `VTCompose\Sample\ErrorHandling\Implementor` class you should make sure this directive is set to `Off` for
  the custom error page to appear as expected. Again, see [Error
  Handling](http://www.voj-tech.net/vtcompose/error-handling/) for more information.
- `display_errors:` Be aware that if this directive is set to `'1'` the PHP error output might interfere with
  your application output. Enabling this feature is highly discouraged, certainly in production environments.
- `include_path:` This directive needs to list a directory where vtCompose classes can be found. The directory
  structure underneath this directory is expected to reflect the vtCompose namespace structure. The
  installation steps below go into more details.

### Installation Steps

The following installation steps assume an Unix-like environment however you should be able to use vtCompose
in any environment capable of running PHP.

1. Download a ZIP file with the required version of vtCompose from [the project
   page](http://www.voj-tech.net/vtcompose/).
   
       wget http://www.voj-tech.net/data/content/projects/vtcompose-0.3.0.zip
   
2. Unpack the downloaded file.

       unzip vtcompose-0.3.0.zip

3. Optionally remove files or directories from within the `php/VTCompose` directory which contains vtCompose
   classes. You can do this if you do not want to install the complete vtCompose framework but instead you
   want to use only a subset of the classes. Note that there are dependencies between vtCompose classes and
   namespaces. These dependencies are undocumented and so this might be a rather risky thing to do.
4. Copy the `php/VTCompose` directory to a directory listed in the `include_path` `php.ini` directive. You
   might have previously created the `/usr/local/share/vtcompose/php` directory for example and added it to
   the `include_path` directive for this purpose. Make sure this is done as a user account allowed to write to
   the destination directory.

       cp -R vtcompose-0.3.0/php/VTCompose /usr/local/share/vtcompose/php

API Reference
-------------

There is currently no API reference however the codebase is not too large or cluttered so feel free to explore
it.

For the most basic usage please refer to the code example above. Some of the significant features of vtCompose
are then explained on examples on the following pages:

- **[Collections](http://www.voj-tech.net/vtcompose/collections/):** Shows how collections are created and
  used including operations resembling LINQ in .NET Framework.
- **[Data Access](http://www.voj-tech.net/vtcompose/data-access/):** vtCompose features a set of classes
  providing an interface to access relational databases. The way to access and modify data in vtCompose is
  much inspired by ADO.NET.
- **[Markdown](http://www.voj-tech.net/vtcompose/markdown/):** On a step-by-step example this article
  demonstrates how to use the vtCompose built-in Markdown parser on your website.
- **[Error Handling](http://www.voj-tech.net/vtcompose/error-handling/):** vtCompose provides a robust
  interface for the programmer to use the built-in error and exception handlers or hook any custom error
  handling routines if desired.
					
vtCompose currently produces all output encoded in UTF-8. Similarly it assumes any input is encoded in UTF-8.

Tests
-----

There are currently no automated tests for vtCompose. This is not the best situation to be in and hopefully
this will change in near future.

Authors
-------

vtCompose is written by Vojtěch Sobota ([www.voj-tech.net](http://www.voj-tech.net/)). Suggestions and bug
reports are more than welcome at <voj-tech@voj-tech.net>.

License
-------

vtCompose is licensed under the MIT License. See the `LICENSE` file for details.
