<?php

namespace VTCompose\Markdown\Parser;

use VTCompose\Markdown\Document\Inline;

/**
 * @internal
 */
class LinkedInline {
	
	private $inline;
	
	private $next;
	private $previous;
	
	public function __construct(Inline $inline) {
		$this->inline = $inline;
	}
	
	public function getInline() {
		return $this->inline;
	}
	
	public function setNext(LinkedInline $next = NULL) {
		$this->next = $next;
	}
	
	public function getNext() {
		return $this->next;
	}
	
	public function setPrevious(LinkedInline $previous = NULL) {
		$this->previous = $previous;
	}
	
	public function getPrevious() {
		return $this->previous;
	}

}

?>
