<?php

namespace VTCompose\Markdown\Parser;

use VTCompose\Exception\InvalidOperationException;
use VTCompose\StringUtilities as StringUtils;

/**
 * @internal
 */
class EmphasisDelimiter {

	private $linkedText;
	private $potentialOpener;
	private $potentialCloser;
	
	private $next;
	private $previous;
	
	public function __construct(LinkedInline $linkedText, $potentialOpener, $potentialCloser) {
		$this->linkedText = $linkedText;
		$this->potentialOpener = $potentialOpener;
		$this->potentialCloser = $potentialCloser;
	}
	
	public function getLinkedText() {
		return $this->linkedText;
	}
	
	public function getMark() {
		$content = $this->linkedText->getInline()->getContent();
		if (StringUtils::length($content) == 0) {
			throw new InvalidOperationException('The associated Text node content is empty.');
		}
		
		return $content[0];
	}
	
	public function getLength() {
		return StringUtils::length($this->linkedText->getInline()->getContent());
	}
	
	public function canOpen() {
		return $this->potentialOpener;
	}
	
	public function canClose() {
		return $this->potentialCloser;
	}
	
	public function setNext(EmphasisDelimiter $next = NULL) {
		$this->next = $next;
	}
	
	public function getNext() {
		return $this->next;
	}
	
	public function setPrevious(EmphasisDelimiter $previous = NULL) {
		$this->previous = $previous;
	}
	
	public function getPrevious() {
		return $this->previous;
	}

}

?>
