<?php

namespace VTCompose\Markdown\Parser;

use VTCompose\Exception\InvalidOperationException;
use VTCompose\StringUtilities as StringUtils;

/**
 * @internal
 */
class LinkStart {
	
	private $linkedText;
	private $labelIndex;
	private $lastEmphasisDelimiter;
	
	private $active;
	private $last;
	
	private $previous;
	
	public function __construct(LinkedInline $linkedText, $labelIndex,
			EmphasisDelimiter $lastEmphasisDelimiter = NULL, LinkStart $previous = NULL) {
		$this->linkedText = $linkedText;
		$this->labelIndex = $labelIndex;
		$this->lastEmphasisDelimiter = $lastEmphasisDelimiter;
		
		$this->active = $this->last = true;
		
		$this->previous = $previous;
	}
	
	public function getLinkedText() {
		return $this->linkedText;
	}
	
	public function isImageStart() {
		$content = $this->linkedText->getInline()->getContent();
		if (StringUtils::length($content) == 0) {
			throw new InvalidOperationException('The associated Text node content is empty.');
		}
		
		return $content[0] == '!';
	}
	
	public function getLabelIndex() {
		return $this->labelIndex;
	}
	
	public function getLastEmphasisDelimiter() {
		return $this->lastEmphasisDelimiter;
	}
	
	public function setActive($active) {
		$this->active = $active;
	}
	
	public function isActive() {
		return $this->active;
	}
	
	public function setLast($last) {
		$this->last = $last;
	}
	
	public function isLast() {
		return $this->last;
	}
	
	public function getPrevious() {
		return $this->previous;
	}

}

?>
