<?php

namespace VTCompose\Markdown\Parser;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;
use VTCompose\Markdown\Document\Block;
use VTCompose\StringUtilities as StringUtils;

/**
 * @internal
 */
class Context {
	
	private $blocks;
	private $blocksTight;
	
	private $blockType;
	private $block;
	private $blockNumConsecutiveBlankLines;
	
	private $line;
	private $numLeadingSpaces;
	private $text;
	
	private $blockInlineText;
	private $numBlockInlineTextChunks;
	private $blockLines;
	private $blockMark;
	private $blockNumLeadingSpaces;
	private $blockClosed;
	private $blockAttribute;
	
	public function __construct() {
		$this->blocks = new ArrayList();
		$this->blocksTight = true;
		
		$this->blockNumConsecutiveBlankLines = 0;
	}
	
	public function changeBlock($blockType, Block $block = NULL) {
		$this->blockType = $blockType;
		$this->block = $block;
		$this->blockNumConsecutiveBlankLines = 0;
	}
	
	public function pushBlock() {
		$this->blocks->add($this->block);
	}
	
	public function getBlocks() {
		return $this->blocks;
	}
	
	public function setBlocksTight($blocksTight) {
		$this->blocksTight = $blocksTight;
	}
	
	public function areBlocksTight() {
		return $this->blocksTight;
	}
	
	public function getBlockType() {
		return $this->blockType;
	}
	
	public function getBlock() {
		return $this->block;
	}
	
	public function incrementBlockNumConsecutiveBlankLines() {
		$this->blockNumConsecutiveBlankLines++;
	}
	
	public function resetBlockNumConsecutiveBlankLines() {
		$this->blockNumConsecutiveBlankLines = 0;
	}
	
	public function getBlockNumConsecutiveBlankLines() {
		return $this->blockNumConsecutiveBlankLines;
	}
	
	public function isBlockInterrupted() {
		return $this->blockNumConsecutiveBlankLines > 0;
	}
	
	public function setLine($line) {
		$this->line = $line;
		$this->numLeadingSpaces = self::countLeadingSpaces($line);
		$this->text = StringUtils::substring($line, $this->numLeadingSpaces);
	}
	
	private static function countLeadingSpaces($string) {
		$numLeadingSpaces = 0;
		$stringLength = StringUtils::length($string);
		while ($numLeadingSpaces < $stringLength && $string[$numLeadingSpaces] == StringUtils::SPACE) {
			$numLeadingSpaces++;
		}
		return $numLeadingSpaces;
	}
	
	public function getLine() {
		return $this->line;
	}
	
	public function getNumLeadingSpaces() {
		return $this->numLeadingSpaces;
	}
	
	public function getText() {
		return $this->text;
	}
	
	public function setBlockInlineText($blockInlineText) {
		$this->blockInlineText = $blockInlineText;
		$this->numBlockInlineTextChunks = 1;
	}
	
	public function appendBlockInlineText($blockInlineText) {
		$this->blockInlineText .= $blockInlineText;
		$this->numBlockInlineTextChunks++;
	}
	
	public function getBlockInlineText() {
		return $this->blockInlineText;
	}
	
	public function getNumBlockInlineTextChunks() {
		return $this->numBlockInlineTextChunks;
	}
	
	public function setBlockLines(IList $blockLines) {
		$this->blockLines = $blockLines;
	}
	
	public function getBlockLines() {
		return $this->blockLines;
	}
	
	public function setBlockMark($blockMark) {
		$this->blockMark = $blockMark;
	}
	
	public function getBlockMark() {
		return $this->blockMark;
	}
	
	public function setBlockNumLeadingSpaces($blockNumLeadingSpaces) {
		$this->blockNumLeadingSpaces = $blockNumLeadingSpaces;
	}
	
	public function getBlockNumLeadingSpaces() {
		return $this->blockNumLeadingSpaces;
	}
	
	public function setBlockClosed($blockClosed) {
		$this->blockClosed = $blockClosed;
	}
	
	public function isBlockClosed() {
		return $this->blockClosed;
	}
	
	public function setBlockAttribute($blockAttribute) {
		$this->blockAttribute = $blockAttribute;
	}
	
	public function getBlockAttribute() {
		return $this->blockAttribute;
	}

}

?>
