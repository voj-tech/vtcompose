<?php

namespace VTCompose\Markdown\Parser;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\Dictionary;
use VTCompose\Collection\IList;
use VTCompose\Collection\Set;
use VTCompose\Markdown\Document\Block;
use VTCompose\Markdown\Document\BlockList;
use VTCompose\Markdown\Document\BlockQuote;
use VTCompose\Markdown\Document\CodeBlock;
use VTCompose\Markdown\Document\CodeSpan;
use VTCompose\Markdown\Document\Document;
use VTCompose\Markdown\Document\Emphasis;
use VTCompose\Markdown\Document\HardLineBreak;
use VTCompose\Markdown\Document\Heading;
use VTCompose\Markdown\Document\HtmlBlock;
use VTCompose\Markdown\Document\HtmlInline;
use VTCompose\Markdown\Document\Image;
use VTCompose\Markdown\Document\Link;
use VTCompose\Markdown\Document\ListItem;
use VTCompose\Markdown\Document\NodeType;
use VTCompose\Markdown\Document\Paragraph;
use VTCompose\Markdown\Document\SoftLineBreak;
use VTCompose\Markdown\Document\StrongEmphasis;
use VTCompose\Markdown\Document\Table;
use VTCompose\Markdown\Document\TableColumn;
use VTCompose\Markdown\Document\TableRow;
use VTCompose\Markdown\Document\Text;
use VTCompose\Markdown\Document\TextAlignment;
use VTCompose\Markdown\Document\ThematicBreak;
use VTCompose\StringUtilities as StringUtils;
use VTCompose\Text\RegularExpressions\Regex;
use VTCompose\Text\RegularExpressions\RegexOptions;

/**
 * 
 *
 * 
 */
class Parser {

	private static $codeBlockNumLeadingSpaces = 4;
	
	private $linkReferences;
	private $inlineTexts;
	
	private $inlineContentHead;
	private $inlineText;
	private $inlineTextPosition;
	private $remainingInlineText;
	private $emphasisDelimitersHead;
	private $linkStartsTail;
	
	/**
	 * 
	 *
	 * 
	 */
	public function __construct() {
		$this->linkReferences = new Dictionary();
		$this->inlineTexts = new Dictionary();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Document 
	 */
	public function parse($string) {
		$this->linkReferences->clear();
		$this->inlineTexts->clear();
		
		$lines = new ArrayList();
		$string = str_replace([StringUtils::CRLF, StringUtils::CR], StringUtils::LF, $string);
		foreach (explode(StringUtils::LF, trim($string, StringUtils::LF)) as $line) {
			$lines->add(self::replaceTabsWithSpaces($line));
		}

		$document = new Document($this->createBlocksFromLines($lines));

		foreach ($this->inlineTexts as $inlineContent => $inlineText) {
			$linkedInline = $this->createInlineContentFromInlineText($inlineText);
			while ($linkedInline != NULL) {
				$inlineContent->add($linkedInline->getInline());
				$linkedInline = $linkedInline->getNext();
			}
		}

		return $document;
	}
	
	private static function replaceTabsWithSpaces($string, $tabSize = 4) {
		$stringParts = explode("\t", $string);
		$string = array_shift($stringParts);
		foreach ($stringParts as $stringPart) {
			$numSpacesToAdd = $tabSize - StringUtils::length($string) % $tabSize;
			$string .= str_repeat(StringUtils::SPACE, $numSpacesToAdd) . $stringPart;
		}
		return $string;
	}
	
	private function createBlocksFromLines(IList $lines, &$blocksAreTight = true) {
		$context = new Context();
		
		foreach ($lines as $line) {
			$blockType = $context->getBlockType();
		
			if (StringUtils::length(trim($line)) == 0) {
				if ($blockType !== NULL) {
					$context->incrementBlockNumConsecutiveBlankLines();
				}
				
				continue;
			}
			
			$context->setLine($line);
			
			if ($blockType !== NULL && $this->continueBlock($context)) {
				continue;
			}
			
			if ($this->startBlock($context)) {
				continue;
			}
			
			if ($blockType === BlockType::PARAGRAPH && $this->continueParagraph($context)) {
				continue;
			}
			
			$this->startParagraph($context);
		}
		
		$this->pushBlock($context);
		$blocksAreTight = $context->areBlocksTight();
		return $context->getBlocks();
	}
	
	/**
	 * @todo Once there's an addRange() method in the IList interface use ArrayList for block types.
	 */
	private function startBlock(Context $context) {
		$blockTypes = [BlockType::CODE_BLOCK];
		$blockMark = $context->getText()[0];
		$blockMarkToBlockTypesMap = self::getBlockMarkToBlockTypesMap();
		if (isset($blockMarkToBlockTypesMap[$blockMark])) {
			$blockTypes = array_merge($blockTypes, $blockMarkToBlockTypesMap[$blockMark]);
		}
		
		foreach ($blockTypes as $blockType) {
			if ($this->startBlockOfType($context, $blockType)) {
				return true;
			}
		}
		
		return false;
	}
	
	private function startBlockOfType(Context $context, $blockType) {
		switch ($blockType) {
		case BlockType::HEADING:
			return $this->startHeading($context);
			
		case BlockType::UNDERLINE_HEADING:
			return $this->startUnderlineHeading($context);
			
		case BlockType::BLOCK_LIST:
			return $this->startList($context);
			
		case BlockType::CODE_BLOCK:
			return $this->startCodeBlock($context);
			
		case BlockType::FENCED_CODE_BLOCK:
			return $this->startFencedCodeBlock($context);
			
		case BlockType::TABLE:
			return $this->startTable($context);
			
		case BlockType::BLOCK_QUOTE:
			return $this->startBlockQuote($context);
			
		case BlockType::THEMATIC_BREAK:
			return $this->startThematicBreak($context);
			
		case BlockType::LINK_REFERENCE:
			return $this->startLinkReference($context);
			
		case BlockType::HTML_BLOCK:
			return $this->startHtmlBlock($context);
		
		default:
			return false;
		}
	}
	
	private function continueBlock(Context $context) {
		switch ($context->getBlockType()) {
		case BlockType::BLOCK_LIST:
			return $this->continueList($context);
			
		case BlockType::CODE_BLOCK:
			return $this->continueCodeBlock($context);
			
		case BlockType::FENCED_CODE_BLOCK:
			return $this->continueFencedCodeBlock($context);
			
		case BlockType::TABLE:
			return $this->continueTable($context);
			
		case BlockType::BLOCK_QUOTE:
			return $this->continueBlockQuote($context);
			
		case BlockType::HTML_BLOCK:
			return $this->continueHtmlBlock($context);
			
		default:
			return false;
		}
	}
	
	private function startParagraph(Context $context) {
		$this->pushAndChangeBlock($context, BlockType::PARAGRAPH, new Paragraph());
		$context->setBlockInlineText($context->getText());
		
		return true;
	}
	
	private function continueParagraph(Context $context) {
		if ($context->isBlockInterrupted()) {
			return false;
		}
		
		$context->appendBlockInlineText(StringUtils::LF . $context->getText());
		
		return true;
	}
	
	private function startHeading(Context $context) {
		$match = self::getHeadingRegex()->match($context->getText());
		if (!$match->isSuccess()) {
			return false;
		}
		
		$groups = $match->getGroups();
		$heading = new Heading(StringUtils::length($groups[1]->getValue()));
		$this->pushAndChangeBlock($context, BlockType::HEADING, $heading);
		$context->setBlockInlineText($groups->count() > 2 ? $groups[2]->getValue() : '');
		
		return true;
	}
	
	private static function getHeadingRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(#{1,6})(?: +(.+?))??(?: +#+)? *\\z', RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private function startUnderlineHeading(Context $context) {
		$blockType = $context->getBlockType();
		if ($blockType === NULL || $blockType != BlockType::PARAGRAPH || $context->isBlockInterrupted()) {
			return false;
		}
		
		$text = $context->getText();
		$blockMark = $text[0];
		
		if (StringUtils::length(trim($text, $blockMark)) > 0) {
			return false;
		}
		
		$level = $blockMark == '=' ? 1 : 2;
		$context->changeBlock(BlockType::HEADING, new Heading($level));
		
		return true;
	}
	
	private function startList(Context $context) {
		if (!$this->parseFirstListItemLine($context,
				$ordered, $startNumber, $blockMark, $blockLine, $blockNumLeadingSpaces)) {
			return false;
		}
		
		if (!$context->isBlockInterrupted() && $context->getBlockType() === BlockType::PARAGRAPH) {
			if ($startNumber != 1 || StringUtils::length($blockLine) == 0) {
				return false;
			}
		}
		
		$tight = true;
		$blockList = new BlockList(new ArrayList([new ListItem()]), $ordered, $startNumber, $tight);
		$this->pushAndChangeBlock($context, BlockType::BLOCK_LIST, $blockList);
		$context->setBlockLines(new ArrayList([$blockLine]));
		$context->setBlockMark($blockMark);
		$context->setBlockNumLeadingSpaces($blockNumLeadingSpaces);
		
		return true;
	}
	
	private function continueList(Context $context) {
		$numLeadingSpaces = $context->getNumLeadingSpaces();
		$blockNumLeadingSpaces = $context->getBlockNumLeadingSpaces();
		
		if ($numLeadingSpaces < $blockNumLeadingSpaces) {
			if ($this->parseFirstListItemLine($context,
					$ordered, $startNumber, $blockMark, $blockLine, $newBlockNumLeadingSpaces)) {
				if ($blockMark != $context->getBlockMark()) {
					return false;
				}
				
				$this->processListItem($context);
				$blockList = $context->getBlock();
				$blockList->getItems()->add(new ListItem());
				if ($context->isBlockInterrupted()) {
					$blockList->setTight(false);
					$context->resetBlockNumConsecutiveBlankLines();
				}
				$blockLines = $context->getBlockLines();
				$blockLines->clear();
				$blockLines->add($blockLine);
				$context->setBlockNumLeadingSpaces($newBlockNumLeadingSpaces);
				
				return true;
			}
		}
		
		if (!$context->isBlockInterrupted() || $numLeadingSpaces >= $blockNumLeadingSpaces) {
			$blockLines = $context->getBlockLines();
			
			$blockNumConsecutiveBlankLines = $context->getBlockNumConsecutiveBlankLines();
			for ($i = 0; $i < $blockNumConsecutiveBlankLines; $i++) {
				$blockLines->add('');
			}
			$context->resetBlockNumConsecutiveBlankLines();
			
			if ($blockNumLeadingSpaces < $numLeadingSpaces) {
				$text = StringUtils::substring($context->getLine(), $blockNumLeadingSpaces);
			} else {
				$text = $context->getText();
			}
			
			$blockLines->add($text);
		
			return true;
		}
		
		return false;
	}
	
	private function processListItem(Context $context) {
		$blockList = $context->getBlock();
		$blocks = $this->createBlocksFromLines($context->getBlockLines(), $blocksAreTight);
		$blockList->getItems()->last()->setBlocks($blocks);
		if (!$blocksAreTight) {
			$blockList->setTight($blocksAreTight);
		}
	}
	
	private function parseFirstListItemLine(Context $context,
			&$ordered, &$startNumber, &$blockMark, &$blockLine, &$blockNumLeadingSpaces) {
		$firstListItemLine = $context->getText();
		$match = self::getFirstListItemLineRegex()->match($firstListItemLine);
		if (!$match->isSuccess()) {
			return false;
		}
		
		$groups = $match->getGroups();
		
		if ($groups[1]->getIndex() >= 0) {
			$ordered = true;
			$startNumber = (int) $groups[1]->getValue();
			$blockMarkGroup = $groups[2];
		} else {
			$ordered = false;
			$startNumber = 1;
			$blockMarkGroup = $groups[3];
		}
		
		$blockMark = $blockMarkGroup->getValue();
		
		$blockLine = '';
		$numSpacesFollowingListMark = 0;
		if ($groups->count() > 5) {
			$blockLine = $groups[5]->getValue();
			if (StringUtils::length($blockLine) > 0) {
				$numSpacesFollowingListMark = StringUtils::length($groups[4]->getValue());
				if ($numSpacesFollowingListMark > self::$codeBlockNumLeadingSpaces) {
					$blockLine = StringUtils::substring($firstListItemLine, $groups[4]->getIndex() + 1);
				}
			}
		}
		
		$codeBlockNumLeadingSpaces = self::$codeBlockNumLeadingSpaces;
		if ($numSpacesFollowingListMark == 0 || $numSpacesFollowingListMark > $codeBlockNumLeadingSpaces) {
			$blockNumLeadingSpaces = $blockMarkGroup->getIndex() + 2;
		} else {
			$blockNumLeadingSpaces = $groups[5]->getIndex();
		}
		$blockNumLeadingSpaces += $context->getNumLeadingSpaces();
		
		return true;
	}
	
	private static function getFirstListItemLineRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$pattern = '^(?:([0-9]{1,9})(\\)|\\.)|([*+-]))(?:( +)(.*))?\\z';
			$regex = new Regex($pattern, RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private function startCodeBlock(Context $context) {
		if ($context->getBlockType() === BlockType::PARAGRAPH && !$context->isBlockInterrupted()) {
			return false;
		}
		
		if ($context->getNumLeadingSpaces() < self::$codeBlockNumLeadingSpaces) {
			return false;
		}
		
		$codeBlockNumLeadingSpaces = self::$codeBlockNumLeadingSpaces;
		$codeText = StringUtils::substring($context->getLine(), $codeBlockNumLeadingSpaces) . StringUtils::LF;
		$this->pushAndChangeBlock($context, BlockType::CODE_BLOCK, new CodeBlock($codeText));
		
		return true;
	}
	
	private function continueCodeBlock(Context $context) {
		if ($context->getNumLeadingSpaces() < self::$codeBlockNumLeadingSpaces) {
			return false;
		}
		
		$codeText = '';
		$blockNumConsecutiveBlankLines = $context->getBlockNumConsecutiveBlankLines();
		for ($i = 0; $i < $blockNumConsecutiveBlankLines; $i++) {
			$codeText .= StringUtils::LF;
		}
		$context->resetBlockNumConsecutiveBlankLines();
		$codeText .= StringUtils::substring($context->getLine(), self::$codeBlockNumLeadingSpaces);
		$codeText .= StringUtils::LF;
		
		$context->getBlock()->appendCodeText($codeText);
	
		return true;
	}
	
	private function startFencedCodeBlock(Context $context) {
		$match = self::getCodeFenceRegex()->match($context->getText());
		if (!$match->isSuccess()) {
			return false;
		}
		
		$groups = $match->getGroups();
		$fenceGroupIndex = $groups[1]->getIndex() >= 0 ? 1 : 3;
		$infoGroupIndex = $fenceGroupIndex + 1;
		$codeText = '';
		$info = $groups->count() > $infoGroupIndex ? $groups[$infoGroupIndex]->getValue() : NULL;
		$this->pushAndChangeBlock($context, BlockType::FENCED_CODE_BLOCK, new CodeBlock($codeText, $info));
		$fence = $groups[$fenceGroupIndex]->getValue();
		$context->setBlockMark($fence[0]);
		$context->setBlockNumLeadingSpaces($context->getNumLeadingSpaces());
		$context->setBlockClosed(false);
		$context->setBlockAttribute(StringUtils::length($fence));
		
		return true;
	}
	
	private function continueFencedCodeBlock(Context $context) {
		if ($context->isBlockClosed()) {
			return false;
		}
		
		$block = $context->getBlock();
		
		$blockNumConsecutiveBlankLines = $context->getBlockNumConsecutiveBlankLines();
		for ($i = 0; $i < $blockNumConsecutiveBlankLines; $i++) {
			$block->appendCodeText(StringUtils::LF);
		}
		$context->resetBlockNumConsecutiveBlankLines();
		
		$text = $context->getText();
		$numLeadingSpaces = $context->getNumLeadingSpaces();
		
		if ($numLeadingSpaces < self::$codeBlockNumLeadingSpaces) {
			$match = self::getCodeFenceRegex()->match($text);
			if ($match->isSuccess()) {
				$groups = $match->getGroups();
				$fenceGroupIndex = $groups[1]->getIndex() >= 0 ? 1 : 3;
				if ($groups->count() == $fenceGroupIndex + 1) {
					$fence = $groups[$fenceGroupIndex]->getValue();
					if ($fence[0] == $context->getBlockMark() &&
							StringUtils::length($fence) >= $context->getBlockAttribute()) {
						$context->setBlockClosed(true);
					
						return true;
					}
				}
			}
		}
		
		$blockNumLeadingSpaces = $context->getBlockNumLeadingSpaces();
		if ($blockNumLeadingSpaces < $numLeadingSpaces) {
			$text = StringUtils::substring($context->getLine(), $blockNumLeadingSpaces);
		}
		
		$block->appendCodeText($text . StringUtils::LF);
		
		return true;
	}
	
	private static function getCodeFenceRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(?:(`{3,}) *([^`]+?)?|(~{3,}) *(.+?)?) *\\z', RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private function startTable(Context $context) {
		$blockType = $context->getBlockType();
		if ($blockType === NULL || $blockType != BlockType::PARAGRAPH || $context->isBlockInterrupted()) {
			return false;
		}
		
		if ($context->getNumBlockInlineTextChunks() > 1) {
			return false;
		}
		
		$text = $context->getText();
		if (!self::getTableHeaderSeparatorRegex()->match($text)->isSuccess()) {
			return false;
		}
		
		$matches = self::getTableHeaderSeparatorColumnRegex()->matches($text);
		$blockMark = $text[0];
		$numTableColumns = $matches->count();
		
		$tableHeaderText = $context->getBlockInlineText();
		if (!$this->parseTableRowText($tableHeaderText, $numTableColumns, $blockMark, $cellInlineContents)) {
			return false;
		}
		
		$columns = new ArrayList();
		foreach ($matches as $i => $match) {
			$groups = $match->getGroups();
			$leftGroupValueLength = StringUtils::length($groups[1]->getValue());
			$rightGroupValueLength = StringUtils::length($groups[2]->getValue());
			if ($leftGroupValueLength > 0 && $rightGroupValueLength > 0) {
				$textAlignment = TextAlignment::CENTER;
			} elseif ($leftGroupValueLength > 0) {
				$textAlignment = TextAlignment::LEFT;
			} elseif ($rightGroupValueLength > 0) {
				$textAlignment = TextAlignment::RIGHT;
			} else {
				$textAlignment = TextAlignment::NONE;
			}
			$columns->add(new TableColumn($cellInlineContents[$i], $textAlignment));
		}
		
		$rows = new ArrayList();
		$context->changeBlock(BlockType::TABLE, new Table($columns, $rows));
		$context->setBlockMark($blockMark);
		$context->setBlockAttribute($numTableColumns);
		
		return true;
	}
	
	private function continueTable(Context $context) {
		if ($context->isBlockInterrupted()) {
			return false;
		}
		
		$text = $context->getText();
		$numTableColumns = $context->getBlockAttribute();
		$blockMark = $context->getBlockMark();
		if (!$this->parseTableRowText($text, $numTableColumns, $blockMark, $cellInlineContents)) {
			return false;
		}
		
		$context->getBlock()->getRows()->add(new TableRow($cellInlineContents));
	
		return true;
	}
	
	private function parseTableRowText($tableRowText, $expectedNumCellInlineContents, $blockMark,
			&$cellInlineContents) {
		$matches = self::getTableRowRegex()->matches($tableRowText);
		$numMatches = $matches->count();
		
		if ($blockMark == '|') {
			if ($numMatches != $expectedNumCellInlineContents + 2) {
				return false;
			}
			
			$firstMatchValue = $matches->first()->getGroups()[1]->getValue();
			$lastMatchValue = $matches->last()->getGroups()[1]->getValue();
			if (StringUtils::length($firstMatchValue) > 0 || StringUtils::length($lastMatchValue) > 0) {
				return false;
			}
			
			$matches->removeAt($numMatches - 1);
			$matches->removeAt(0);
		} else {
			if ($numMatches != $expectedNumCellInlineContents) {
				return false;
			}
		}
		
		$cellInlineContents = new ArrayList();
		foreach ($matches as $match) {
			$inlineContent = new ArrayList();
			$this->inlineTexts->addAt($inlineContent, $match->getGroups()[1]->getValue());
			$cellInlineContents->add($inlineContent);
		}
		
		return true;
	}
	
	private static function getTableHeaderSeparatorRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(?:\\| *:?-+:? *(?:\\| *:?-+:? *)*\\| *|:?-+:? *(?:\\| *:?-+:? *)+)\\z');
		}
		
		return $regex;
	}
	
	private static function getTableHeaderSeparatorColumnRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(:?)-+(:?)');
		}
		
		return $regex;
	}
	
	private static function getTableRowRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|\\|) *([^|]*)(?<! ) *(?=\\z|\\|)');
		}
		
		return $regex;
	}
	
	private function startBlockQuote(Context $context) {
		$this->pushAndChangeBlock($context, BlockType::BLOCK_QUOTE, new BlockQuote());
		$context->setBlockLines(new ArrayList([self::removeBlockQuoteMark($context->getText())]));
		
		return true;
	}
	
	private function continueBlockQuote(Context $context) {
		if ($context->isBlockInterrupted()) {
			return false;
		}
		
		$text = $context->getText();
		$blockLines = $context->getBlockLines();
		$blockLines->add($text[0] == '>' ? self::removeBlockQuoteMark($text) : $context->getLine());
		
		return true;
	}
	
	private static function removeBlockQuoteMark($text) {
		$textLength = StringUtils::length($text);
		if ($textLength < 2) {
			return '';
		}
		
		$startIndex = $text[1] == StringUtils::SPACE ? 2 : 1;
		return $startIndex < $textLength ? StringUtils::substring($text, $startIndex) : '';
	}
	
	private function startThematicBreak(Context $context) {
		if (!self::getThematicBreakRegex()->isMatch($context->getText())) {
			return false;
		}
		
		$this->pushAndChangeBlock($context, BlockType::THEMATIC_BREAK, new ThematicBreak());
		
		return true;
	}
	
	private static function getThematicBreakRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^([*\\-_])(?: *\\1){2,} *\\z');
		}
		
		return $regex;
	}
	
	private function startLinkReference(Context $context) {
		$match = self::getLinkReferenceRegex()->match($context->getText());
		if (!$match->isSuccess()) {
			return false;
		}
		
		$groups = $match->getGroups();
		$label = StringUtils::toLower($groups[1]->getValue());
		if ($this->linkReferences->containsKey($label)) {
			return true;
		}
		
		$destinationGroupIndex = $groups[2]->getIndex() >= 0 ? 2 : 3;
		if ($groups->count() > 4) {
			$titleGroupIndex = $groups[4]->getIndex() >= 0 ? 4 : ($groups[5]->getIndex() >= 0 ? 5 : 6);
			$title = $groups[$titleGroupIndex]->getValue();
		} else {
			$title = NULL;
		} 
		$this->linkReferences->addAt($label, [$groups[$destinationGroupIndex]->getValue(), $title]);
		$this->pushAndChangeBlock($context, BlockType::LINK_REFERENCE);
		
		return true;
	}
	
	private static function getLinkReferenceRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^\\[([^\\[\\]]*[^ \\[\\]]+[^\\[\\]]*)\\]: *(?:<([^ <>]*)>|([^ ()]+))' .
					'(?: +(?:"([^"]*)"|\'([^\']*)\'|\\(([^)]*)\\)))? *\\z');
		}
		
		return $regex;
	}
	
	private function startHtmlBlock(Context $context) {
		$text = $context->getText();
		$match = self::getFirstHtmlBlockLineRegex()->match($text);
		if (!$match->isSuccess()) {
			return false;
		}
		
		$markup = $context->getLine() . StringUtils::LF;
		$this->pushAndChangeBlock($context, BlockType::HTML_BLOCK, new HtmlBlock($markup));
		$groupIndex = $match->getGroups()[1]->getIndex() >= 0 ? 1 : 2;
		$context->setBlockClosed($groupIndex == 1 && mb_strpos($text, '-->') !== false);
		$context->setBlockAttribute($groupIndex);
		
		return true;
	}
	
	private function continueHtmlBlock(Context $context) {
		if ($context->isBlockClosed()) {
			return false;
		}
		
		$groupIndex = $context->getBlockAttribute();
		
		if ($groupIndex > 1 && $context->isBlockInterrupted()) {
			return false;
		}
		
		$markup = '';
		$blockNumConsecutiveBlankLines = $context->getBlockNumConsecutiveBlankLines();
		for ($i = 0; $i < $blockNumConsecutiveBlankLines; $i++) {
			$markup .= StringUtils::LF;
		}
		$context->resetBlockNumConsecutiveBlankLines();
		
		$context->getBlock()->appendMarkup($markup . $context->getLine() . StringUtils::LF);
		
		if ($groupIndex == 1 && mb_strpos($context->getText(), '-->') !== false) {
			$context->setBlockClosed(true);
		}
		
		return true;
	}
	
	private static function getFirstHtmlBlockLineRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^<(?:(!--)|(/?(?:address|article|aside|base|basefont|blockquote|body|' .
					'caption|center|col|colgroup|dd|details|dialog|dir|div|dl|dt|fieldset|figcaption|' .
					'figure|footer|form|frame|frameset|h[1-6]|head|header|hr|html|iframe|legend|li|link|' .
					'main|menu|menuitem|meta|nav|noframes|ol|optgroup|option|p|param|section|source|title|' .
					'summary|table|tbody|td|tfoot|th|thead|title|tr|track|ul)(?: |/?>|\\z)))',
					RegexOptions::IGNORE_CASE);
		}
		
		return $regex;
	}
	
	private function pushBlock(Context $context) {
		$blockType = $context->getBlockType();
		if ($blockType === NULL) {
			return false;
		}
		
		if ($blockType == BlockType::LINK_REFERENCE) {
			return true;
		}
		
		switch ($blockType) {
		case BlockType::PARAGRAPH:
			$this->processParagraph($context);
			break;
		
		case BlockType::HEADING:
		case BlockType::UNDERLINE_HEADING:
			$this->processHeading($context);
			break;
			
		case BlockType::BLOCK_LIST:
			$this->processList($context);
			break;
			
		case BlockType::BLOCK_QUOTE:
			$this->processBlockQuote($context);
			break;
		}
		
		$context->pushBlock();
		
		return true;
	}
	
	private function processParagraph(Context $context) {
		$inlineContent = new ArrayList();
		$this->inlineTexts->addAt($inlineContent, $context->getBlockInlineText());
		$context->getBlock()->setInlineContent($inlineContent);
	}
	
	private function processHeading(Context $context) {
		$inlineContent = new ArrayList();
		$this->inlineTexts->addAt($inlineContent, $context->getBlockInlineText());
		$context->getBlock()->setInlineContent($inlineContent);
	}
	
	private function processList(Context $context) {
		$this->processListItem($context);
	}
	
	private function processBlockQuote(Context $context) {
		$context->getBlock()->setBlocks($this->createBlocksFromLines($context->getBlockLines()));
	}
	
	private function pushAndChangeBlock(Context $context, $blockType, Block $block = NULL) {
		if ($this->pushBlock($context) && $context->getBlockNumConsecutiveBlankLines() > 0) {
			$context->setBlocksTight(false);
		}
		
		$context->changeBlock($blockType, $block);
	}
	
	private static function getBlockMarkToBlockTypesMap() {
		static $blockMarkToBlockTypesMap = [
			'#'	=> [BlockType::HEADING],
			'*'	=> [BlockType::THEMATIC_BREAK, BlockType::BLOCK_LIST],
			'+'	=> [BlockType::BLOCK_LIST],
			'-'	=> [BlockType::UNDERLINE_HEADING, BlockType::TABLE, BlockType::THEMATIC_BREAK,
					BlockType::BLOCK_LIST],
			'0'	=> [BlockType::BLOCK_LIST],
			'1'	=> [BlockType::BLOCK_LIST],
			'2'	=> [BlockType::BLOCK_LIST],
			'3'	=> [BlockType::BLOCK_LIST],
			'4'	=> [BlockType::BLOCK_LIST],
			'5'	=> [BlockType::BLOCK_LIST],
			'6'	=> [BlockType::BLOCK_LIST],
			'7'	=> [BlockType::BLOCK_LIST],
			'8'	=> [BlockType::BLOCK_LIST],
			'9'	=> [BlockType::BLOCK_LIST],
			':'	=> [BlockType::TABLE],
			'<'	=> [BlockType::HTML_BLOCK],
			'='	=> [BlockType::UNDERLINE_HEADING],
			'>'	=> [BlockType::BLOCK_QUOTE],
			'['	=> [BlockType::LINK_REFERENCE],
			'_'	=> [BlockType::THEMATIC_BREAK],
			'`'	=> [BlockType::FENCED_CODE_BLOCK],
			'|'	=> [BlockType::TABLE],
			'~'	=> [BlockType::FENCED_CODE_BLOCK],
		];
		
		return $blockMarkToBlockTypesMap;
	}
	
	private function createInlineContentFromInlineText($inlineText) {
		$this->inlineContentHead = NULL;
		
		$this->inlineText = trim($inlineText);
		$this->inlineTextPosition = 0;
		
		$this->emphasisDelimitersHead = $this->linkStartsTail = NULL;
		
		$inlineTextLength = StringUtils::length($this->inlineText);
		
		while ($this->inlineTextPosition < $inlineTextLength) {
			$this->remainingInlineText = StringUtils::substring($this->inlineText, $this->inlineTextPosition);
			
			$parsedInlineTextLength = $this->parseInline();
			
			if ($parsedInlineTextLength == 0) {
				$parsedInlineTextLength = $this->parseText();
			}
			
			$this->inlineTextPosition += $parsedInlineTextLength;
		}
		
		$this->processEmphases();
		
		if ($this->inlineContentHead != NULL) {
			$this->inlineContentHead->getPrevious()->setNext(NULL);
		}
		
		return $this->inlineContentHead;
	}
	
	private function parseInline() {
		switch ($this->remainingInlineText[0]) {
		case '\\':
			return $this->parseEscapeSequence();
		
		case '&':
			return $this->parseHtmlEntity();
		
		case '*':
		case '_':
			return $this->parseEmphasisDelimiter();
			
		case '[':
			return $this->parseLinkStart();
		
		case ']':
			return $this->parseLinkEnd();
			
		case '<':
			$parsedInlineTextLength = $this->parseEmailAutolink();
			if ($parsedInlineTextLength > 0) {
				return $parsedInlineTextLength;
			}
			
			$parsedInlineTextLength = $this->parseAutolink();
			if ($parsedInlineTextLength > 0) {
				return $parsedInlineTextLength;
			}
			
			return $this->parseHtmlInline();
		
		case '!':
			return $this->parseImageStart();
			
		case '`':
			return $this->parseCodeSpan();
		
		case StringUtils::LF:
			return $this->parseLineBreak();
		
		default:
			return 0;
		}
	}
	
	private function parseText() {
		$match = self::getTextRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
	
		$content = $match->getValue();
		$this->pushText($content);
		
		return StringUtils::length($content);
	}
	
	private static function getTextRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^.[^\\n!&*<\\[\\\\\\]_`]*', RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private function parseEscapeSequence() {
		if (StringUtils::length($this->remainingInlineText) < 2 ||
				!self::getEscapeSequenceCharacters()->contains($this->remainingInlineText[1])) {
			return 0;
		}
		
		if ($this->remainingInlineText[1] == StringUtils::LF) {
			$this->pushInline(new HardLineBreak());
		} else {
			$this->pushText($this->remainingInlineText[1]);
		}
		
		return 2;
	}
	
	private static function getEscapeSequenceCharacters() {
		static $characters = NULL;
		if ($characters == NULL) {
			$characters = new Set([StringUtils::LF, '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+',
					',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_', '`', '{',
					'|', '}', '~']);
		}
		
		return $characters;
	}
	
	private function parseHtmlEntity() {
		$match = self::getHtmlEntityRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
	
		$htmlEntity = $match->getValue();
		$this->pushText(html_entity_decode($htmlEntity, ENT_QUOTES | ENT_HTML5, 'UTF-8'));
		
		return StringUtils::length($htmlEntity);
	}
	
	private static function getHtmlEntityRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^&(?:#(?:X|x)[0-9A-Fa-f]{1,8}|#[0-9]{1,8}|[A-Za-z][0-9A-Za-z]{1,31});');
		}
		
		return $regex;
	}
	
	private function parseEmphasisDelimiter() {
		$match = self::getEmphasisDelimiterRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
		
		$delimiter = $match->getValue();
		$delimiterLength = StringUtils::length($delimiter);
		
		if ($this->inlineTextPosition > 0) {
			$precedingCharacter = StringUtils::substring($this->inlineText, $this->inlineTextPosition - 1, 1);
		} else {
			$precedingCharacter = StringUtils::LF;
		}
		
		if (StringUtils::length($this->remainingInlineText) > $delimiterLength) {
			$followingCharacter = StringUtils::substring($this->remainingInlineText, $delimiterLength, 1);
		} else {
			$followingCharacter = StringUtils::LF;
		}
		
		$punctuationCharacterRegex = self::getPunctuationCharacterRegex();
		$precedingCharacterIsWhitespaceCharacter = $precedingCharacter == StringUtils::LF ||
				$precedingCharacter == StringUtils::SPACE;
		$precedingCharacterIsPunctuationCharacter = $punctuationCharacterRegex->isMatch($precedingCharacter);
		$followingCharacterIsWhitespaceCharacter = $followingCharacter == StringUtils::LF ||
				$followingCharacter == StringUtils::SPACE;
		$followingCharacterIsPunctuationCharacter = $punctuationCharacterRegex->isMatch($followingCharacter);
		
		$delimiterIsLeftFlanking = !$followingCharacterIsWhitespaceCharacter &&
				(!$followingCharacterIsPunctuationCharacter ||
				$precedingCharacterIsWhitespaceCharacter || $precedingCharacterIsPunctuationCharacter);
		$delimiterIsRightFlanking = !$precedingCharacterIsWhitespaceCharacter &&
				(!$precedingCharacterIsPunctuationCharacter ||
				$followingCharacterIsWhitespaceCharacter || $followingCharacterIsPunctuationCharacter);
		
		if ($delimiter[0] == '_') {
			$potentialOpener = $delimiterIsLeftFlanking &&
					(!$delimiterIsRightFlanking || $precedingCharacterIsPunctuationCharacter);
			$potentialCloser = $delimiterIsRightFlanking &&
					(!$delimiterIsLeftFlanking || $followingCharacterIsPunctuationCharacter);
		} else {
			$potentialOpener = $delimiterIsLeftFlanking;
			$potentialCloser = $delimiterIsRightFlanking;
		}
		
		$linkedText = $this->pushInline(new Text($delimiter));
		$emphasisDelimiter = new EmphasisDelimiter($linkedText, $potentialOpener, $potentialCloser);
		if ($this->emphasisDelimitersHead != NULL) {
			$tail = $this->emphasisDelimitersHead->getPrevious();
			$emphasisDelimiter->setNext($this->emphasisDelimitersHead);
			$emphasisDelimiter->setPrevious($tail);
			$tail->setNext($emphasisDelimiter);
			$this->emphasisDelimitersHead->setPrevious($emphasisDelimiter);
		} else {
			$emphasisDelimiter->setNext($emphasisDelimiter);
			$emphasisDelimiter->setPrevious($emphasisDelimiter);
			$this->emphasisDelimitersHead = $emphasisDelimiter;
		}
		
		return $delimiterLength;
	}
	
	private static function getEmphasisDelimiterRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(([*_])\\2*)');
		}
		
		return $regex;
	}
	
	private static function getPunctuationCharacterRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[!"#%-*,-/:;?@[\\\\\\]_{}\\xa1\\xa7\\xab\\xb6\\xb7\\xbb\\xbf\\x{037e}' .
					'\\x{0387}\\x{055a}-\\x{055f}\\x{0589}\\x{058a}\\x{05be}\\x{05c0}\\x{05c3}\\x{05c6}' .
					'\\x{05f3}\\x{05f4}\\x{0609}\\x{060a}\\x{060c}\\x{060d}\\x{061b}\\x{061e}\\x{061f}' .
					'\\x{066a}-\\x{066d}\\x{06d4}\\x{0700}-\\x{070d}\\x{07f7}-\\x{07f9}\\x{0830}-\\x{083e}' .
					'\\x{085e}\\x{0964}\\x{0965}\\x{0970}\\x{0af0}\\x{0df4}\\x{0e4f}\\x{0e5a}\\x{0e5b}' .
					'\\x{0f04}-\\x{0f12}\\x{0f14}\\x{0f3a}-\\x{0f3d}\\x{0f85}\\x{0fd0}-\\x{0fd4}\\x{0fd9}' .
					'\\x{0fda}\\x{104a}-\\x{104f}\\x{10fb}\\x{1360}-\\x{1368}\\x{1400}\\x{166d}\\x{166e}' .
					'\\x{169b}\\x{169c}\\x{16eb}-\\x{16ed}\\x{1735}\\x{1736}\\x{17d4}-\\x{17d6}' .
					'\\x{17d8}-\\x{17da}\\x{1800}-\\x{180a}\\x{1944}\\x{1945}\\x{1a1e}\\x{1a1f}' .
					'\\x{1aa0}-\\x{1aa6}\\x{1aa8}-\\x{1aad}\\x{1b5a}-\\x{1b60}\\x{1bfc}-\\x{1bff}' .
					'\\x{1c3b}-\\x{1c3f}\\x{1c7e}\\x{1c7f}\\x{1cc0}-\\x{1cc7}\\x{1cd3}\\x{2010}-\\x{2027}' .
					'\\x{2030}-\\x{2043}\\x{2045}-\\x{2051}\\x{2053}-\\x{205e}\\x{207d}\\x{207e}\\x{208d}' .
					'\\x{208e}\\x{2308}-\\x{230b}\\x{2329}\\x{232a}\\x{2768}-\\x{2775}\\x{27c5}\\x{27c6}' .
					'\\x{27e6}-\\x{27ef}\\x{2983}-\\x{2998}\\x{29d8}-\\x{29db}\\x{29fc}\\x{29fd}' .
					'\\x{2cf9}-\\x{2cfc}\\x{2cfe}\\x{2cff}\\x{2d70}\\x{2e00}-\\x{2e2e}\\x{2e30}-\\x{2e42}' .
					'\\x{3001}-\\x{3003}\\x{3008}-\\x{3011}\\x{3014}-\\x{301f}\\x{3030}\\x{303d}\\x{30a0}' .
					'\\x{30fb}\\x{a4fe}\\x{a4ff}\\x{a60d}-\\x{a60f}\\x{a673}\\x{a67e}\\x{a6f2}-\\x{a6f7}' .
					'\\x{a874}-\\x{a877}\\x{a8ce}\\x{a8cf}\\x{a8f8}-\\x{a8fa}\\x{a8fc}\\x{a92e}\\x{a92f}' .
					'\\x{a95f}\\x{a9c1}-\\x{a9cd}\\x{a9de}\\x{a9df}\\x{aa5c}-\\x{aa5f}\\x{aade}\\x{aadf}' .
					'\\x{aaf0}\\x{aaf1}\\x{abeb}\\x{fd3e}\\x{fd3f}\\x{fe10}-\\x{fe19}\\x{fe30}-\\x{fe52}' .
					'\\x{fe54}-\\x{fe61}\\x{fe63}\\x{fe68}\\x{fe6a}\\x{fe6b}\\x{ff01}-\\x{ff03}' .
					'\\x{ff05}-\\x{ff0a}\\x{ff0c}-\\x{ff0f}\\x{ff1a}\\x{ff1b}\\x{ff1f}\\x{ff20}' .
					'\\x{ff3b}-\\x{ff3d}\\x{ff3f}\\x{ff5b}\\x{ff5d}\\x{ff5f}-\\x{ff65}]\\z');
		}
		
		return $regex;
	}
	
	private function parseLinkStart() {
		$this->pushLinkStart($this->pushInline(new Text('[')), $this->inlineTextPosition + 1);
	
		return 1;
	}
	
	private function parseLinkEnd() {
		if ($this->linkStartsTail == NULL) {
			return 0;
		}
		
		$linkStart = $this->linkStartsTail;
		$this->linkStartsTail = $linkStart->getPrevious();
		
		if (!$linkStart->isActive()) {
			return 0;
		}
		
		$match = self::getRestOfLinkRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
		
		$groups = $match->getGroups();
		$numGroups = $groups->count();
		if ($numGroups > 1 && $numGroups < 7) {
			$destination = $groups[$groups[1]->getIndex() >= 0 ? 1 : 2]->getValue();
			if ($numGroups > 3) {
				$titleGroupIndex = $groups[3]->getIndex() >= 0 ? 3 : ($groups[4]->getIndex() >= 0 ? 4 : 5);
				$title = $groups[$titleGroupIndex]->getValue();
			} else {
				$title = NULL;
			}
		} else {
			if ($numGroups > 6) {
				$label = $groups[6]->getValue();
			} elseif ($linkStart->isLast()) {
				$labelIndex = $linkStart->getLabelIndex();
				$labelLength = $this->inlineTextPosition - $labelIndex;
				$label = StringUtils::substring($this->inlineText, $labelIndex, $labelLength);
			} else {
				return 0;
			}
			
			$label = StringUtils::toLower($label);
			if (!$this->linkReferences->containsKey($label)) {
				return 0;
			}
			
			list($destination, $title) = $this->linkReferences[$label];
		}
		
		$this->processEmphases($linkStart->getLastEmphasisDelimiter());
		
		$linkedText = $linkStart->getLinkedText();
		
		$inlineContent = new ArrayList();
		$linkedInline = $linkedText->getNext();
		while ($linkedInline !== $this->inlineContentHead) {
			$inlineContent->add($linkedInline->getInline());
			$linkedInline = $linkedInline->getNext();
		}
		
		if ($linkedText !== $this->inlineContentHead) {
			$tail = $linkedText->getPrevious();
			$tail->setNext($this->inlineContentHead);
			$this->inlineContentHead->setPrevious($tail);
		} else {
			$this->inlineContentHead = NULL;
		}
		
		if ($linkStart->isImageStart()) {
			$inline = new Image($inlineContent, $destination, $title);
		} else {
			$inline = new Link($inlineContent, $destination, $title);
			
			$linkStart = $this->linkStartsTail;
			while ($linkStart != NULL) {
				if (!$linkStart->isImageStart()) {
					$linkStart->setActive(false);
				}
				$linkStart = $linkStart->getPrevious();
			}
		}
		
		$this->pushInline($inline);
	
		return StringUtils::length($match->getValue());
	}
	
	private static function getRestOfLinkRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^](?:\\( *(?:\\n *)?(?:<([^\\n <>]*)>|([^\\n ()]*)) *(?:\\n *)?' .
					'(?:(?<=[\\n ])(?:"([^"]*)"|\'([^\']*)\'|\\(([^)]*)\\)) *(?:\\n *)?)?\\)|' .
					'(?:\\[([^\\[\\]]*[^\\n \\[\\]]+[^\\[\\]]*)?\\])?)');
		}
		
		return $regex;
	}
	
	private function parseEmailAutolink() {
		$match = self::getEmailAutolinkRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
		
		$emailAddress = $match->getGroups()[1]->getValue();
		$this->pushInline(new Link(new ArrayList([new Text($emailAddress)]), 'mailto:' . $emailAddress));
	
		return StringUtils::length($match->getValue());
	}
	
	private static function getEmailAutolinkRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^<([!#-\'*+\\--9=?A-Z^-~]+@[0-9A-Za-z](?:[-0-9A-Za-z]{0,61}[0-9A-Za-z])?' .
					'(?:\\.[0-9A-Za-z](?:[-0-9A-Za-z]{0,61}[0-9A-Za-z])?)*)>');
		}
		
		return $regex;
	}
	
	private function parseAutolink() {
		$match = self::getAutolinkRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
		
		$uri = $match->getGroups()[1]->getValue();
		$this->pushInline(new Link(new ArrayList([new Text($uri)]), $uri));
	
		return StringUtils::length($match->getValue());
	}
	
	private static function getAutolinkRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^<([A-Za-z][+\\-.0-9A-Za-z]{1,31}:[!-;=?-~]*)>');
		}
		
		return $regex;
	}
	
	private function parseImageStart() {
		if (StringUtils::length($this->remainingInlineText) < 2 || $this->remainingInlineText[1] != '[') {
			return 0;
		}
		
		$this->pushLinkStart($this->pushInline(new Text('![')), $this->inlineTextPosition + 2);
		
		return 2;
	}
	
	private function parseCodeSpan() {
		if ($this->inlineTextPosition > 0) {
			if (StringUtils::substring($this->inlineText, $this->inlineTextPosition - 1, 1) == '`') {
				return 0;
			}
		}
	
		$match = self::getCodeSpanRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
		
		$regex = self::getWhitespaceCollapseRegex();
		$space = StringUtils::SPACE;
		$this->pushInline(new CodeSpan($regex->replace($match->getGroups()[2]->getValue(), $space)));
	
		return StringUtils::length($match->getValue());
	}
	
	private static function getCodeSpanRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(`+)(?!`)[\\n ]*(.*?)[\\n ]*(?<!`)\\1(?!`)', RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private static function getWhitespaceCollapseRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('[\\n ]+');
		}
		
		return $regex;
	}
	
	private function parseLineBreak() {
		$hard = false;
		if ($this->inlineContentHead != NULL) {
			$inlineContentTail = $this->inlineContentHead->getPrevious();
			$text = $inlineContentTail->getInline();
			if ($text->getNodeType() == NodeType::TEXT) {
				$content = $text->getContent();
				if (StringUtils::length($content) >= 2 && StringUtils::substring($content, -2) == '  ') {
					$hard = true;
				}
				$content = rtrim($content);
				if (StringUtils::length($content) > 0) {
					$text->setContent($content);
				} else {
					if ($this->inlineContentHead === $inlineContentTail) {
						$this->inlineContentHead = NULL;
					} else {
						$inlineContentTail = $inlineContentTail->getPrevious();
						$inlineContentTail->setNext($this->inlineContentHead);
						$this->inlineContentHead->setPrevious($inlineContentTail);
					}
				}
			}
		}
		
		$this->pushInline($hard ? new HardLineBreak() : new SoftLineBreak());
		
		return 1;
	}
	
	private function parseHtmlInline() {
		$match = self::getHtmlInlineRegex()->match($this->remainingInlineText);
		if (!$match->isSuccess()) {
			return 0;
		}
		
		$markup = $match->getValue();
		$this->pushInline(new HtmlInline($markup));
	
		return StringUtils::length($markup);
	}
	
	private static function getHtmlInlineRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^<(?:[A-Za-z][-0-9A-Za-z]*(?:[\\n ]+[:A-Z_a-z][-.0-9:A-Z_a-z]*' .
					'(?:[\\n ]*=[\\n ]*(?:[^\\x00- "\'<=>`]+|\'[^\']*\'|"[^"]*"))?)*[\\n ]*/?|' .
					'/[A-Za-z][-0-9A-Za-z]*[\\n ]*|!----|!---?[^->](?:-?[^-])*--|\\?.*?\\?|' .
					'![A-Z]+[\\n ]+[^>]*|!\\[CDATA\\[.*?\\]\\])>', RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private function processEmphases(EmphasisDelimiter $emphasisDelimitersLowerBound = NULL) {
		if ($emphasisDelimitersLowerBound != NULL) {
			$closer = $emphasisDelimitersLowerBound->getNext();
			if ($closer === $this->emphasisDelimitersHead) {
				$closer = NULL;
			}
			$openerLowerBounds = [
				'*'	=> $emphasisDelimitersLowerBound,
				'_'	=> $emphasisDelimitersLowerBound,
			];
		} else {
			$closer = $this->emphasisDelimitersHead;
			$tail = $closer != NULL ? $closer->getPrevious() : NULL;
			$openerLowerBounds = [
				'*'	=> $tail,
				'_'	=> $tail,
			];
		}
		$isOddMatch = false;
		
		while ($closer != NULL) {
			if (!$closer->canClose()) {
				$closer = $closer->getNext();
				if ($closer === $this->emphasisDelimitersHead) {
					$closer = NULL;
				}
				continue;
			}
			
			$mark = $closer->getMark();
			$closerLength = $closer->getLength();
			$closerCanOpen = $closer->canOpen();
			
			$opener = NULL;
			$emphasisDelimiter = $closer->getPrevious();
			while ($emphasisDelimiter !== $openerLowerBounds[$mark] && $opener == NULL) {
				$isOddMatch = $closerCanOpen || $emphasisDelimiter->canClose();
				$isOddMatch = $isOddMatch && ($emphasisDelimiter->getLength() + $closerLength) % 3 == 0;
				if ($emphasisDelimiter->getMark() == $mark && $emphasisDelimiter->canOpen() && !$isOddMatch) {
					$opener = $emphasisDelimiter;
				} else {
					$emphasisDelimiter = $emphasisDelimiter->getPrevious();
				}
			}
			
			if ($opener != NULL) {
				$openerLength = $opener->getLength();
				if ($openerLength < 3 || $closerLength < 3) {
					$usedLength = min($openerLength, $closerLength);
				} else {
					$usedLength = $closerLength % 2 == 0 ? 2 : 1;
				}
				
				$openerLinkedText = $opener->getLinkedText();
				$closerLinkedText = $closer->getLinkedText();
				
				$openerText = $openerLinkedText->getInline();
				$closerText = $closerLinkedText->getInline();
				$openerText->setContent(StringUtils::substring($openerText->getContent(), 0, -$usedLength));
				$closerText->setContent(StringUtils::substring($closerText->getContent(), 0, -$usedLength));
				
				$inlineContent = new ArrayList();
				$linkedInline = $openerLinkedText->getNext();
				while ($linkedInline !== $closerLinkedText) {
					$inlineContent->add($linkedInline->getInline());
					$linkedInline = $linkedInline->getNext();
				}
				
				$inline = $usedLength > 1 ? new StrongEmphasis($inlineContent) : new Emphasis($inlineContent);
				$linkedInline = new LinkedInline($inline);
				$linkedInline->setNext($closerLinkedText);
				$linkedInline->setPrevious($openerLinkedText);
				$openerLinkedText->setNext($linkedInline);
				$closerLinkedText->setPrevious($linkedInline);
				
				$opener->setNext($closer);
				$closer->setPrevious($opener);
				
				if ($opener->getLength() == 0) {
					$this->removeLinkedInline($openerLinkedText);
					$this->removeEmphasisDelimiter($opener);
				}
				
				if ($closer->getLength() == 0) {
					$emphasisDelimiter = $closer;
					$closer = $closer->getNext();
					if ($closer === $this->emphasisDelimitersHead) {
						$closer = NULL;
					}
					$this->removeLinkedInline($closerLinkedText);
					$this->removeEmphasisDelimiter($emphasisDelimiter);
				}
			} else {
				$emphasisDelimiter = $closer;
				$closer = $closer->getNext();
				if ($closer === $this->emphasisDelimitersHead) {
					$closer = NULL;
				}
				if (!$isOddMatch) {
					$openerLowerBounds[$mark] = $emphasisDelimiter->getPrevious();
					if (!$closerCanOpen) {
						$this->removeEmphasisDelimiter($emphasisDelimiter);
					}
				}
			}
		}
		
		if ($emphasisDelimitersLowerBound != NULL) {
			$emphasisDelimitersLowerBound->setNext($this->emphasisDelimitersHead);
			$this->emphasisDelimitersHead->setPrevious($emphasisDelimitersLowerBound);
		} else {
			$this->emphasisDelimitersHead = NULL;
		}
	}
	
	private function pushText($content) {
		if ($this->inlineContentHead != NULL) {
			$text = $this->inlineContentHead->getPrevious()->getInline();
			if ($text->getNodeType() == NodeType::TEXT) {
				$originalContent = $text->getContent();
				if ($originalContent != '[' && $originalContent != '![') {
					if (StringUtils::length($originalContent) == 0 ||
							($originalContent[0] != '*' && $originalContent[0] != '_')) {
						$text->appendContent($content);
						return;
					}
				}
			}
		}
		
		$this->pushInline(new Text($content));
	}
	
	private function pushInline($inline) {
		$linkedInline = new LinkedInline($inline);
		if ($this->inlineContentHead != NULL) {
			$tail = $this->inlineContentHead->getPrevious();
			$linkedInline->setNext($this->inlineContentHead);
			$linkedInline->setPrevious($tail);
			$tail->setNext($linkedInline);
			$this->inlineContentHead->setPrevious($linkedInline);
		} else {
			$linkedInline->setNext($linkedInline);
			$linkedInline->setPrevious($linkedInline);
			$this->inlineContentHead = $linkedInline;
		}
		
		return $linkedInline;
	}
	
	private function removeLinkedInline(LinkedInline $linkedInline) {
		if ($linkedInline === $linkedInline->getNext()) {
			$this->inlineContentHead = NULL;
			return;
		}
		
		$next = $linkedInline->getNext();
		$previous = $linkedInline->getPrevious();
		$previous->setNext($next);
		$next->setPrevious($previous);
		
		if ($linkedInline === $this->inlineContentHead) {
			$this->inlineContentHead = $next;
		}
	}
	
	private function removeEmphasisDelimiter(EmphasisDelimiter $emphasisDelimiter) {
		if ($emphasisDelimiter === $emphasisDelimiter->getNext()) {
			$this->emphasisDelimitersHead = NULL;
			return;
		}
		
		$next = $emphasisDelimiter->getNext();
		$previous = $emphasisDelimiter->getPrevious();
		$previous->setNext($next);
		$next->setPrevious($previous);
		
		if ($emphasisDelimiter === $this->emphasisDelimitersHead) {
			$this->emphasisDelimitersHead = $next;
		}
	}
	
	private function pushLinkStart(LinkedInline $linkedText, $labelIndex) {
		if ($this->linkStartsTail != NULL) {
			$this->linkStartsTail->setLast(false);
		}
		
		if ($this->emphasisDelimitersHead != NULL) {
			$lastEmphasisDelimiter = $this->emphasisDelimitersHead->getPrevious();
		} else {
			$lastEmphasisDelimiter = NULL;
		}
		$linkStart = new LinkStart($linkedText, $labelIndex, $lastEmphasisDelimiter, $this->linkStartsTail);
		$this->linkStartsTail = $linkStart;
	}

}

?>
