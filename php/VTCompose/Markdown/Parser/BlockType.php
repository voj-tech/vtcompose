<?php

namespace VTCompose\Markdown\Parser;

use VTCompose\Enum;

/**
 * @internal
 */
final class BlockType extends Enum {

	const PARAGRAPH = 0;
	const HEADING = 1;
	const UNDERLINE_HEADING = 2;
	const BLOCK_LIST = 3;
	const CODE_BLOCK = 4;
	const FENCED_CODE_BLOCK = 5;
	const TABLE = 6;
	const BLOCK_QUOTE = 7;
	const THEMATIC_BREAK = 8;
	const LINK_REFERENCE = 9;
	const HTML_BLOCK = 10;

}

?>
