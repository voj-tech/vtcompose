<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class NodeType extends Enum {
	
	/**
	 * 
	 *
	 * 
	 */
	const PARAGRAPH = 0;
	
	/**
	 * 
	 *
	 * 
	 */
	const HEADING = 1;
	
	/**
	 * 
	 *
	 * 
	 */
	const BLOCK_LIST = 2;
	
	/**
	 * 
	 *
	 * 
	 */
	const CODE_BLOCK = 3;
	
	/**
	 * 
	 *
	 * 
	 */
	const TABLE = 4;
	
	/**
	 * 
	 *
	 * 
	 */
	const BLOCK_QUOTE = 5;
	
	/**
	 * 
	 *
	 * 
	 */
	const THEMATIC_BREAK = 6;
	
	/**
	 * 
	 *
	 * 
	 */
	const HTML_BLOCK = 7;
	
	/**
	 * 
	 *
	 * 
	 */
	const TEXT = 8;
	
	/**
	 * 
	 *
	 * 
	 */
	const EMPHASIS = 9;
	
	/**
	 * 
	 *
	 * 
	 */
	const STRONG_EMPHASIS = 10;
	
	/**
	 * 
	 *
	 * 
	 */
	const LINK = 11;
	
	/**
	 * 
	 *
	 * 
	 */
	const IMAGE = 12;
	
	/**
	 * 
	 *
	 * 
	 */
	const CODE_SPAN = 13;
	
	/**
	 * 
	 *
	 * 
	 */
	const HARD_LINE_BREAK = 14;
	
	/**
	 * 
	 *
	 * 
	 */
	const SOFT_LINE_BREAK = 15;
	
	/**
	 * 
	 *
	 * 
	 */
	const HTML_INLINE = 16;

}

?>
