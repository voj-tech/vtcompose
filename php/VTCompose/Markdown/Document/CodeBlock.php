<?php

namespace VTCompose\Markdown\Document;

/**
 * 
 *
 * 
 */
class CodeBlock extends Block {

	private $codeText;
	private $info;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 */
	public function __construct($codeText = '', $info = NULL) {
		$this->codeText = $codeText;
		$this->info = $info;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setCodeText($codeText) {
		$this->codeText = $codeText;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function appendCodeText($codeText) {
		$this->codeText .= $codeText;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getCodeText() {
		return $this->codeText;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setInfo($info) {
		$this->info = $info;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getInfo() {
		return $this->info;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::CODE_BLOCK;
	}

}

?>
