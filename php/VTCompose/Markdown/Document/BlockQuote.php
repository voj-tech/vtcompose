<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class BlockQuote extends Block {

	private $blocks;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function __construct(IList $blocks = NULL) {
		$this->blocks = $blocks;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setBlocks(IList $blocks) {
		$this->blocks = $blocks;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getBlocks() {
		return $this->blocks;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::BLOCK_QUOTE;
	}

}

?>
