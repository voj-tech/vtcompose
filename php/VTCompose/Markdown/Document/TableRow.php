<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class TableRow {

	private $cellInlineContents;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function __construct(IList $cellInlineContents = NULL) {
		$this->cellInlineContents = $cellInlineContents;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setCellInlineContents(IList $cellInlineContents) {
		$this->cellInlineContents = $cellInlineContents;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getCellInlineContents() {
		return $this->cellInlineContents;
	}

}

?>
