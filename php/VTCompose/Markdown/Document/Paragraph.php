<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class Paragraph extends Block {

	private $inlineContent;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function __construct(IList $inlineContent = NULL) {
		$this->inlineContent = $inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setInlineContent(IList $inlineContent) {
		$this->inlineContent = $inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getInlineContent() {
		return $this->inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::PARAGRAPH;
	}

}

?>
