<?php

namespace VTCompose\Markdown\Document;

/**
 * 
 *
 * 
 */
class CodeSpan extends Inline {

	private $codeText;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function __construct($codeText = '') {
		$this->codeText = $codeText;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setCodeText($codeText) {
		$this->codeText = $codeText;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getCodeText() {
		return $this->codeText;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::CODE_SPAN;
	}

}

?>
