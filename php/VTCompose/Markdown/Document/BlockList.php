<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class BlockList extends Block {

	private $items;
	private $ordered;
	private $startNumber;
	private $tight;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 * @param bool 
	 * @param int 
	 * @param bool 
	 */
	public function __construct(IList $items = NULL, $ordered = true, $startNumber = 1, $tight = false) {
		$this->items = $items;
		$this->ordered = $ordered;
		$this->startNumber = $startNumber;
		$this->tight = $tight;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setItems(IList $items) {
		$this->items = $items;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getItems() {
		return $this->items;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 */
	public function setOrdered($ordered) {
		$this->ordered = $ordered;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isOrdered() {
		return $this->ordered;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function setStartNumber($startNumber) {
		$this->startNumber = $startNumber;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getStartNumber() {
		return $this->startNumber;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 */
	public function setTight($tight) {
		$this->tight = $tight;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isTight() {
		return $this->tight;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::BLOCK_LIST;
	}

}

?>
