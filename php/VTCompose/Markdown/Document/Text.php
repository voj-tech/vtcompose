<?php

namespace VTCompose\Markdown\Document;

/**
 * 
 *
 * 
 */
class Text extends Inline {

	private $content;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function __construct($content = '') {
		$this->content = $content;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setContent($content) {
		$this->content = $content;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function appendContent($content) {
		$this->content .= $content;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getContent() {
		return $this->content;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::TEXT;
	}

}

?>
