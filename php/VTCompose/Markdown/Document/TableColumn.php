<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class TableColumn {

	private $headerInlineContent;
	private $textAlignment;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 * @param int 
	 */
	public function __construct(IList $headerInlineContent = NULL, $textAlignment = TextAlignment::NONE) {
		$this->headerInlineContent = $headerInlineContent;
		$this->textAlignment = $textAlignment;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setHeaderInlineContent(IList $headerInlineContent) {
		$this->headerInlineContent = $headerInlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getHeaderInlineContent() {
		return $this->headerInlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function setTextAlignment($textAlignment) {
		$this->textAlignment = $textAlignment;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getTextAlignment() {
		return $this->textAlignment;
	}

}

?>
