<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class Image extends Inline {

	private $inlineContent;
	private $destination;
	private $title;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 * @param string 
	 * @param string 
	 */
	public function __construct(IList $inlineContent = NULL, $destination = '', $title = NULL) {
		$this->inlineContent = $inlineContent;
		$this->destination = $destination;
		$this->title = $title;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setInlineContent(IList $inlineContent) {
		$this->inlineContent = $inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getInlineContent() {
		return $this->inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setDestination($destination) {
		$this->destination = $destination;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getDestination() {
		return $this->destination;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::IMAGE;
	}

}

?>
