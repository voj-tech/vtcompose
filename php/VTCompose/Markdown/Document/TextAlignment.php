<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class TextAlignment extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const NONE = 0;
	
	/**
	 * 
	 *
	 * 
	 */
	const LEFT = 1;
	
	/**
	 * 
	 *
	 * 
	 */
	const RIGHT = 2;
	
	/**
	 * 
	 *
	 * 
	 */
	const CENTER = 3;

}

?>
