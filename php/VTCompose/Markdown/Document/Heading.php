<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class Heading extends Block {

	private $level;
	private $inlineContent;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param IList 
	 */
	public function __construct($level, IList $inlineContent = NULL) {
		$this->level = $level;
		$this->inlineContent = $inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function setLevel($level) {
		$this->level = $level;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getLevel() {
		return $this->level;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setInlineContent(IList $inlineContent) {
		$this->inlineContent = $inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getInlineContent() {
		return $this->inlineContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::HEADING;
	}

}

?>
