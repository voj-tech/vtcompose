<?php

namespace VTCompose\Markdown\Document;

use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class Table extends Block {

	private $columns;
	private $rows;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 * @param IList 
	 */
	public function __construct(IList $columns = NULL, IList $rows = NULL) {
		$this->columns = $columns;
		$this->rows = $rows;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setColumns(IList $columns) {
		$this->columns = $columns;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getColumns() {
		return $this->columns;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setRows(IList $rows) {
		$this->rows = $rows;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getRows() {
		return $this->rows;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::TABLE;
	}

}

?>
