<?php

namespace VTCompose\Markdown\Document;

/**
 * 
 *
 * 
 */
class HtmlInline extends Inline {

	private $markup;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function __construct($markup = '') {
		$this->markup = $markup;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setMarkup($markup) {
		$this->markup = $markup;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getMarkup() {
		return $this->markup;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::HTML_INLINE;
	}

}

?>
