<?php

namespace VTCompose\Markdown;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Markdown\Document\BlockList;
use VTCompose\Markdown\Document\BlockQuote;
use VTCompose\Markdown\Document\CodeBlock;
use VTCompose\Markdown\Document\CodeSpan;
use VTCompose\Markdown\Document\Document;
use VTCompose\Markdown\Document\Emphasis;
use VTCompose\Markdown\Document\HardLineBreak;
use VTCompose\Markdown\Document\Heading;
use VTCompose\Markdown\Document\HtmlBlock;
use VTCompose\Markdown\Document\HtmlInline;
use VTCompose\Markdown\Document\Image;
use VTCompose\Markdown\Document\Link;
use VTCompose\Markdown\Document\Node;
use VTCompose\Markdown\Document\NodeType;
use VTCompose\Markdown\Document\Paragraph;
use VTCompose\Markdown\Document\SoftLineBreak;
use VTCompose\Markdown\Document\StrongEmphasis;
use VTCompose\Markdown\Document\Table;
use VTCompose\Markdown\Document\Text;
use VTCompose\Markdown\Document\TextAlignment;
use VTCompose\Markdown\Document\ThematicBreak;
use VTCompose\StringUtilities as StringUtils;
use VTCompose\Xml\Dom\Core\Document as DomDocument;
use VTCompose\Xml\Dom\Core\Implementation as DomImplementation;
use VTCompose\Xml\Dom\Core\Node as DomNode;

/**
 * 
 *
 * 
 *
 * @todo Create an interface for classes hooking onto traversing a Markdown document. Create an abstract class
 * implementing the traversal without any additional logic.
 */
class DomGenerator {

	private $domImplementation;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param DomImplementation 
	 */
	public function __construct(DomImplementation $domImplementation = NULL) {
		$this->domImplementation = $domImplementation != NULL ? $domImplementation : new DomImplementation();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Document 
	 * @return DomDocument 
	 */
	public function generateDom(Document $document) {
		$namespaceUri = '';
		$qualifiedName = 'document';
		$domDocument = $this->domImplementation->createDocument($namespaceUri, $qualifiedName);
		
		$nodeStack = new ArrayList();
		self::pushNodesOntoStack($nodeStack, $document->getBlocks(), $domDocument->getDocumentElement());
		while ($nodeStack->count() > 0) {
			list($node, $parentDomNode) = self::popNodeFromStack($nodeStack);
			$this->processNode($domDocument, $nodeStack, $node, $parentDomNode);
		}
		
		return $domDocument;
	}
	
	/**
	 * @todo When there is a reverse() method in the IEnumerable interface use it here.
	 */
	private static function pushNodesOntoStack(IList $nodeStack, IList $nodes, DomNode $parentDomNode) {
		for ($i = $nodes->count() - 1; $i >= 0; $i--) {
			self::pushNodeOntoStack($nodeStack, $nodes[$i], $parentDomNode);
		}
	}
	
	private static function pushNodeOntoStack(IList $nodeStack, Node $node, DomNode $parentDomNode) {
		$nodeStack->add([$node, $parentDomNode]);
	}
	
	private static function popNodeFromStack(IList $nodeStack) {
		$nodeStackIndex = $nodeStack->count() - 1;
		if ($nodeStackIndex == -1) {
			throw new InvalidOperationException('Node stack is empty.');
		}
		
		$nodeAndParentDomNode = $nodeStack[$nodeStackIndex];
		$nodeStack->removeAt($nodeStackIndex);
		return $nodeAndParentDomNode;
	}
	
	private function processNode(DomDocument $domDocument, IList $nodeStack,
			Node $node, DomNode $parentDomNode) {
		switch ($node->getNodeType()) {
		case NodeType::PARAGRAPH:
			$this->processParagraph($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::HEADING:
			$this->processHeading($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::BLOCK_LIST:
			$this->processList($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::CODE_BLOCK:
			$this->processCodeBlock($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::TABLE:
			$this->processTable($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::BLOCK_QUOTE:
			$this->processBlockQuote($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::THEMATIC_BREAK:
			$this->processThematicBreak($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::HTML_BLOCK:
			$this->processHtmlBlock($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::TEXT:
			$this->processText($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::EMPHASIS:
			$this->processEmphasis($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::STRONG_EMPHASIS:
			$this->processStrongEmphasis($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::LINK:
			$this->processLink($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::IMAGE:
			$this->processImage($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::CODE_SPAN:
			$this->processCodeSpan($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::HARD_LINE_BREAK:
			$this->processHardLineBreak($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::SOFT_LINE_BREAK:
			$this->processSoftLineBreak($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		case NodeType::HTML_INLINE:
			$this->processHtmlInline($domDocument, $nodeStack, $node, $parentDomNode);
			break;
			
		default:
			throw new ArgumentException('Node type is invalid.');
		}
	}
	
	private function processParagraph(DomDocument $domDocument, IList $nodeStack,
			Paragraph $paragraph, DomNode $parentDomNode) {
		$element = $domDocument->createElement('paragraph');
		$parentDomNode->appendChild($element);
		$inlineContent = $paragraph->getInlineContent();
		if ($inlineContent != NULL) {
			self::pushNodesOntoStack($nodeStack, $inlineContent, $element);
		}
	}
	
	private function processHeading(DomDocument $domDocument, IList $nodeStack,
			Heading $heading, DomNode $parentDomNode) {
		$element = $domDocument->createElement('heading');
		$element->setAttribute('level', $heading->getLevel());
		$parentDomNode->appendChild($element);
		$inlineContent = $heading->getInlineContent();
		if ($inlineContent != NULL) {
			self::pushNodesOntoStack($nodeStack, $inlineContent, $element);
		}
	}
	
	private function processList(DomDocument $domDocument, IList $nodeStack,
			BlockList $list, DomNode $parentDomNode) {
		$element = $domDocument->createElement('list');
		$isOrdered = $list->isOrdered();
		$element->setAttribute('ordered', self::convertBoolToXmlBoolean($isOrdered));
		if ($isOrdered) {
			$element->setAttribute('start-number', $list->getStartNumber());
		}
		$element->setAttribute('tight', self::convertBoolToXmlBoolean($list->isTight()));
		$items = $list->getItems();
		if ($items != NULL) {
			foreach ($items as $item) {
				$itemElement = $domDocument->createElement('item');
				$element->appendChild($itemElement);
				$blocks = $item->getBlocks();
				if ($blocks != NULL) {
					self::pushNodesOntoStack($nodeStack, $blocks, $itemElement);
				}
			}
		}
		$parentDomNode->appendChild($element);
	}
	
	private function processCodeBlock(DomDocument $domDocument, IList $nodeStack,
			CodeBlock $codeBlock, DomNode $parentDomNode) {
		$element = $domDocument->createElement('code-block');
		$info = $codeBlock->getInfo();
		if ($info !== NULL) {
			$element->setAttribute('info', $info);
		}
		$element->appendChild($domDocument->createTextNode($codeBlock->getCodeText()));
		$parentDomNode->appendChild($element);
	}
	
	private function processTable(DomDocument $domDocument, IList $nodeStack,
			Table $table, DomNode $parentDomNode) {
		$element = $domDocument->createElement('table');
		$columns = $table->getColumns();
		if ($columns != NULL) {
			foreach ($columns as $column) {
				$columnElement = $domDocument->createElement('column');
				$textAlignment = $column->getTextAlignment();
				if ($textAlignment != TextAlignment::NONE) {
					$textAlignment = StringUtils::toLower(TextAlignment::getName($textAlignment));
					$columnElement->setAttribute('text-alignment', $textAlignment);
				}
				$element->appendChild($columnElement);
				$headerInlineContent = $column->getHeaderInlineContent();
				if ($headerInlineContent != NULL) {
					self::pushNodesOntoStack($nodeStack, $headerInlineContent, $columnElement);
				}
			}
		}
		$rows = $table->getRows();
		if ($rows != NULL) {
			foreach ($rows as $row) {
				$rowElement = $domDocument->createElement('row');
				$cellInlineContents = $row->getCellInlineContents();
				if ($cellInlineContents != NULL) {
					foreach ($cellInlineContents as $cellInlineContent) {
						$cellElement = $domDocument->createElement('cell');
						$rowElement->appendChild($cellElement);
						self::pushNodesOntoStack($nodeStack, $cellInlineContent, $cellElement);
					}
				}
				$element->appendChild($rowElement);
			}
		}
		$parentDomNode->appendChild($element);
	}
	
	private function processBlockQuote(DomDocument $domDocument, IList $nodeStack,
			BlockQuote $blockQuote, DomNode $parentDomNode) {
		$element = $domDocument->createElement('block-quote');
		$parentDomNode->appendChild($element);
		$blocks = $blockQuote->getBlocks();
		if ($blocks != NULL) {
			self::pushNodesOntoStack($nodeStack, $blocks, $element);
		}
	}
	
	private function processThematicBreak(DomDocument $domDocument, IList $nodeStack,
			ThematicBreak $thematicBreak, DomNode $parentDomNode) {
		$parentDomNode->appendChild($domDocument->createElement('thematic-break'));
	}
	
	private function processHtmlBlock(DomDocument $domDocument, IList $nodeStack,
			HtmlBlock $htmlBlock, DomNode $parentDomNode) {
		$element = $domDocument->createElement('html-block');
		$element->appendChild($domDocument->createTextNode($htmlBlock->getMarkup()));
		$parentDomNode->appendChild($element);
	}
	
	private function processText(DomDocument $domDocument, IList $nodeStack,
			Text $text, DomNode $parentDomNode) {
		$element = $domDocument->createElement('text');
		$element->appendChild($domDocument->createTextNode($text->getContent()));
		$parentDomNode->appendChild($element);
	}
	
	private function processEmphasis(DomDocument $domDocument, IList $nodeStack,
			Emphasis $emphasis, DomNode $parentDomNode) {
		$element = $domDocument->createElement('emphasis');
		$parentDomNode->appendChild($element);
		$inlineContent = $emphasis->getInlineContent();
		if ($inlineContent != NULL) {
			self::pushNodesOntoStack($nodeStack, $inlineContent, $element);
		}
	}
	
	private function processStrongEmphasis(DomDocument $domDocument, IList $nodeStack,
			StrongEmphasis $strongEmphasis, DomNode $parentDomNode) {
		$element = $domDocument->createElement('strong-emphasis');
		$parentDomNode->appendChild($element);
		$inlineContent = $strongEmphasis->getInlineContent();
		if ($inlineContent != NULL) {
			self::pushNodesOntoStack($nodeStack, $inlineContent, $element);
		}
	}
	
	private function processLink(DomDocument $domDocument, IList $nodeStack,
			Link $link, DomNode $parentDomNode) {
		$element = $domDocument->createElement('link');
		$element->setAttribute('destination', $link->getDestination());
		$title = $link->getTitle();
		if ($title !== NULL) {
			$element->setAttribute('title', $title);
		}
		$parentDomNode->appendChild($element);
		$inlineContent = $link->getInlineContent();
		if ($inlineContent != NULL) {
			self::pushNodesOntoStack($nodeStack, $inlineContent, $element);
		}
	}
	
	private function processImage(DomDocument $domDocument, IList $nodeStack,
			Image $image, DomNode $parentDomNode) {
		$element = $domDocument->createElement('image');
		$element->setAttribute('destination', $image->getDestination());
		$title = $image->getTitle();
		if ($title !== NULL) {
			$element->setAttribute('title', $title);
		}
		$parentDomNode->appendChild($element);
		$inlineContent = $image->getInlineContent();
		if ($inlineContent != NULL) {
			self::pushNodesOntoStack($nodeStack, $inlineContent, $element);
		}
	}
	
	private function processCodeSpan(DomDocument $domDocument, IList $nodeStack,
			CodeSpan $codeSpan, DomNode $parentDomNode) {
		$element = $domDocument->createElement('code-span');
		$element->appendChild($domDocument->createTextNode($codeSpan->getCodeText()));
		$parentDomNode->appendChild($element);
	}
	
	private function processHardLineBreak(DomDocument $domDocument, IList $nodeStack,
			HardLineBreak $hardLineBreak, DomNode $parentDomNode) {
		$parentDomNode->appendChild($domDocument->createElement('hard-line-break'));
	}
	
	private function processSoftLineBreak(DomDocument $domDocument, IList $nodeStack,
			SoftLineBreak $softLineBreak, DomNode $parentDomNode) {
		$parentDomNode->appendChild($domDocument->createElement('soft-line-break'));
	}
	
	private function processHtmlInline(DomDocument $domDocument, IList $nodeStack,
			HtmlInline $htmlInline, DomNode $parentDomNode) {
		$element = $domDocument->createElement('html-inline');
		$element->appendChild($domDocument->createTextNode($htmlInline->getMarkup()));
		$parentDomNode->appendChild($element);
	}
	
	private static function convertBoolToXmlBoolean($value) {
		return $value ? 'true' : 'false';
	}
	
}

?>
