<?php

namespace VTCompose\Time;

use VTCompose\Exception\InvalidOperationException;

/**
 * Provides a way to measure elapsed time.
 *
 * Elapsed time is measured as the difference between results of calls to the {@link http://php.net/microtime
 * microtime()} function.
 */
class Stopwatch {

	private $elapsedTime;
	private $startTime;

	private function initialize() {
		$this->elapsedTime = $this->startTime = 0.0;
	}

	private function getCurrentElapsedTime() {
		return microtime(true) - $this->startTime;
	}

	/**
	 * Initializes a new object.
	 *
	 * 
	 */
	public function __construct() {
		$this->initialize();
	}

	/**
	 * Starts, or resumes, measuring elapsed time for an interval.
	 *
	 * The measuring can be subsequently stopped by calling the stop() method or restarted by calling the
	 * restart() method.
	 *
	 * @throws InvalidOperationException if the object represents an already running stopwatch.
	 * @see stop() 
	 * @see restart() 
	 */
	public function start() {
		if ($this->isRunning()) {
			throw new InvalidOperationException('Stopwatch already running.');
		}

		$this->startTime = microtime(true);
	}

	/**
	 * Stops measuring elapsed time for an interval.
	 *
	 * It is required that the measuring has been previously started, typically by calling the start() method.
	 *
	 * @throws InvalidOperationException if the object represents a not running stopwatch.
	 * @see start() 
	 */
	public function stop() {
		if (!$this->isRunning()) {
			throw new InvalidOperationException('Stopwatch not running.');
		}

		$this->elapsedTime += $this->getCurrentElapsedTime();
		$this->startTime = 0.0;
	}

	/**
	 * Stops time interval measurement and resets the elapsed time to zero.
	 *
	 * Method returns the object to its initial state, i.e. stops the measurement, if running at all, and
	 * resets the elapsed time to zero.
	 *
	 * @see restart() 
	 */
	public function reset() {
		$this->initialize();
	}

	/**
	 * Stops time interval measurement, resets the elapsed time to zero and starts measuring elapsed time.
	 *
	 * Method achieves its purpose by simply calling the reset() and the start() methods in that order.
	 *
	 * @see reset() 
	 * @see start() 
	 */
	public function restart() {
		$this->reset();
		$this->start();
	}

	/**
	 * Returns the total elapsed time measured by the object.
	 *
	 * Method can be called regardless of the state of the object, i.e. it is not necessary to stop the
	 * measuring in order to ask for the elapsed time.
	 *
	 * @return float the total elapsed time measured by the object, in seconds
	 */
	public function getElapsedTime() {
		$elapsedTime = $this->elapsedTime;
		if ($this->isRunning()) {
			$elapsedTime += $this->getCurrentElapsedTime();
		}
		
		return $elapsedTime;
	}

	/**
	 * Returns a value indicating whether the object represents a running stopwatch.
	 *
	 * A Stopwatch object typically begins running with a call to the start() method and stops running with a
	 * call to the stop() method.
	 *
	 * @return bool true if the object represents a running stopwatch, otherwise false
	 * @see start() 
	 * @see stop() 
	 */
	public function isRunning() {
		return $this->startTime != 0.0;
	}

}

?>
