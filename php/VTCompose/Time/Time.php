<?php

namespace VTCompose\Time;

use DateTime;
use DateTimeZone;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
final class Time {

	/**
	 * 
	 *
	 * 
	 *
	 * @param int Unix timestamp
	 * @param string 
	 * @param string 
	 * @return string formatted timestamp
	 */
	public static function formatTimestamp($timestamp, $timezone = 'UTC', $format = 'j/n/Y, H:i:s') {
		$dateTime = new DateTime('@' . $timestamp);
		$dateTime->setTimezone(new DateTimeZone($timezone));
		return $dateTime->format($format);
	}
	
	private function __construct() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}

}

?>
