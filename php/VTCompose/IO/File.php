<?php

namespace VTCompose\IO;

use Throwable;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\NotSupportedException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
final class File {

	private static function pathIsValid($path) {
		return StringUtils::length($path) > 0;
	}
	
	private static function validatePath($path) {
		if (!self::pathIsValid($path)) {
			throw new ArgumentException('Path is invalid.');
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public static function exists($path) {
		if (!self::pathIsValid($path)) {
			return false;
		}
		
		try {
			return file_exists($path);
		} catch (Throwable $exception) {
			return false;
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return int 
	 * @throws IOException .
	 */
	public static function getType($path) {
		self::validatePath($path);
		
		switch (filetype($path)) {
		case 'fifo':
			return FileType::NAMED_PIPE;
		
		case 'char':
			return FileType::CHARACTER_SPECIAL;
			
		case 'dir':
			return FileType::DIRECTORY;
			
		case 'block':
			return FileType::BLOCK_SPECIAL;
			
		case 'file':
			return FileType::REGULAR;
			
		case 'link':
			return FileType::SYMBOLIC_LINK;
			
		case 'socket':
			return FileType::SOCKET;
			
		default:
			throw new IOException('Failed to get the type of a file.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 * @throws IOException .
	 */
	public static function readAllText($path) {
		self::validatePath($path);
	
		$result = file_get_contents($path);
		if ($result === false) {
			throw new IOException('Failed to read all lines of a file.');
		}
		
		return $result;
	}
	
	private function __construct() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}

}

?>
