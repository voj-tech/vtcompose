<?php

namespace VTCompose\IO;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class FileType extends Enum {
	
	/**
	 * 
	 *
	 * 
	 */
	const NAMED_PIPE = 0010000;
	
	/**
	 * 
	 *
	 * 
	 */
	const CHARACTER_SPECIAL = 0020000;
	
	/**
	 * 
	 *
	 * 
	 */
	const DIRECTORY = 0040000;
	
	/**
	 * 
	 *
	 * 
	 */
	const BLOCK_SPECIAL = 0060000;
	
	/**
	 * 
	 *
	 * 
	 */
	const REGULAR = 0100000;
	
	/**
	 * 
	 *
	 * 
	 */
	const SYMBOLIC_LINK = 0120000;
	
	/**
	 * 
	 *
	 * 
	 */
	const SOCKET = 0140000;
	
}

?>
