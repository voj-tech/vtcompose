<?php

namespace VTCompose\Logging;

use VTCompose\IO\IOException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
class FileLogger implements ILogger {

	/**
	 * 
	 *
	 * 
	 */
	const ENTRY_FORMAT_TIME_PLACEHOLDER = '%t';
	
	/**
	 * 
	 *
	 * 
	 */
	const ENTRY_FORMAT_SEVERITY_PLACEHOLDER = '%s';
	
	/**
	 * 
	 *
	 * 
	 */
	const ENTRY_FORMAT_MESSAGE_PLACEHOLDER = '%m';

	private $filename;
	private $entryFormat;
	private $timeFormat;

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param string 
	 */
	public function __construct($filename, $entryFormat = self::ENTRY_FORMAT_TIME_PLACEHOLDER . '; ' .
			self::ENTRY_FORMAT_SEVERITY_PLACEHOLDER . '; ' . self::ENTRY_FORMAT_MESSAGE_PLACEHOLDER .
			StringUtils::LF, $timeFormat = 'Y-m-d H:i:s') {
		$this->filename = $filename;
		$this->entryFormat = $entryFormat;
		$this->timeFormat = $timeFormat;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param string 
	 * @param mixed 
	 * @throws IOException if writing the log entry fails.
	 */
	public function log($severity, $message, ...$parameters) {
		$time = date($this->timeFormat);
		$severityName = StringUtils::toLower(Severity::getName($severity));
		$message = sprintf($message, ...$parameters);
		
		static $searchArray = [
			self::ENTRY_FORMAT_TIME_PLACEHOLDER,
			self::ENTRY_FORMAT_SEVERITY_PLACEHOLDER,
			self::ENTRY_FORMAT_MESSAGE_PLACEHOLDER,
		];
		
		$entry = str_replace($searchArray, [$time, $severityName, $message], $this->entryFormat);
		
		if (file_put_contents($this->filename, $entry, FILE_APPEND | LOCK_EX) === false) {
			throw new IOException('Failed to write a log entry.');
		}
	}

}

?>
