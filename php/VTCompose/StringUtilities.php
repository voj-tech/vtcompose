<?php

namespace VTCompose;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;
use VTCompose\Exception\Exception;
use VTCompose\Exception\NotSupportedException;
use VTCompose\Text\RegularExpressions\Regex;
use VTCompose\Text\RegularExpressions\RegexOptions;

/**
 * Collection of static methods for string manipulation and some basic string constants.
 *
 * 
 */
final class StringUtilities {

	/**
	 * 
	 *
	 * 
	 */
	const LF = "\n";

	/**
	 * 
	 *
	 * 
	 */
	const CR = "\r";

	/**
	 * 
	 *
	 * 
	 */
	const CRLF = self::CR . self::LF;

	/**
	 * 
	 *
	 * 
	 */
	const SPACE = ' ';
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return int 
	 * @throws Exception .
	 */
	public static function length($string) {
		$result = mb_strlen($string);
		if ($result === false) {
			throw new Exception('Unable to get string length.');
		}
		
		return $result;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 * @param int 
	 * @return string 
	 * @throws Exception .
	 */
	public static function substring($string, $startIndex, $length = NULL) {
		$result = mb_substr($string, $startIndex, $length);
		if ($result === false) {
			throw new Exception('Unable to retrieve a substring of a string.');
		}
		
		return $result;
	}

	private static function getToLowerReplacePairs() {
		static $replacePairs = ['A'=>'a', 'B'=>'b', 'C'=>'c', 'D'=>'d', 'E'=>'e', 'F'=>'f', 'G'=>'g',
				'H'=>'h', 'I'=>'i', 'J'=>'j', 'K'=>'k', 'L'=>'l', 'M'=>'m', 'N'=>'n', 'O'=>'o', 'P'=>'p',
				'Q'=>'q', 'R'=>'r', 'S'=>'s', 'T'=>'t', 'U'=>'u', 'V'=>'v', 'W'=>'w', 'X'=>'x', 'Y'=>'y',
				'Z'=>'z', 'À'=>'à', 'Á'=>'á', 'Â'=>'â', 'Ã'=>'ã', 'Ä'=>'ä', 'Å'=>'å', 'Æ'=>'æ', 'Ç'=>'ç',
				'È'=>'è', 'É'=>'é', 'Ê'=>'ê', 'Ë'=>'ë', 'Ì'=>'ì', 'Í'=>'í', 'Î'=>'î', 'Ï'=>'ï', 'Ð'=>'ð',
				'Ñ'=>'ñ', 'Ò'=>'ò', 'Ó'=>'ó', 'Ô'=>'ô', 'Õ'=>'õ', 'Ö'=>'ö', 'Ø'=>'ø', 'Ù'=>'ù', 'Ú'=>'ú',
				'Û'=>'û', 'Ü'=>'ü', 'Ý'=>'ý', 'Þ'=>'þ', 'Ā'=>'ā', 'Ă'=>'ă', 'Ą'=>'ą', 'Ć'=>'ć', 'Ĉ'=>'ĉ',
				'Ċ'=>'ċ', 'Č'=>'č', 'Ď'=>'ď', 'Đ'=>'đ', 'Ē'=>'ē', 'Ĕ'=>'ĕ', 'Ė'=>'ė', 'Ę'=>'ę', 'Ě'=>'ě',
				'Ĝ'=>'ĝ', 'Ğ'=>'ğ', 'Ġ'=>'ġ', 'Ģ'=>'ģ', 'Ĥ'=>'ĥ', 'Ħ'=>'ħ', 'Ĩ'=>'ĩ', 'Ī'=>'ī', 'Ĭ'=>'ĭ',
				'Į'=>'į', 'İ'=>'i', 'Ĳ'=>'ĳ', 'Ĵ'=>'ĵ', 'Ķ'=>'ķ', 'Ĺ'=>'ĺ', 'Ļ'=>'ļ', 'Ľ'=>'ľ', 'Ŀ'=>'ŀ',
				'Ł'=>'ł', 'Ń'=>'ń', 'Ņ'=>'ņ', 'Ň'=>'ň', 'Ŋ'=>'ŋ', 'Ō'=>'ō', 'Ŏ'=>'ŏ', 'Ő'=>'ő', 'Œ'=>'œ',
				'Ŕ'=>'ŕ', 'Ŗ'=>'ŗ', 'Ř'=>'ř', 'Ś'=>'ś', 'Ŝ'=>'ŝ', 'Ş'=>'ş', 'Š'=>'š', 'Ţ'=>'ţ', 'Ť'=>'ť',
				'Ŧ'=>'ŧ', 'Ũ'=>'ũ', 'Ū'=>'ū', 'Ŭ'=>'ŭ', 'Ů'=>'ů', 'Ű'=>'ű', 'Ų'=>'ų', 'Ŵ'=>'ŵ', 'Ŷ'=>'ŷ',
				'Ÿ'=>'ÿ', 'Ź'=>'ź', 'Ż'=>'ż', 'Ž'=>'ž', 'Ɓ'=>'ɓ', 'Ƃ'=>'ƃ', 'Ƅ'=>'ƅ', 'Ɔ'=>'ɔ', 'Ƈ'=>'ƈ',
				'Ɗ'=>'ɗ', 'Ƌ'=>'ƌ', 'Ǝ'=>'ɘ', 'Ə'=>'ə', 'Ɛ'=>'ɛ', 'Ƒ'=>'ƒ', 'Ɠ'=>'ɠ', 'Ɣ'=>'ɣ', 'Ɩ'=>'ɩ',
				'Ɨ'=>'ɨ', 'Ƙ'=>'ƙ', 'Ɯ'=>'ɯ', 'Ɲ'=>'ɲ', 'Ơ'=>'ơ', 'Ƣ'=>'ƣ', 'Ƥ'=>'ƥ', 'Ƨ'=>'ƨ', 'Ʃ'=>'ʃ',
				'Ƭ'=>'ƭ', 'Ʈ'=>'ʈ', 'Ư'=>'ư', 'Ʊ'=>'ʊ', 'Ʋ'=>'ʋ', 'Ƴ'=>'ƴ', 'Ƶ'=>'ƶ', 'Ʒ'=>'ʒ', 'Ƹ'=>'ƹ',
				'Ƽ'=>'ƽ', 'Ǆ'=>'ǆ', 'ǅ'=>'ǆ', 'Ǉ'=>'ǉ', 'ǈ'=>'ǉ', 'Ǌ'=>'ǌ', 'ǋ'=>'ǌ', 'Ǎ'=>'ǎ', 'Ǐ'=>'ǐ',
				'Ǒ'=>'ǒ', 'Ǔ'=>'ǔ', 'Ǖ'=>'ǖ', 'Ǘ'=>'ǘ', 'Ǚ'=>'ǚ', 'Ǜ'=>'ǜ', 'Ǟ'=>'ǟ', 'Ǡ'=>'ǡ', 'Ǣ'=>'ǣ',
				'Ǥ'=>'ǥ', 'Ǧ'=>'ǧ', 'Ǩ'=>'ǩ', 'Ǫ'=>'ǫ', 'Ǭ'=>'ǭ', 'Ǯ'=>'ǯ', 'Ǳ'=>'ǳ', 'ǲ'=>'ǳ', 'Ǵ'=>'ǵ',
				'Ƿ'=>'ƿ', 'Ǹ'=>'ǹ', 'Ǻ'=>'ǻ', 'Ǽ'=>'ǽ', 'Ǿ'=>'ǿ', 'Ȁ'=>'ȁ', 'Ȃ'=>'ȃ', 'Ȅ'=>'ȅ', 'Ȇ'=>'ȇ',
				'Ȉ'=>'ȉ', 'Ȋ'=>'ȋ', 'Ȍ'=>'ȍ', 'Ȏ'=>'ȏ', 'Ȑ'=>'ȑ', 'Ȓ'=>'ȓ', 'Ȕ'=>'ȕ', 'Ȗ'=>'ȗ', 'Ș'=>'ș',
				'Ț'=>'ț', 'Ȝ'=>'ȝ', 'Ȟ'=>'ȟ', 'Ƞ'=>'ƞ', 'Ȣ'=>'ȣ', 'Ȥ'=>'ȥ', 'Ȧ'=>'ȧ', 'Ȩ'=>'ȩ', 'Ȫ'=>'ȫ',
				'Ȭ'=>'ȭ', 'Ȯ'=>'ȯ', 'Ȱ'=>'ȱ', 'Ȳ'=>'ȳ', 'Ⱥ'=>'ⱥ', 'Ȼ'=>'ȼ', 'Ƚ'=>'ƚ', 'Ⱦ'=>'ⱦ', 'Ɂ'=>'ɂ',
				'Ƀ'=>'ƀ', 'Ʉ'=>'ʉ', 'Ʌ'=>'ʌ', 'Ɇ'=>'ɇ', 'Ɉ'=>'ɉ', 'Ɋ'=>'ɋ', 'Ɍ'=>'ɍ', 'Ɏ'=>'ɏ', 'Ḁ'=>'ḁ',
				'Ḃ'=>'ḃ', 'Ḅ'=>'ḅ', 'Ḇ'=>'ḇ', 'Ḉ'=>'ḉ', 'Ḋ'=>'ḋ', 'Ḍ'=>'ḍ', 'Ḏ'=>'ḏ', 'Ḑ'=>'ḑ', 'Ḓ'=>'ḓ',
				'Ḕ'=>'ḕ', 'Ḗ'=>'ḗ', 'Ḙ'=>'ḙ', 'Ḛ'=>'ḛ', 'Ḝ'=>'ḝ', 'Ḟ'=>'ḟ', 'Ḡ'=>'ḡ', 'Ḣ'=>'ḣ', 'Ḥ'=>'ḥ',
				'Ḧ'=>'ḧ', 'Ḩ'=>'ḩ', 'Ḫ'=>'ḫ', 'Ḭ'=>'ḭ', 'Ḯ'=>'ḯ', 'Ḱ'=>'ḱ', 'Ḳ'=>'ḳ', 'Ḵ'=>'ḵ', 'Ḷ'=>'ḷ',
				'Ḹ'=>'ḹ', 'Ḻ'=>'ḻ', 'Ḽ'=>'ḽ', 'Ḿ'=>'ḿ', 'Ṁ'=>'ṁ', 'Ṃ'=>'ṃ', 'Ṅ'=>'ṅ', 'Ṇ'=>'ṇ', 'Ṉ'=>'ṉ',
				'Ṋ'=>'ṋ', 'Ṍ'=>'ṍ', 'Ṏ'=>'ṏ', 'Ṑ'=>'ṑ', 'Ṓ'=>'ṓ', 'Ṕ'=>'ṕ', 'Ṗ'=>'ṗ', 'Ṙ'=>'ṙ', 'Ṛ'=>'ṛ',
				'Ṝ'=>'ṝ', 'Ṟ'=>'ṟ', 'Ṡ'=>'ṡ', 'Ṣ'=>'ṣ', 'Ṥ'=>'ṥ', 'Ṧ'=>'ṧ', 'Ṩ'=>'ṩ', 'Ṫ'=>'ṫ', 'Ṭ'=>'ṭ',
				'Ṯ'=>'ṯ', 'Ṱ'=>'ṱ', 'Ṳ'=>'ṳ', 'Ṵ'=>'ṵ', 'Ṷ'=>'ṷ', 'Ṹ'=>'ṹ', 'Ṻ'=>'ṻ', 'Ṽ'=>'ṽ', 'Ṿ'=>'ṿ',
				'Ẁ'=>'ẁ', 'Ẃ'=>'ẃ', 'Ẅ'=>'ẅ', 'Ẇ'=>'ẇ', 'Ẉ'=>'ẉ', 'Ẋ'=>'ẋ', 'Ẍ'=>'ẍ', 'Ẏ'=>'ẏ', 'Ẑ'=>'ẑ',
				'Ẓ'=>'ẓ', 'Ẕ'=>'ẕ', 'ẜ'=>'ß', 'Ạ'=>'ạ', 'Ả'=>'ả', 'Ấ'=>'ấ', 'Ầ'=>'ầ', 'Ẩ'=>'ẩ', 'Ẫ'=>'ẫ',
				'Ậ'=>'ậ', 'Ắ'=>'ắ', 'Ằ'=>'ằ', 'Ẳ'=>'ẳ', 'Ẵ'=>'ẵ', 'Ặ'=>'ặ', 'Ẹ'=>'ẹ', 'Ẻ'=>'ẻ', 'Ẽ'=>'ẽ',
				'Ế'=>'ế', 'Ề'=>'ề', 'Ể'=>'ể', 'Ễ'=>'ễ', 'Ệ'=>'ệ', 'Ỉ'=>'ỉ', 'Ị'=>'ị', 'Ọ'=>'ọ', 'Ỏ'=>'ỏ',
				'Ố'=>'ố', 'Ồ'=>'ồ', 'Ổ'=>'ổ', 'Ỗ'=>'ỗ', 'Ộ'=>'ộ', 'Ớ'=>'ớ', 'Ờ'=>'ờ', 'Ở'=>'ở', 'Ỡ'=>'ỡ',
				'Ợ'=>'ợ', 'Ụ'=>'ụ', 'Ủ'=>'ủ', 'Ứ'=>'ứ', 'Ừ'=>'ừ', 'Ử'=>'ử', 'Ữ'=>'ữ', 'Ự'=>'ự', 'Ỳ'=>'ỳ',
				'Ỵ'=>'ỵ', 'Ỷ'=>'ỷ', 'Ỹ'=>'ỹ', 'Ⱡ'=>'ⱡ', 'Ɫ'=>'ɫ', 'Ᵽ'=>'ᵽ', 'Ɽ'=>'ɽ', 'Ⱨ'=>'ⱨ', 'Ⱪ'=>'ⱪ',
				'Ⱬ'=>'ⱬ', 'Ⱶ'=>'ⱶ'];
				
		return $replacePairs;
	}

	private static function getToUpperReplacePairs() {
		static $replacePairs = NULL;
		if ($replacePairs === NULL) {
			$replacePairs = self::getToLowerReplacePairs();
			unset($replacePairs['İ']);
			unset($replacePairs['ǅ']);
			unset($replacePairs['ǈ']);
			unset($replacePairs['ǋ']);
			unset($replacePairs['ǲ']);
			$replacePairs = array_flip($replacePairs);
			$replacePairs['ı'] = 'I';
			$replacePairs['ǅ'] = 'Ǆ';
			$replacePairs['ǈ'] = 'Ǉ';
			$replacePairs['ǋ'] = 'Ǌ';
			$replacePairs['ǲ'] = 'Ǳ';
		}
		
		return $replacePairs;
	}

	private static function getRemoveDiacriticReplacePairs() {
		static $replacePairs = ['À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'AE',
				'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I',
				'Ð'=>'DH', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
				'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'TH', 'ß'=>'ss', 'à'=>'a', 'á'=>'a', 'â'=>'a',
				'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'ae', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e',
				'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'dh', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o',
				'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'þ'=>'th',
				'ÿ'=>'y', 'Ā'=>'A', 'ā'=>'a', 'Ă'=>'A', 'ă'=>'a', 'Ą'=>'A', 'ą'=>'a', 'Ć'=>'C', 'ć'=>'c',
				'Ĉ'=>'C', 'ĉ'=>'c', 'Ċ'=>'C', 'ċ'=>'c', 'Č'=>'C', 'č'=>'c', 'Ď'=>'D', 'ď'=>'d', 'Đ'=>'D',
				'đ'=>'d', 'Ē'=>'E', 'ē'=>'e', 'Ĕ'=>'E', 'ĕ'=>'e', 'Ė'=>'E', 'ė'=>'e', 'Ę'=>'E', 'ę'=>'e',
				'Ě'=>'E', 'ě'=>'e', 'Ĝ'=>'G', 'ĝ'=>'g', 'Ğ'=>'G', 'ğ'=>'g', 'Ġ'=>'G', 'ġ'=>'g', 'Ģ'=>'G',
				'ģ'=>'g', 'Ĥ'=>'H', 'ĥ'=>'h', 'Ħ'=>'H', 'ħ'=>'h', 'Ĩ'=>'I', 'ĩ'=>'i', 'Ī'=>'I', 'ī'=>'i',
				'Ĭ'=>'I', 'ĭ'=>'i', 'Į'=>'I', 'į'=>'i', 'İ'=>'I', 'ı'=>'i', 'Ĳ'=>'IJ', 'ĳ'=>'ij', 'Ĵ'=>'J',
				'ĵ'=>'j', 'Ķ'=>'K', 'ķ'=>'k', 'ĸ'=>'q', 'Ĺ'=>'L', 'ĺ'=>'l', 'Ļ'=>'L', 'ļ'=>'l', 'Ľ'=>'L',
				'ľ'=>'l', 'Ŀ'=>'L', 'ŀ'=>'l', 'Ł'=>'L', 'ł'=>'l', 'Ń'=>'N', 'ń'=>'n', 'Ņ'=>'N', 'ņ'=>'n',
				'Ň'=>'N', 'ň'=>'n', 'ŉ'=>'n', 'Ŋ'=>'N', 'ŋ'=>'n', 'Ō'=>'O', 'ō'=>'o', 'Ŏ'=>'O', 'ŏ'=>'o',
				'Ő'=>'O', 'ő'=>'o', 'Œ'=>'OE', 'œ'=>'oe', 'Ŕ'=>'R', 'ŕ'=>'r', 'Ŗ'=>'R', 'ŗ'=>'r', 'Ř'=>'R',
				'ř'=>'r', 'Ś'=>'S', 'ś'=>'s', 'Ŝ'=>'S', 'ŝ'=>'s', 'Ş'=>'S', 'ş'=>'s', 'Š'=>'S', 'š'=>'s',
				'Ţ'=>'T', 'ţ'=>'t', 'Ť'=>'T', 'ť'=>'t', 'Ŧ'=>'T', 'ŧ'=>'t', 'Ũ'=>'U', 'ũ'=>'u', 'Ū'=>'U',
				'ū'=>'u', 'Ŭ'=>'U', 'ŭ'=>'u', 'Ů'=>'U', 'ů'=>'u', 'Ű'=>'U', 'ű'=>'u', 'Ų'=>'U', 'ų'=>'u',
				'Ŵ'=>'W', 'ŵ'=>'w', 'Ŷ'=>'Y', 'ŷ'=>'y', 'Ÿ'=>'Y', 'Ź'=>'Z', 'ź'=>'z', 'Ż'=>'Z', 'ż'=>'z',
				'Ž'=>'Z', 'ž'=>'z', 'ſ'=>'s', 'ƀ'=>'b', 'Ɓ'=>'B', 'Ƃ'=>'B', 'ƃ'=>'b', 'Ƅ'=>'B', 'ƅ'=>'b',
				'Ɔ'=>'C', 'Ƈ'=>'C', 'ƈ'=>'c', 'Ɖ'=>'D', 'Ɗ'=>'D', 'Ƌ'=>'D', 'ƌ'=>'d', 'ƍ'=>'d', 'Ǝ'=>'E',
				'Ə'=>'E', 'Ɛ'=>'E', 'Ƒ'=>'F', 'ƒ'=>'f', 'Ɠ'=>'G', 'Ɣ'=>'G', 'ƕ'=>'h', 'Ɩ'=>'I', 'Ɨ'=>'I',
				'Ƙ'=>'K', 'ƙ'=>'k', 'ƚ'=>'l', 'ƛ'=>'l', 'Ɯ'=>'M', 'Ɲ'=>'N', 'ƞ'=>'n', 'Ɵ'=>'O', 'Ơ'=>'O',
				'ơ'=>'o', 'Ƣ'=>'OI', 'ƣ'=>'oi', 'Ƥ'=>'P', 'ƥ'=>'p', 'Ʀ'=>'R', 'Ƨ'=>'S', 'ƨ'=>'s', 'Ʃ'=>'SH',
				'ƪ'=>'sh', 'ƫ'=>'t', 'Ƭ'=>'T', 'ƭ'=>'t', 'Ʈ'=>'T', 'Ư'=>'U', 'ư'=>'u', 'Ʊ'=>'U', 'Ʋ'=>'V',
				'Ƴ'=>'Y', 'ƴ'=>'y', 'Ƶ'=>'Z', 'ƶ'=>'z', 'Ʒ'=>'Z', 'Ƹ'=>'Z', 'ƹ'=>'z', 'ƺ'=>'z', 'ƻ'=>'2',
				'Ƽ'=>'5', 'ƽ'=>'5', 'Ǆ'=>'DZ', 'ǅ'=>'Dz', 'ǆ'=>'dz', 'Ǉ'=>'LJ', 'ǈ'=>'Lj', 'ǉ'=>'lj',
				'Ǌ'=>'NJ', 'ǋ'=>'Nj', 'ǌ'=>'nj', 'Ǎ'=>'A', 'ǎ'=>'a', 'Ǐ'=>'I', 'ǐ'=>'i', 'Ǒ'=>'O', 'ǒ'=>'o',
				'Ǔ'=>'U', 'ǔ'=>'u', 'Ǖ'=>'U', 'ǖ'=>'u', 'Ǘ'=>'U', 'ǘ'=>'u', 'Ǚ'=>'U', 'ǚ'=>'u', 'Ǜ'=>'U',
				'ǜ'=>'u', 'ǝ'=>'e', 'Ǟ'=>'A', 'ǟ'=>'a', 'Ǡ'=>'A', 'ǡ'=>'a', 'Ǣ'=>'AE', 'ǣ'=>'ae', 'Ǥ'=>'G',
				'ǥ'=>'g', 'Ǧ'=>'G', 'ǧ'=>'g', 'Ǩ'=>'K', 'ǩ'=>'k', 'Ǫ'=>'O', 'ǫ'=>'o', 'Ǭ'=>'O', 'ǭ'=>'o',
				'Ǯ'=>'Z', 'ǯ'=>'z', 'ǰ'=>'j', 'Ǳ'=>'DZ', 'ǲ'=>'Dz', 'ǳ'=>'dz', 'Ǵ'=>'G', 'ǵ'=>'g', 'Ƕ'=>'H',
				'Ǹ'=>'N', 'ǹ'=>'n', 'Ǻ'=>'A', 'ǻ'=>'a', 'Ǽ'=>'AE', 'ǽ'=>'ae', 'Ǿ'=>'O', 'ǿ'=>'o', 'Ȁ'=>'A',
				'ȁ'=>'a', 'Ȃ'=>'A', 'ȃ'=>'a', 'Ȅ'=>'E', 'ȅ'=>'e', 'Ȇ'=>'E', 'ȇ'=>'e', 'Ȉ'=>'I', 'ȉ'=>'i',
				'Ȋ'=>'I', 'ȋ'=>'i', 'Ȍ'=>'O', 'ȍ'=>'o', 'Ȏ'=>'O', 'ȏ'=>'o', 'Ȑ'=>'R', 'ȑ'=>'r', 'Ȓ'=>'R',
				'ȓ'=>'r', 'Ȕ'=>'U', 'ȕ'=>'u', 'Ȗ'=>'U', 'ȗ'=>'u', 'Ș'=>'S', 'ș'=>'s', 'Ț'=>'T', 'ț'=>'t',
				'Ȝ'=>'Y', 'ȝ'=>'y', 'Ȟ'=>'H', 'ȟ'=>'h', 'Ƞ'=>'N', 'ȡ'=>'d', 'Ȥ'=>'Z', 'ȥ'=>'z', 'Ȧ'=>'A',
				'ȧ'=>'a', 'Ȩ'=>'E', 'ȩ'=>'e', 'Ȫ'=>'O', 'ȫ'=>'o', 'Ȭ'=>'O', 'ȭ'=>'o', 'Ȯ'=>'O', 'ȯ'=>'o',
				'Ȱ'=>'O', 'ȱ'=>'o', 'Ȳ'=>'Y', 'ȳ'=>'y', 'ȴ'=>'l', 'ȵ'=>'n', 'ȶ'=>'t', 'ȷ'=>'j', 'ȸ'=>'db',
				'ȹ'=>'qp', 'Ⱥ'=>'A', 'Ȼ'=>'C', 'ȼ'=>'c', 'Ƚ'=>'L', 'Ⱦ'=>'T', 'ȿ'=>'s', 'ɀ'=>'z', 'Ƀ'=>'B',
				'Ʉ'=>'U', 'Ʌ'=>'V', 'Ɇ'=>'E', 'ɇ'=>'e', 'Ɉ'=>'J', 'ɉ'=>'j', 'Ɋ'=>'Q', 'ɋ'=>'q', 'Ɍ'=>'R',
				'ɍ'=>'r', 'Ɏ'=>'Y', 'ɏ'=>'y', 'ɐ'=>'a', 'ɑ'=>'a', 'ɒ'=>'a', 'ɓ'=>'b', 'ɔ'=>'c', 'ɕ'=>'c',
				'ɖ'=>'d', 'ɗ'=>'d', 'ɘ'=>'e', 'ə'=>'e', 'ɚ'=>'e', 'ɛ'=>'e', 'ɜ'=>'e', 'ɝ'=>'e', 'ɞ'=>'e',
				'ɟ'=>'j', 'ɠ'=>'g', 'ɡ'=>'g', 'ɢ'=>'G', 'ɣ'=>'g', 'ɤ'=>'g', 'ɥ'=>'h', 'ɦ'=>'h', 'ɧ'=>'h',
				'ɨ'=>'i', 'ɩ'=>'i', 'ɪ'=>'I', 'ɫ'=>'l', 'ɬ'=>'l', 'ɭ'=>'l', 'ɮ'=>'lz', 'ɯ'=>'m', 'ɰ'=>'m',
				'ɱ'=>'m', 'ɲ'=>'n', 'ɳ'=>'n', 'ɴ'=>'N', 'ɵ'=>'o', 'ɶ'=>'OE', 'ɷ'=>'o', 'ɸ'=>'o', 'ɹ'=>'r',
				'ɺ'=>'r', 'ɻ'=>'r', 'ɼ'=>'r', 'ɽ'=>'r', 'ɾ'=>'r', 'ɿ'=>'r', 'ʀ'=>'R', 'ʁ'=>'R', 'ʂ'=>'s',
				'ʃ'=>'sh', 'ʄ'=>'j', 'ʅ'=>'sh', 'ʆ'=>'sh', 'ʇ'=>'t', 'ʈ'=>'t', 'ʉ'=>'u', 'ʊ'=>'u', 'ʋ'=>'v',
				'ʌ'=>'v', 'ʍ'=>'w', 'ʎ'=>'y', 'ʏ'=>'Y', 'ʐ'=>'z', 'ʑ'=>'z', 'ʒ'=>'z', 'ʓ'=>'z', 'ʗ'=>'C',
				'ʘ'=>'O', 'ʙ'=>'B', 'ʚ'=>'e', 'ʛ'=>'G', 'ʜ'=>'H', 'ʝ'=>'j', 'ʞ'=>'k', 'ʟ'=>'L', 'ʠ'=>'q',
				'ʣ'=>'dz', 'ʤ'=>'dz', 'ʥ'=>'dz', 'ʦ'=>'ts', 'ʧ'=>'tsh', 'ʨ'=>'tc', 'ʩ'=>'fn', 'ʪ'=>'ls',
				'ʫ'=>'lz', 'ʮ'=>'h', 'ʯ'=>'h', 'ᴀ'=>'A', 'ᴁ'=>'AE', 'ᴂ'=>'ae', 'ᴃ'=>'B', 'ᴄ'=>'C', 'ᴅ'=>'D',
				'ᴆ'=>'D', 'ᴇ'=>'E', 'ᴈ'=>'e', 'ᴉ'=>'i', 'ᴊ'=>'J', 'ᴋ'=>'K', 'ᴌ'=>'L', 'ᴍ'=>'M', 'ᴎ'=>'N',
				'ᴏ'=>'O', 'ᴐ'=>'C', 'ᴑ'=>'o', 'ᴒ'=>'c', 'ᴓ'=>'o', 'ᴔ'=>'oe', 'ᴕ'=>'OU', 'ᴖ'=>'o', 'ᴗ'=>'o',
				'ᴘ'=>'P', 'ᴙ'=>'R', 'ᴚ'=>'R', 'ᴛ'=>'T', 'ᴜ'=>'U', 'ᴝ'=>'u', 'ᴞ'=>'u', 'ᴟ'=>'m', 'ᴠ'=>'V',
				'ᴡ'=>'W', 'ᴢ'=>'Z', 'ᴣ'=>'Z', 'ᵫ'=>'ue', 'ᵬ'=>'b', 'ᵭ'=>'d', 'ᵮ'=>'f', 'ᵯ'=>'m', 'ᵰ'=>'n',
				'ᵱ'=>'p', 'ᵲ'=>'r', 'ᵳ'=>'r', 'ᵴ'=>'s', 'ᵵ'=>'t', 'ᵶ'=>'z', 'ᵷ'=>'g', 'ᵹ'=>'g', 'ᵺ'=>'th',
				'ᵻ'=>'I', 'ᵼ'=>'i', 'ᵽ'=>'p', 'ᵾ'=>'U', 'ᵿ'=>'u', 'ᶀ'=>'b', 'ᶁ'=>'d', 'ᶂ'=>'f', 'ᶃ'=>'g',
				'ᶄ'=>'k', 'ᶅ'=>'l', 'ᶆ'=>'m', 'ᶇ'=>'n', 'ᶈ'=>'p', 'ᶉ'=>'r', 'ᶊ'=>'s', 'ᶋ'=>'sh', 'ᶌ'=>'v',
				'ᶍ'=>'x', 'ᶎ'=>'z', 'ᶏ'=>'a', 'ᶐ'=>'a', 'ᶑ'=>'d', 'ᶒ'=>'e', 'ᶓ'=>'e', 'ᶔ'=>'e', 'ᶕ'=>'e',
				'ᶖ'=>'i', 'ᶗ'=>'c', 'ᶘ'=>'sh', 'ᶙ'=>'u', 'ᶚ'=>'z', 'Ḁ'=>'A', 'ḁ'=>'a', 'Ḃ'=>'B', 'ḃ'=>'b',
				'Ḅ'=>'B', 'ḅ'=>'b', 'Ḇ'=>'B', 'ḇ'=>'b', 'Ḉ'=>'C', 'ḉ'=>'c', 'Ḋ'=>'D', 'ḋ'=>'d', 'Ḍ'=>'D',
				'ḍ'=>'d', 'Ḏ'=>'D', 'ḏ'=>'d', 'Ḑ'=>'D', 'ḑ'=>'d', 'Ḓ'=>'D', 'ḓ'=>'d', 'Ḕ'=>'E', 'ḕ'=>'e',
				'Ḗ'=>'E', 'ḗ'=>'e', 'Ḙ'=>'E', 'ḙ'=>'e', 'Ḛ'=>'E', 'ḛ'=>'e', 'Ḝ'=>'E', 'ḝ'=>'e', 'Ḟ'=>'F',
				'ḟ'=>'f', 'Ḡ'=>'G', 'ḡ'=>'g', 'Ḣ'=>'H', 'ḣ'=>'h', 'Ḥ'=>'H', 'ḥ'=>'h', 'Ḧ'=>'H', 'ḧ'=>'h',
				'Ḩ'=>'H', 'ḩ'=>'h', 'Ḫ'=>'H', 'ḫ'=>'h', 'Ḭ'=>'I', 'ḭ'=>'i', 'Ḯ'=>'I', 'ḯ'=>'i', 'Ḱ'=>'K',
				'ḱ'=>'k', 'Ḳ'=>'K', 'ḳ'=>'k', 'Ḵ'=>'K', 'ḵ'=>'k', 'Ḷ'=>'L', 'ḷ'=>'l', 'Ḹ'=>'L', 'ḹ'=>'l',
				'Ḻ'=>'L', 'ḻ'=>'l', 'Ḽ'=>'L', 'ḽ'=>'l', 'Ḿ'=>'M', 'ḿ'=>'m', 'Ṁ'=>'M', 'ṁ'=>'m', 'Ṃ'=>'M',
				'ṃ'=>'m', 'Ṅ'=>'N', 'ṅ'=>'n', 'Ṇ'=>'N', 'ṇ'=>'n', 'Ṉ'=>'N', 'ṉ'=>'n', 'Ṋ'=>'N', 'ṋ'=>'n',
				'Ṍ'=>'O', 'ṍ'=>'o', 'Ṏ'=>'O', 'ṏ'=>'o', 'Ṑ'=>'O', 'ṑ'=>'o', 'Ṓ'=>'o', 'ṓ'=>'o', 'Ṕ'=>'P',
				'ṕ'=>'p', 'Ṗ'=>'P', 'ṗ'=>'p', 'Ṙ'=>'R', 'ṙ'=>'r', 'Ṛ'=>'R', 'ṛ'=>'r', 'Ṝ'=>'R', 'ṝ'=>'r',
				'Ṟ'=>'R', 'ṟ'=>'r', 'Ṡ'=>'S', 'ṡ'=>'s', 'Ṣ'=>'S', 'ṣ'=>'s', 'Ṥ'=>'S', 'ṥ'=>'s', 'Ṧ'=>'S',
				'ṧ'=>'s', 'Ṩ'=>'S', 'ṩ'=>'s', 'Ṫ'=>'T', 'ṫ'=>'t', 'Ṭ'=>'T', 'ṭ'=>'t', 'Ṯ'=>'T', 'ṯ'=>'t',
				'Ṱ'=>'T', 'ṱ'=>'t', 'Ṳ'=>'U', 'ṳ'=>'u', 'Ṵ'=>'U', 'ṵ'=>'u', 'Ṷ'=>'U', 'ṷ'=>'u', 'Ṹ'=>'U',
				'ṹ'=>'u', 'Ṻ'=>'U', 'ṻ'=>'u', 'Ṽ'=>'V', 'ṽ'=>'v', 'Ṿ'=>'V', 'ṿ'=>'v', 'Ẁ'=>'W', 'ẁ'=>'w',
				'Ẃ'=>'W', 'ẃ'=>'w', 'Ẅ'=>'W', 'ẅ'=>'w', 'Ẇ'=>'W', 'ẇ'=>'w', 'Ẉ'=>'W', 'ẉ'=>'w', 'Ẋ'=>'X',
				'ẋ'=>'x', 'Ẍ'=>'X', 'ẍ'=>'x', 'Ẏ'=>'Y', 'ẏ'=>'y', 'Ẑ'=>'Z', 'ẑ'=>'z', 'Ẓ'=>'Z', 'ẓ'=>'z',
				'Ẕ'=>'Z', 'ẕ'=>'z', 'ẖ'=>'h', 'ẗ'=>'t', 'ẘ'=>'w', 'ẙ'=>'y', 'ẚ'=>'a', 'ẛ'=>'s', 'ẜ'=>'SS',
				'Ạ'=>'A', 'ạ'=>'a', 'Ả'=>'A', 'ả'=>'a', 'Ấ'=>'A', 'ấ'=>'a', 'Ầ'=>'A', 'ầ'=>'a', 'Ẩ'=>'A',
				'ẩ'=>'a', 'Ẫ'=>'a', 'ẫ'=>'a', 'Ậ'=>'A', 'ậ'=>'a', 'Ắ'=>'A', 'ắ'=>'a', 'Ằ'=>'A', 'ằ'=>'a',
				'Ẳ'=>'A', 'ẳ'=>'a', 'Ẵ'=>'A', 'ẵ'=>'a', 'Ặ'=>'A', 'ặ'=>'a', 'Ẹ'=>'E', 'ẹ'=>'e', 'Ẻ'=>'E',
				'ẻ'=>'e', 'Ẽ'=>'E', 'ẽ'=>'e', 'Ế'=>'E', 'ế'=>'e', 'Ề'=>'E', 'ề'=>'e', 'Ể'=>'E', 'ể'=>'e',
				'Ễ'=>'E', 'ễ'=>'e', 'Ệ'=>'E', 'ệ'=>'e', 'Ỉ'=>'I', 'ỉ'=>'i', 'Ị'=>'I', 'ị'=>'i', 'Ọ'=>'O',
				'ọ'=>'o', 'Ỏ'=>'O', 'ỏ'=>'o', 'Ố'=>'O', 'ố'=>'o', 'Ồ'=>'O', 'ồ'=>'o', 'Ổ'=>'O', 'ổ'=>'o',
				'Ỗ'=>'O', 'ỗ'=>'o', 'Ộ'=>'O', 'ộ'=>'o', 'Ớ'=>'O', 'ớ'=>'o', 'Ờ'=>'O', 'ờ'=>'o', 'Ở'=>'O',
				'ở'=>'o', 'Ỡ'=>'O', 'ỡ'=>'o', 'Ợ'=>'O', 'ợ'=>'o', 'Ụ'=>'U', 'ụ'=>'u', 'Ủ'=>'U', 'ủ'=>'u',
				'Ứ'=>'U', 'ứ'=>'u', 'Ừ'=>'U', 'ừ'=>'u', 'Ử'=>'U', 'ử'=>'u', 'Ữ'=>'U', 'ữ'=>'u', 'Ự'=>'U',
				'ự'=>'u', 'Ỳ'=>'Y', 'ỳ'=>'y', 'Ỵ'=>'Y', 'ỵ'=>'y', 'Ỷ'=>'Y', 'ỷ'=>'y', 'Ỹ'=>'Y', 'ỹ'=>'y',
				'ↄ'=>'c', 'Ⱡ'=>'L', 'ⱡ'=>'l', 'Ɫ'=>'L', 'Ᵽ'=>'P', 'Ɽ'=>'R', 'ⱥ'=>'a', 'ⱦ'=>'t', 'Ⱨ'=>'H',
				'ⱨ'=>'h', 'Ⱪ'=>'K', 'ⱪ'=>'k', 'Ⱬ'=>'Z', 'ⱬ'=>'z', 'ⱴ'=>'v', 'Ⱶ'=>'H', 'ⱶ'=>'h', 'ⱷ'=>'o',
				'ﬀ'=>'ff', 'ﬁ'=>'fi', 'ﬂ'=>'fl', 'ﬃ'=>'ffi', 'ﬄ'=>'ffl', 'ﬅ'=>'st', 'ﬆ'=>'st'];
				
		return $replacePairs;
	}

	/**
	 * Converts passed string to lower case.
	 *
	 * All latin upper case characters will be replaced by their lower case equivalents.  Function works with
	 * diacritic regardless of the current locale.
	 *
	 * @param string a string you want to get converted
	 * @return string the passed string converted to lower case
	 * @see toUpper() 
	 */
	public static function toLower($string) {
		return strtr($string, self::getToLowerReplacePairs());
	}

	/**
	 * Converts passed string to upper case.
	 *
	 * All latin lower case characters will be replaced by their upper case equivalents.  Function works with
	 * diacritic regardless of the current locale.
	 *
	 * @param string a string you want to get converted
	 * @return string the passed string converted to upper case
	 * @see toLower() 
	 */
	public static function toUpper($string) {
		return strtr($string, self::getToUpperReplacePairs());
	}

	/**
	 * Removes diacritic in the passed string.
	 *
	 * Removes accent, umlauts and such from latin characters in the passed string.
	 *
	 * @param string a string you want to get without diacritic
	 * @return string the passed string without diacritic
	 */
	public static function removeDiacritic($string) {
		return strtr($string, self::getRemoveDiacriticReplacePairs());
	}
	
	private static function getToCharListRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('.', RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return IList 
	 */
	public static function toCharList($string) {
		$characters = new ArrayList();
		foreach (self::getToCharListRegex()->matches($string) as $match) {
			$characters->add($match->getValue());
		}
		
		return $characters;
	}
	
	private function __construct() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}

}

?>
