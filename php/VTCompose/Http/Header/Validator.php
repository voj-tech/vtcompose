<?php

namespace VTCompose\Http\Header;

use VTCompose\Text\RegularExpressions\Regex;

/**
 * 
 *
 * 
 */
class Validator {

	private static function getStringIsValidFieldNameRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[0-9A-Za-z]+(?:-[0-9A-Za-z]+)*\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[ -~]*\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidTokenRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[!#-\'*+\\-.0-9A-Z^-z|~]+\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidCookieValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(?:[!#-+--:<-[\\]-~]*|"[!#-+--:<-[\\]-~]*")\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidSubdomainRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(?!-)[-0-9A-Za-z]{1,63}(?<!-)(?:\\.(?!-)[-0-9A-Za-z]{1,63}(?<!-))*\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidNonnegativeIntegerRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[0-9]+\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidPositiveIntegerRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[1-9][0-9]*\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidPathRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[ -:<-~]*\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidQuotedStringRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^"(?:[ !#-~\\x80-\\xff]|\\r\\n[\\t ]+|\\\\[\\x00-\\x7f])*"\\z');
		}
		
		return $regex;
	}
	
	private static function getStringIsValidListElementRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^(?:(?! )[ !#-+--~]*(?<! )|"[ !#-~]*")\\z');
		}
		
		return $regex;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidFieldName($fieldName) {
		return self::getStringIsValidFieldNameRegex()->isMatch($fieldName);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidFieldValue($fieldValue) {
		return self::getStringIsValidFieldValueRegex()->isMatch($fieldValue);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidToken($token) {
		return self::getStringIsValidTokenRegex()->isMatch($token);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidCookieValue($cookieValue) {
		return self::getStringIsValidCookieValueRegex()->isMatch($cookieValue);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidSubdomain($subdomain) {
		return self::getStringIsValidSubdomainRegex()->isMatch($subdomain);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function valueIsValidNonnegativeInteger($integer) {
		return is_int($integer) ?
				$integer >= 0 : self::getStringIsValidNonnegativeIntegerRegex()->isMatch($integer);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function valueIsValidPositiveInteger($integer) {
		return is_int($integer) ?
				$integer > 0 : self::getStringIsValidPositiveIntegerRegex()->isMatch($integer);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidPath($path) {
		return self::getStringIsValidPathRegex()->isMatch($path);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidQuotedString($string) {
		return self::getStringIsValidQuotedStringRegex()->isMatch($string);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function stringIsValidListElement($listElement) {
		return self::getStringIsValidListElementRegex()->isMatch($listElement);
	}

}

?>
