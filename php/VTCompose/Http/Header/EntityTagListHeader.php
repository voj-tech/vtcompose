<?php

namespace VTCompose\Http\Header;

use VTCompose\StringUtilities as StringUtils;
use VTCompose\Text\RegularExpressions\Regex;

/**
 * 
 *
 * 
 */
abstract class EntityTagListHeader extends Header {

	private static $matchAnyEntityFieldValue = '*';

	private $entityTags;
	
	private static function getParseFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|,)(?:(?:\\r\\n)?[\\t ])*(W/)?("(?:[^"]|\\\\")*")(?<![\\t\\n\\r ])' .
					'(?:(?:\\r\\n)?[\\t ])*(?=\\z|,)');
		}
		
		return $regex;
	}
	
	private static function parseFieldValue($fieldValue) {
		if ($fieldValue == self::$matchAnyEntityFieldValue) {
			return NULL;
		}
		
		$entityTags = [];
		
		foreach (self::getParseFieldValueRegex()->matches($fieldValue) as $match) {
			$groups = $match->getGroups();
			$weak = $groups[1]->getValue() == EntityTag::WEAK_PREFIX;
			$entityTags[] = new EntityTag($groups[2]->getValue(), $weak);
		}
		
		return new EntityTagList($entityTags);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return EntityTagListHeader 
	 */
	public static function createFromFieldValue($fieldValue) {
		return new static(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param EntityTagList 
	 */
	public function __construct($fieldName, EntityTagList $entityTags = NULL) {
		parent::__construct($fieldName);
		$this->setEntityTags($entityTags);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	final public function setFieldValue($fieldValue) {
		$this->setEntityTags(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	final public function getFieldValue() {
		if ($this->entityTags == NULL) {
			return self::$matchAnyEntityFieldValue;
		}
		
		$fieldValue = '';
		foreach ($this->entityTags as $entityTag) {
			$fieldValue .= $entityTag->toString() . ', ';
		}
		return StringUtils::substring($fieldValue, 0, -2);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param EntityTagList 
	 */
	final public function setEntityTags(EntityTagList $entityTags = NULL) {
		$this->entityTags = $entityTags;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return EntityTagList 
	 */
	final public function getEntityTags() {
		return $this->entityTags;
	}
	
	/**
	 * 
	 *
	 * 
	 */
	final public function setMatchAnyEntity() {
		$this->entityTags = NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	final public function matchesAnyEntity() {
		return $this->entityTags == NULL;
	}

}

?>
