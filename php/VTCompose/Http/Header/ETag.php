<?php

namespace VTCompose\Http\Header;

use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
final class ETag extends Header {

	private $entityTag;
	
	private static function parseFieldValue($fieldValue) {
		if (StringUtils::substring($fieldValue, 0, 2) == EntityTag::WEAK_PREFIX) {
			$opaqueTag = StringUtils::substring($fieldValue, 2);
			$weak = true;
		} else {
			$opaqueTag = $fieldValue;
			$weak = false;
		}
		
		return new EntityTag($opaqueTag, $weak);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return ETag 
	 */
	public static function createFromFieldValue($fieldValue) {
		return new self(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param EntityTag 
	 */
	public function __construct(EntityTag $entityTag) {
		parent::__construct(FieldName::ETAG);
		$this->setEntityTag($entityTag);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setFieldValue($fieldValue) {
		$this->setEntityTag(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		return $this->entityTag->toString();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param EntityTag 
	 */
	public function setEntityTag(EntityTag $entityTag) {
		$this->entityTag = $entityTag;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return EntityTag 
	 */
	public function getEntityTag() {
		return $this->entityTag;
	}

}

?>
