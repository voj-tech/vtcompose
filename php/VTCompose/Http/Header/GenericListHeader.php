<?php

namespace VTCompose\Http\Header;

use VTCompose\Exception\ArgumentException;
use VTCompose\Text\RegularExpressions\Regex;

/**
 * 
 *
 * 
 */
final class GenericListHeader extends ListHeader {

	private static function getParseFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|,)(?:(?:\\r\\n)?[\\t ])*([^",]+|"[^"]*")(?<![\\t\\n\\r ])' .
				'(?:(?:\\r\\n)?[\\t ])*(?=\\z|,)');
		}
		
		return $regex;
	}
	
	private static function parseFieldValue($fieldValue) {
		$values = [];
		
		foreach (self::getParseFieldValueRegex()->matches($fieldValue) as $match) {
			$values[] = $match->getGroups()[1]->getValue();
		}
		
		return $values;
	}
	
	private static function getValueValidator() {
		static $valueValidator = NULL;
		if ($valueValidator == NULL) {
			$valueValidator = function($value) {
				if (!self::getValidator()->stringIsValidListElement($value)) {
					throw new ArgumentException('Value is not a valid list element.');
				}
			};
		}
		
		return $valueValidator;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param int 
	 * @param int 
	 * @return GenericListHeader 
	 */
	public static function createFromFieldValue($fieldName, $fieldValue,
			$minNumElements = 0, $maxNumElements = -1) {
		return new self($fieldName, self::parseFieldValue($fieldValue), $minNumElements, $maxNumElements);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param mixed 
	 * @param int 
	 * @param int 
	 */
	public function __construct($fieldName, $values, $minNumElements = 0, $maxNumElements = -1) {
		parent::__construct($fieldName, $values, self::getValueValidator(), $minNumElements, $maxNumElements);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setFieldValue($fieldValue) {
		$this->setValues(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		return implode(', ', $this->toArray());
	}

}

?>
