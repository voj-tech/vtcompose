<?php

namespace VTCompose\Http\Header;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class FieldName extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const ALLOW = 'Allow';

	/**
	 * 
	 *
	 * 
	 */
	const CACHE_CONTROL = 'Cache-Control';

	/**
	 * 
	 *
	 * 
	 */
	const CONTENT_LANGUAGE = 'Content-Language';

	/**
	 * 
	 *
	 * 
	 */
	const CONTENT_SCRIPT_TYPE = 'Content-Script-Type';

	/**
	 * 
	 *
	 * 
	 */
	const CONTENT_STYLE_TYPE = 'Content-Style-Type';

	/**
	 * 
	 *
	 * 
	 */
	const CONTENT_TYPE = 'Content-Type';

	/**
	 * 
	 *
	 * 
	 */
	const COOKIE = 'Cookie';

	/**
	 * 
	 *
	 * 
	 */
	const ETAG = 'ETag';
	
	/**
	 * 
	 *
	 * 
	 */
	const HOST = 'Host';
	
	/**
	 * 
	 *
	 * 
	 */
	const IF_MATCH = 'If-Match';
	
	/**
	 * 
	 *
	 * 
	 */
	const IF_NONE_MATCH = 'If-None-Match';

	/**
	 * 
	 *
	 * 
	 */
	const LOCATION = 'Location';

	/**
	 * 
	 *
	 * 
	 */
	const SET_COOKIE = 'Set-Cookie';

}

?>
