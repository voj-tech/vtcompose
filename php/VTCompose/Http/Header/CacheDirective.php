<?php

namespace VTCompose\Http\Header;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class CacheDirective extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const NO_CACHE = 'no-cache';

	/**
	 * 
	 *
	 * 
	 */
	const NO_STORE = 'no-store';

	/**
	 * 
	 *
	 * 
	 */
	const MAX_AGE = 'max-age';

	/**
	 * 
	 *
	 * 
	 */
	const MAX_STALE = 'max-stale';

	/**
	 * 
	 *
	 * 
	 */
	const MIN_FRESH = 'min-fresh';

	/**
	 * 
	 *
	 * 
	 */
	const NO_TRANSFORM = 'no-transform';

	/**
	 * 
	 *
	 * 
	 */
	const ONLY_IF_CACHED = 'only-if-cached';

	/**
	 * 
	 *
	 * 
	 */
	const PUBLIC_DIRECTIVE = 'public';

	/**
	 * 
	 *
	 * 
	 */
	const PRIVATE_DIRECTIVE = 'private';
	
	/**
	 * 
	 *
	 * 
	 */
	const MUST_REVALIDATE = 'must-revalidate';

	/**
	 * 
	 *
	 * 
	 */
	const PROXY_REVALIDATE = 'proxy-revalidate';
	
	/**
	 * 
	 *
	 * 
	 */
	const S_MAXAGE = 's-maxage';

}

?>
