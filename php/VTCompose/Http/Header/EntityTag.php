<?php

namespace VTCompose\Http\Header;

use VTCompose\Exception\ArgumentException;

/**
 * 
 *
 * 
 */
final class EntityTag {

	/**
	 * @internal
	 */
	const WEAK_PREFIX = 'W/';

	private $opaqueTag;
	private $weak;
	
	private static function getValidator() {
		static $validator = NULL;
		if ($validator == NULL) {
			$validator = new Validator();
		}
		
		return $validator;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param bool 
	 */
	public function __construct($opaqueTag, $weak = false) {
		$this->setOpaqueTag($opaqueTag);
		$this->setWeak($weak);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function setOpaqueTag($opaqueTag) {
		if (!self::getValidator()->stringIsValidQuotedString($opaqueTag)) {
			throw new ArgumentException('Opaque tag is not a valid quoted string.');
		}

		$this->opaqueTag = $opaqueTag;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getOpaqueTag() {
		return $this->opaqueTag;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 */
	public function setWeak($weak) {
		$this->weak = $weak;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isWeak() {
		return $this->weak;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function toString() {
		return ($this->weak ? self::WEAK_PREFIX : '') . $this->opaqueTag;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function __toString() {
		return $this->toString();
	}

}

?>
