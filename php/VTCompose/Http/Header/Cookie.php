<?php

namespace VTCompose\Http\Header;

use VTCompose\Collection\KeyValuePair;
use VTCompose\Exception\ArgumentException;
use VTCompose\StringUtilities as StringUtils;
use VTCompose\Text\RegularExpressions\Regex;
use VTCompose\Text\RegularExpressions\RegexOptions;

/**
 * 
 *
 * 
 */
final class Cookie extends ListHeader {
	
	private static function getTrimRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$pattern = '^(?:(?:\\r\\n)?[\\t ])*(.*)(?<![\\t\\n\\r ])(?:(?:\\r\\n)?[\\t ])*\\z';
			$regex = new Regex($pattern, RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private static function getParseFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|;)[\\t ]*([^;=]*)(?<![\\t ])(?:[\\t ]*=[\\t ]*([^;]*))?(?<![\\t ])' .
					'[\\t ]*(?=\\z|;)');
		}
		
		return $regex;
	}

	private static function parseFieldValue($fieldValue) {
		static $errorMessage = 'Field value is not a valid Cookie field value.';
	
		$trimMatch = self::getTrimRegex()->match($fieldValue);
		if (!$trimMatch->isSuccess()) {
			throw new ArgumentException($errorMessage);
		}
		
		$cookiePairs = [];
		
		foreach (self::getParseFieldValueRegex()->matches($trimMatch->getGroups()[1]->getValue()) as $match) {
			$groups = $match->getGroups();
			if ($groups->count() < 3) {
				throw new ArgumentException($errorMessage);
			}
			
			$cookiePairs[] = new KeyValuePair($groups[1]->getValue(), $groups[2]->getValue());
		}
		
		return $cookiePairs;
	}
	
	private static function getValueValidator() {
		static $valueValidator = NULL;
		if ($valueValidator == NULL) {
			$valueValidator = function($value) {
				if (!$value instanceof KeyValuePair) {
					throw new ArgumentException('Value is not an instance of KeyValuePair.');
				}
				
				if (!self::getValidator()->stringIsValidToken($value->getKey())) {
					throw new ArgumentException('Name is invalid.');
				}
				
				if (!self::getValidator()->stringIsValidCookieValue($value->getValue())) {
					throw new ArgumentException('Value is invalid.');
				}
			};
		}
		
		return $valueValidator;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Cookie 
	 */
	public static function createFromFieldValue($fieldValue) {
		return new self(self::parseFieldValue($fieldValue));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function __construct($cookiePairs) {
		$minNumElements = 1;
		parent::__construct(FieldName::COOKIE, $cookiePairs, self::getValueValidator(), $minNumElements);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setFieldValue($fieldValue) {
		$this->setValues(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		$fieldValue = '';
		foreach ($this as $cookiePair) {
			$fieldValue .= $cookiePair->getKey() . '=' . $cookiePair->getValue() . '; ';
		}
		return StringUtils::substring($fieldValue, 0, -2);
	}

}

?>
