<?php

namespace VTCompose\Http\Header;

use VTCompose\Exception\ArgumentException;
use VTCompose\Text\RegularExpressions\Regex;
use VTCompose\Text\RegularExpressions\RegexMatch;

/**
 * 
 *
 * 
 */
final class SetCookie extends Header {

	private $name;
	private $value;
	private $domain;
	private $maxAge;
	private $path;
	private $secure;
	
	private static function getParseFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('[\\t ]*([^;=]*)(?<![\\t ])(?:[\\t ]*=[\\t ]*([^;]*)(?<![\\t ]))?');
		}
		
		return $regex;
	}

	private static function assignNameAndValue(RegexMatch $match, &$name, &$value) {
		$groups = $match->getGroups();
		$name = $groups[1]->getValue();
		$value = $groups->count() > 2 ? $groups[2]->getValue() : NULL;
	}

	private static function parseFieldValue($fieldValue,
			&$name, &$value, &$domain, &$maxAge, &$path, &$secure) {
		$matches = self::getParseFieldValueRegex()->matches($fieldValue);
		if ($matches->count() == 0 || $matches[0]->getGroups()->count() < 3) {
			throw new ArgumentException('Field value is not a valid Set-Cookie field value.');
		}
		
		self::assignNameAndValue($matches[0], $name, $value);
		$domain = $maxAge = $path = NULL;
		$secure = false;
		
		for ($i = 1; $i < $matches->count(); $i++) {
			self::assignNameAndValue($matches[$i], $attributeName, $attributeValue);
			
			switch ($attributeName) {
			case 'Domain':
				$domain = $attributeValue;
				break;
				
			case 'Max-Age':
				$maxAge = $attributeValue;
				break;
				
			case 'Path':
				$path = $attributeValue;
				break;
				
			case 'Secure':
				$secure = true;
				break;
			}
		}
	}

	private static function nameIsValid($name) {
		return self::getValidator()->stringIsValidToken($name);
	}

	private static function valueIsValid($value) {
		return self::getValidator()->stringIsValidCookieValue($value);
	}

	private static function domainIsValid($domain) {
		return $domain === NULL || self::getValidator()->stringIsValidSubdomain($domain);
	}

	private static function maxAgeIsValid($maxAge) {
		return $maxAge === NULL || self::getValidator()->valueIsValidPositiveInteger($maxAge);
	}

	private static function pathIsValid($path) {
		return $path === NULL || self::getValidator()->stringIsValidPath($path);
	}

	private function setAttributes($domain, $maxAge, $path, $secure) {
		$this->domain = self::domainIsValid($domain) ? $domain : NULL;
		$this->maxAge = self::maxAgeIsValid($maxAge) ? $maxAge : NULL;
		$this->path = self::pathIsValid($path) ? $path : NULL;
		$this->secure = $secure;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return SetCookie 
	 */
	public static function createFromFieldValue($fieldValue) {
		self::parseFieldValue($fieldValue, $name, $value, $domain, $maxAge, $path, $secure);

		$setCookie = new self($name, $value);
		$setCookie->setAttributes($domain, $maxAge, $path, $secure);
		return $setCookie;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param string 
	 * @param mixed 
	 * @param string 
	 * @param bool 
	 */
	public function __construct($name, $value = '',
			$domain = NULL, $maxAge = NULL, $path = NULL, $secure = false) {
		parent::__construct(FieldName::SET_COOKIE);
		$this->setName($name);
		$this->setValue($value);
		$this->setDomain($domain);
		$this->setMaxAge($maxAge);
		$this->setPath($path);
		$this->setSecure($secure);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setFieldValue($fieldValue) {
		self::parseFieldValue($fieldValue, $name, $value, $domain, $maxAge, $path, $secure);
		$this->setName($name);
		$this->setValue($value);
		$this->setAttributes($domain, $maxAge, $path, $secure);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		$fieldValue = $this->name . '=' . $this->value;

		if ($this->domain !== NULL) {
			$fieldValue .= '; Domain=' . $this->domain;
		}

		if ($this->maxAge !== NULL) {
			$fieldValue .= '; Max-Age=' . $this->maxAge;
		}

		if ($this->path !== NULL) {
			$fieldValue .= '; Path=' . $this->path;
		}

		if ($this->secure) {
			$fieldValue .= '; Secure';
		}

		$fieldValue .= '; Version=1';

		return $fieldValue;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function setName($name) {
		if (!self::nameIsValid($name)) {
			throw new ArgumentException('Name is invalid.');
		}

		$this->name = $name;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function setValue($value) {
		if (!self::valueIsValid($value)) {
			throw new ArgumentException('Value is invalid.');
		}

		$this->value = $value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function setDomain($domain) {
		if (!self::domainIsValid($domain)) {
			throw new ArgumentException('Domain is invalid.');
		}

		$this->domain = $domain;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getDomain() {
		return $this->domain;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	public function setMaxAge($maxAge) {
		if (!self::maxAgeIsValid($maxAge)) {
			throw new ArgumentException('Max age is not a positive integer or NULL.');
		}

		$this->maxAge = $maxAge;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getMaxAge() {
		return $this->maxAge;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function setPath($path) {
		if (!self::pathIsValid($path)) {
			throw new ArgumentException('Path is invalid.');
		}

		$this->path = $path;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPath() {
		return $this->path;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 */
	public function setSecure($secure) {
		$this->secure = $secure;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isSecure() {
		return $this->secure;
	}

}

?>
