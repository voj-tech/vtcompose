<?php

namespace VTCompose\Http\Header;

use VTCompose\Exception\ArgumentException;

/**
 * 
 *
 * 
 */
final class GenericHeader extends Header {

	private $fieldValue;

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 */
	public function __construct($fieldName, $fieldValue) {
		parent::__construct($fieldName);
		$this->setFieldValue($fieldValue);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function setFieldValue($fieldValue) {
		if (!self::getValidator()->stringIsValidFieldValue($fieldValue)) {
			throw new ArgumentException('Field value is invalid.');
		}

		$this->fieldValue = $fieldValue;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		return $this->fieldValue;
	}

}

?>
