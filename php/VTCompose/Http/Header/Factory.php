<?php

namespace VTCompose\Http\Header;

/**
 * 
 *
 * 
 */
class Factory {

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return Header 
	 */
	public function createHeader($fieldName, $fieldValue) {
		switch ($fieldName) {
		case FieldName::ALLOW:
			return Allow::createFromFieldValue($fieldValue);
		
		case FieldName::CACHE_CONTROL:
			return CacheControl::createFromFieldValue($fieldValue);
			
		case FieldName::CONTENT_LANGUAGE:
			$minNumElements = 1;
			return GenericListHeader::createFromFieldValue($fieldName, $fieldValue, $minNumElements);
			
		case FieldName::CONTENT_SCRIPT_TYPE:
		case FieldName::CONTENT_STYLE_TYPE:
		case FieldName::CONTENT_TYPE:
			return MediaTypeHeader::createFromFieldValue($fieldName, $fieldValue);
			
		case FieldName::COOKIE:
			return Cookie::createFromFieldValue($fieldValue);
			
		case FieldName::ETAG:
			return ETag::createFromFieldValue($fieldValue);
			
		case FieldName::IF_MATCH:
			return IfMatch::createFromFieldValue($fieldValue);
			
		case FieldName::IF_NONE_MATCH:
			return IfNoneMatch::createFromFieldValue($fieldValue);
			
		case FieldName::SET_COOKIE:
			return SetCookie::createFromFieldValue($fieldValue);
			
		default:
			return new GenericHeader($fieldName, $fieldValue);
		}
	}

}

?>
