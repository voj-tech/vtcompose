<?php

namespace VTCompose\Http\Header;

use IteratorAggregate;
use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;
use VTCompose\Collection\KeyValuePair;
use VTCompose\Exception\ArgumentException;
use VTCompose\Text\RegularExpressions\Regex;

/**
 * 
 *
 * 
 */
final class MediaTypeHeader extends Header {

	private $type;
	private $parameters;
	
	private static function getParseFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|;)[\\t ]*([^;=]*)(?<![\\t ])' .
					'(?:[\\t ]*=[\\t ]*([^";]*|"(?:[^"]|\\\\")*"))?(?<![\\t ])[\\t ]*(?=\\z|;)');
		}
		
		return $regex;
	}
	
	private static function parseFieldValue($fieldValue, &$type, &$parameters) {
		$matches = self::getParseFieldValueRegex()->matches($fieldValue);
		$groups = $matches->count() > 0 ? $matches[0]->getGroups() : NULL;
		if (count($groups) != 2) {
			throw new ArgumentException('Field value is not a valid media type header field value.');
		}
		
		$type = $groups[1]->getValue();
		$parameters = new ArrayList();
		
		for ($i = 1; $i < $matches->count(); $i++) {
			$groups = $matches[$i]->getGroups();
			if ($groups->count() < 3) {
				continue;
			}
			
			$parameters->add(new KeyValuePair($groups[1]->getValue(), $groups[2]->getValue()));
		}
	}
	
	private static function typeIsValid($type) {
		$typeParts = explode('/', $type, 2);
		if (count($typeParts) < 2) {
			return false;
		}
		
		if (!self::getValidator()->stringIsValidToken($typeParts[0])) {
			return false;
		}
		
		if (!self::getValidator()->stringIsValidToken($typeParts[1])) {
			return false;
		}
		
		return true;
	}
	
	private static function parameterNameIsValid($parameterName) {
		return self::getValidator()->stringIsValidToken($parameterName);
	}
	
	private static function parameterValueIsValid($parameterValue) {
		if (self::getValidator()->stringIsValidToken($parameterValue)) {
			return true;
		}
		
		if (self::getValidator()->stringIsValidQuotedString($parameterValue)) {
			return true;
		}
		
		return false;
	}
	
	private static function validateParameter(KeyValuePair $parameter) {		
		if (!self::parameterNameIsValid($parameter->getKey())) {
			throw new ArgumentException('Attribute is invalid.');
		}
		
		if (!self::parameterValueIsValid($parameter->getValue())) {
			throw new ArgumentException('Value is invalid.');
		}
	}
	
	private function setParametersPermissive(IteratorAggregate $parameters) {
		$this->parameters = new ArrayList();

		foreach ($parameters as $parameter) {
			$key = $parameter->getKey();
			$value = $parameter->getValue();
			if (!self::parameterNameIsValid($key) || !self::parameterValueIsValid($value)) {
				continue;
			}
			
			$this->parameters->add($parameter);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return MediaTypeHeader 
	 */
	public static function createFromFieldValue($fieldName, $fieldValue) {
		self::parseFieldValue($fieldValue, $type, $parameters);
		$mediaTypeHeader = new self($fieldName, $type);
		$mediaTypeHeader->setParametersPermissive($parameters);
		return $mediaTypeHeader;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param mixed 
	 */
	public function __construct($fieldName, $type, $parameters = []) {
		parent::__construct($fieldName);
		$this->setType($type);
		$this->setParameters($parameters);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setFieldValue($fieldValue) {
		self::parseFieldValue($fieldValue, $type, $parameters);
		$this->setType($type);
		$this->setParametersPermissive($parameters);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		$fieldValue = $this->type;
		foreach ($this->parameters as $parameter) {
			$fieldValue .= '; ' . $parameter->getKey() . '=' . $parameter->getValue();
		}
		return $fieldValue;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function setType($type) {
		if (!self::typeIsValid($type)) {
			throw new ArgumentException('Type is invalid.');
		}
		
		$this->type = $type;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function setParameters($parameters) {
		$parameters = new ArrayList($parameters);

		foreach ($parameters as $parameter) {
			self::validateParameter($parameter);
		}
		
		$this->parameters = $parameters;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 * @todo When there are read-only versions of collections return a read-only wrapper here.
	 */
	public function getParameters() {
		return clone $this->parameters;
	}

}

?>
