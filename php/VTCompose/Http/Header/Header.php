<?php

namespace VTCompose\Http\Header;

use VTCompose\Exception\ArgumentException;

/**
 * 
 *
 * 
 */
abstract class Header {

	private $fieldName;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Validator 
	 */
	protected static function getValidator() {
		static $validator = NULL;
		if ($validator == NULL) {
			$validator = new Validator();
		}
		
		return $validator;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function __construct($fieldName) {
		if (!self::getValidator()->stringIsValidFieldName($fieldName)) {
			throw new ArgumentException('Field name is invalid.');
		}

		$this->fieldName = $fieldName;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldName() {
		return $this->fieldName;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	abstract public function setFieldValue($fieldValue);

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	abstract public function getFieldValue();

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function toString() {
		return $this->fieldName . ': ' . $this->getFieldValue();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function __toString() {
		return $this->toString();
	}

}

?>
