<?php

namespace VTCompose\Http\Header;

use Traversable;
use VTCompose\Collection\ArrayList;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\IList;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
final class EntityTagList implements IList {

	use Enumerable;
	
	private $entityTags;
	
	private static function numEntityTagsIsValid($numEntityTags) {
		return $numEntityTags >= 1;
	}
	
	private static function validateValue($value) {
		if (!$value instanceof EntityTag) {
			throw new ArgumentException('Value is not an instance of EntityTag.');
		}
	}
	
	private static function validateNumEntityTagsAfterOperation($numEntityTagsAfterOperation) {
		if (!self::numEntityTagsIsValid($numEntityTagsAfterOperation)) {
			throw new InvalidOperationException('Operation results in illegal number of EntityTags.');
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	public function __construct($values) {
		$values = new ArrayList($values);
		if (!self::numEntityTagsIsValid($values->count())) {
			throw new ArgumentException('Invalid number of EntityTags.');
		}
		
		foreach ($values as $value) {
			self::validateValue($value);
		}
		
		$this->entityTags = $values;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		self::validateNumEntityTagsAfterOperation($this->entityTags->count() + 1);
		self::validateValue($value);
		$this->entityTags->add($value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		self::validateNumEntityTagsAfterOperation(0);
		$this->entityTags->clear();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		return $this->entityTags->contains($value);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return int 
	 */
	public function indexOf($value) {
		return $this->entityTags->indexOf($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param mixed 
	 */
	public function insert($index, $value) {
		self::validateNumEntityTagsAfterOperation($this->entityTags->count() + 1);
		self::validateValue($value);
		$this->entityTags->insert($index, $value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		$entityTagsClone = clone $this->entityTags;
		$result = $entityTagsClone->remove($value);
		self::validateNumEntityTagsAfterOperation($entityTagsClone->count());
		$this->entityTags = $entityTagsClone;
		return $result;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function removeAt($index) {
		self::validateNumEntityTagsAfterOperation($this->entityTags->count() - 1);
		$this->entityTags->removeAt($index);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->entityTags->toArray();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		return $this->entityTags->getIterator();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function offsetExists($offset): bool {
		return isset($this->entityTags[$offset]);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function offsetGet($offset): mixed {
		return $this->entityTags[$offset];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function offsetSet($offset, $value): void {
		self::validateValue($value);

		if ($offset === NULL) {
			self::validateNumEntityTagsAfterOperation($this->entityTags->count() + 1);
			$this->entityTags[] = $value;
			return;
		}

		$entityTagsClone = clone $this->entityTags;
		$entityTagsClone[$offset] = $value;
		self::validateNumEntityTagsAfterOperation($entityTagsClone->count());
		$this->entityTags = $entityTagsClone;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function offsetUnset($offset): void {
		$entityTagsClone = clone $this->entityTags;
		unset($entityTagsClone[$offset]);
		self::validateNumEntityTagsAfterOperation($entityTagsClone->count());
		$this->entityTags = $entityTagsClone;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->entityTags->count();
	}

}

?>
