<?php

namespace VTCompose\Http\Header;

use Closure;
use Traversable;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\ISet;
use VTCompose\Collection\Set;

/**
 * 
 *
 * 
 */
abstract class SetHeader extends Header implements ISet {

	use Enumerable;
	
	private $values;
	
	private $valueValidator;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	final protected function setValues($values) {
		$values = new Set($values);
		
		foreach ($values as $value) {
			$this->valueValidator->__invoke($value);
		}
		
		$this->values = $values;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param mixed 
	 * @param Closure 
	 */
	public function __construct($fieldName, $values, Closure $valueValidator) {
		parent::__construct($fieldName);
		
		$this->valueValidator = $valueValidator;
		
		$this->setValues($values);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->valueValidator->__invoke($value);
		$this->values->add($value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		$this->values->clear();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		return $this->values->contains($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		return $this->values->remove($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->values->toArray();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		return $this->values->getIterator();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->values->count();
	}

}

?>
