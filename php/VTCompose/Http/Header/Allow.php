<?php

namespace VTCompose\Http\Header;

use VTCompose\Exception\ArgumentException;
use VTCompose\Http\Method;
use VTCompose\Text\RegularExpressions\Regex;

/**
 * 
 *
 * 
 */
final class Allow extends SetHeader {

	private static function getParseFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|,)(?:(?:\\r\\n)?[\\t ])*([^,]+)(?<![\\t\\n\\r ])' .
					'(?:(?:\\r\\n)?[\\t ])*(?=\\z|,)');
		}
		
		return $regex;
	}

	private static function parseFieldValue($fieldValue) {
		$methods = [];
		
		foreach (self::getParseFieldValueRegex()->matches($fieldValue) as $match) {
			$methods[] = $match->getGroups()[1]->getValue();
		}
		
		return $methods;
	}
	
	private static function getValueValidator() {
		static $valueValidator = NULL;
		if ($valueValidator == NULL) {
			$valueValidator = function($value) {
				if (!Method::isDefined($value)) {
					throw new ArgumentException('Value is not a valid HTTP method.');
				}
			};
		}
		
		return $valueValidator;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Allow 
	 */
	public static function createFromFieldValue($fieldValue) {
		return new self(self::parseFieldValue($fieldValue));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function __construct($methods) {
		parent::__construct(FieldName::ALLOW, $methods, self::getValueValidator());
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setFieldValue($fieldValue) {
		$this->setValues(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		return implode(', ', $this->toArray());
	}

}

?>
