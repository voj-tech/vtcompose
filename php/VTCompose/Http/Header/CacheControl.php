<?php

namespace VTCompose\Http\Header;

use VTCompose\Collection\KeyValuePair;
use VTCompose\Exception\ArgumentException;
use VTCompose\StringUtilities as StringUtils;
use VTCompose\Text\RegularExpressions\Regex;
use VTCompose\Text\RegularExpressions\RegexOptions;

/**
 * 
 *
 * 
 */
final class CacheControl extends ListHeader {

	private static function getTrimRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^"(.*)"\\z', RegexOptions::SINGLELINE);
		}
		
		return $regex;
	}
	
	private static function getParseFieldNamesRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|,)(?:(?:\\r\\n)?[\\t ])*([^,]+)(?<![\\t\\n\\r ])' .
					'(?:(?:\\r\\n)?[\\t ])*(?=\\z|,)');
		}
		
		return $regex;
	}
	
	private static function getParseFieldValueRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('(?<=^|,)(?:(?:\\r\\n)?[\\t ])*([^,=]*)(?<![\\t ])(?:[\\t ]*=[\\t ]*' .
				'([^",]*|"(?:[^"]|\\\\")*"))?(?<![\\t\\n\\r ])(?:(?:\\r\\n)?[\\t ])*(?=\\z|,)');
		}
		
		return $regex;
	}

	private static function parseFieldNames($fieldNames) {
		$trimMatch = self::getTrimRegex()->match($fieldNames);
		if (!$trimMatch->isSuccess()) {
			throw new ArgumentException('Field value is not a valid Cache-Control field value.');
		}
	
		$fieldNameArray = [];

		foreach (self::getParseFieldNamesRegex()->matches($trimMatch->getGroups()[1]->getValue()) as $match) {
			$fieldNameArray[] = $match->getGroups()[1]->getValue();
		}
		
		return $fieldNameArray;
	}

	private static function parseFieldValue($fieldValue) {
		$cacheDirectives = [];
		
		foreach (self::getParseFieldValueRegex()->matches($fieldValue) as $match) {
			$groups = $match->getGroups();
			$directive = $groups[1]->getValue();
			if (StringUtils::length($directive) == 0 && $groups->count() < 3) {
				continue;
			}
			
			if ($groups->count() > 2) {
				$parameter = $groups[2]->getValue();
				
				$directiveToLower = StringUtils::toLower($directive);
				if ($directiveToLower == CacheDirective::NO_CACHE ||
						$directiveToLower == CacheDirective::PRIVATE_DIRECTIVE) {
					$parameter = self::parseFieldNames($parameter);
				}
			
				$directive = new KeyValuePair($directive, $parameter);
			}
			
			$cacheDirectives[] = $directive;
		}
		
		return $cacheDirectives;
	}
	
	private static function fieldNamesAreValid(array $fieldNames) {
		if (count($fieldNames) == 0) {
			return false;
		}
		
		foreach ($fieldNames as $fieldName) {
			if (!self::getValidator()->stringIsValidToken($fieldName)) {
				return false;
			}
		}
		
		return true;
	}
	
	private static function deltaSecondsAreValid($deltaSeconds) {
		return self::getValidator()->valueIsValidNonnegativeInteger($deltaSeconds);
	}
	
	private static function cacheExtensionIsValid($cacheExtension) {
		return self::getValidator()->stringIsValidToken($cacheExtension);
	}
	
	private static function cacheExtensionParameterIsValid($cacheExtensionParameter) {
		if (self::getValidator()->stringIsValidToken($cacheExtensionParameter)) {
			return true;
		}
		
		if (self::getValidator()->stringIsValidQuotedString($cacheExtensionParameter)) {
			return true;
		}
		
		return false;
	}

	private static function getValueValidator() {
		static $valueValidator = NULL;
		if ($valueValidator == NULL) {
			$valueValidator = function($value) {
				if ($value instanceof KeyValuePair) {
					$directive = $value->getKey();
					$parameter = $value->getValue();
				} else {
					$directive = $value;
					$parameter = NULL;
				}
				
				$directiveToLower = StringUtils::toLower($directive);
				
				switch ($directiveToLower) {
				case CacheDirective::NO_CACHE:
				case CacheDirective::PRIVATE_DIRECTIVE:
					if ($parameter !== NULL && !self::fieldNamesAreValid($parameter)) {
						throw new ArgumentException('Field names are invalid.');
					}
					break;
					
				case CacheDirective::NO_STORE:
				case CacheDirective::NO_TRANSFORM:
				case CacheDirective::ONLY_IF_CACHED:
				case CacheDirective::PUBLIC_DIRECTIVE:
				case CacheDirective::MUST_REVALIDATE:
				case CacheDirective::PROXY_REVALIDATE:
					if ($parameter !== NULL) {
						throw new ArgumentException('Parameter is specified.');
					}
					break;
					
				case CacheDirective::MAX_AGE:
				case CacheDirective::MAX_STALE:
				case CacheDirective::MIN_FRESH:
				case CacheDirective::S_MAXAGE:
					if (($directiveToLower != CacheDirective::MAX_STALE && $parameter === NULL) ||
							($parameter !== NULL && !self::deltaSecondsAreValid($parameter))) {
						throw new ArgumentException('Delta seconds are invalid.');
					}
					break;
					
				default:
					if (!self::cacheExtensionIsValid($directive)) {
						throw new ArgumentException('Cache extension is invalid.');
					}
					if ($parameter !== NULL && !self::cacheExtensionParameterIsValid($parameter)) {
						throw new ArgumentException('Cache extension parameter is invalid.');
					}
				}
			};
		}
		
		return $valueValidator;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return CacheControl 
	 */
	public static function createFromFieldValue($fieldValue) {
		return new self(self::parseFieldValue($fieldValue));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function __construct($cacheDirectives) {
		$valueValidator = self::getValueValidator();
		$minNumElements = 1;
		parent::__construct(FieldName::CACHE_CONTROL, $cacheDirectives, $valueValidator, $minNumElements);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setFieldValue($fieldValue) {
		$this->setValues(self::parseFieldValue($fieldValue));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getFieldValue() {
		$fieldValue = '';
		
		foreach ($this as $cacheDirective) {
			if ($cacheDirective instanceof KeyValuePair) {
				$fieldValue .= $cacheDirective->getKey();
				
				$parameter = $cacheDirective->getValue();
				if ($parameter !== NULL) {
					$fieldValue .= '=';
					if (is_array($parameter)) {
						$fieldValue .= '"' . implode(', ', $parameter) . '"';
					} else {
						$fieldValue .= $parameter;
					}
				}
			} else {
				$fieldValue .= $cacheDirective;
			}
			$fieldValue .= ', ';
		}
		
		return StringUtils::substring($fieldValue, 0, -2);
	}

}

?>
