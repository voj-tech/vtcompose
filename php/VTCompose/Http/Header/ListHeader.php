<?php

namespace VTCompose\Http\Header;

use Closure;
use Traversable;
use VTCompose\Collection\ArrayList;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\IList;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
abstract class ListHeader extends Header implements IList {

	use Enumerable;
	
	private $values;
	
	private $valueValidator;
	private $minNumElements;
	private $maxNumElements;
	
	private function numValuesIsValid($numValues) {
		return $numValues >= $this->minNumElements &&
				($this->maxNumElements < 0 || $numValues <= $this->maxNumElements);
	}
	
	private function validateNumValuesAfterOperation($numValuesAfterOperation) {
		if (!$this->numValuesIsValid($numValuesAfterOperation)) {
			throw new InvalidOperationException('Operation results in illegal number of list elements.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	final protected function setValues($values) {
		$values = new ArrayList($values);
		if (!$this->numValuesIsValid($values->count())) {
			throw new ArgumentException('Invalid number of list elements.');
		}
		
		foreach ($values as $value) {
			$this->valueValidator->__invoke($value);
		}
		
		$this->values = $values;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param mixed 
	 * @param Closure 
	 * @param int 
	 * @param int 
	 */
	public function __construct($fieldName, $values, Closure $valueValidator,
			$minNumElements = 0, $maxNumElements = -1) {
		parent::__construct($fieldName);
		
		$this->valueValidator = $valueValidator;
		$this->minNumElements = $minNumElements;
		$this->maxNumElements = $maxNumElements;
		
		$this->setValues($values);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->validateNumValuesAfterOperation($this->values->count() + 1);
		$this->valueValidator->__invoke($value);
		$this->values->add($value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		$this->validateNumValuesAfterOperation(0);
		$this->values->clear();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		return $this->values->contains($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return int 
	 */
	public function indexOf($value) {
		return $this->values->indexOf($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param mixed 
	 */
	public function insert($index, $value) {
		$this->validateNumValuesAfterOperation($this->values->count() + 1);
		$this->valueValidator->__invoke($value);
		$this->values->insert($index, $value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		$valuesClone = clone $this->values;
		$result = $valuesClone->remove($value);
		$this->validateNumValuesAfterOperation($valuesClone->count());
		$this->values = $valuesClone;
		return $result;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function removeAt($index) {
		$this->validateNumValuesAfterOperation($this->values->count() - 1);
		$this->values->removeAt($index);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->values->toArray();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		return $this->values->getIterator();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function offsetExists($offset): bool {
		return isset($this->values[$offset]);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function offsetGet($offset): mixed {
		return $this->values[$offset];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function offsetSet($offset, $value): void {
		$this->valueValidator->__invoke($value);

		if ($offset === NULL) {
			$this->validateNumValuesAfterOperation($this->values->count() + 1);
			$this->values[] = $value;
			return;
		}

		$valuesClone = clone $this->values;
		$valuesClone[$offset] = $value;
		$this->validateNumValuesAfterOperation($valuesClone->count());
		$this->values = $valuesClone;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function offsetUnset($offset): void {
		$valuesClone = clone $this->values;
		unset($valuesClone[$offset]);
		$this->validateNumValuesAfterOperation($valuesClone->count());
		$this->values = $valuesClone;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->values->count();
	}

}

?>
