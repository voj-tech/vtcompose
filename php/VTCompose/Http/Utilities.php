<?php

namespace VTCompose\Http;

use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
final class Utilities {

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return bool 
	 */
	public static function statusCodeIndicatesSuccess($statusCode) {
		return $statusCode >= StatusCode::OK && $statusCode < StatusCode::MULTIPLE_CHOICES;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return bool 
	 */
	public static function statusCodeIndicatesError($statusCode) {
		return $statusCode >= StatusCode::BAD_REQUEST;
	}
	
	private function __construct() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}

}

?>
