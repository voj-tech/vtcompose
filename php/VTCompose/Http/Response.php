<?php

namespace VTCompose\Http;

/**
 * 
 *
 * 
 */
class Response extends Message {

	private $statusCode;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function __construct($statusCode) {
		parent::__construct();
		$this->statusCode = $statusCode;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function setStatusCode($statusCode) {
		$this->statusCode = $statusCode;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getStatusCode() {
		return $this->statusCode;
	}

}

?>
