<?php

namespace VTCompose\Http;

use VTCompose\Collection\Dictionary;
use VTCompose\Collection\IDictionary;

/**
 * 
 *
 * 
 *
 * @todo Create HeaderCollection and use it for headers instead of Dictionary.
 */
abstract class Message {

	private $headers;
	private $body;

	/**
	 * 
	 *
	 * 
	 */
	public function __construct() {
		$this->headers = new Dictionary();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param IDictionary 
	 */
	public function setHeaders(IDictionary $headers) {
		$this->headers = $headers;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return IDictionary 
	 */
	public function getHeaders() {
		return $this->headers;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setBody($body) {
		$this->body = $body;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getBody() {
		return $this->body;
	}

}

?>
