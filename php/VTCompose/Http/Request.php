<?php

namespace VTCompose\Http;

/**
 * 
 *
 * 
 */
class Request extends Message {

	private $method;
	private $uri;

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 */
	public function __construct($method, $uri) {
		parent::__construct();
		$this->method = $method;
		$this->uri = $uri;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setMethod($method) {
		$this->method = $method;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getMethod() {
		return $this->method;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setUri($uri) {
		$this->uri = $uri;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getUri() {
		return $this->uri;
	}

}

?>
