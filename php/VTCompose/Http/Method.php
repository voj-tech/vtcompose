<?php

namespace VTCompose\Http;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class Method extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const GET = 'GET';

	/**
	 * 
	 *
	 * 
	 */
	const HEAD = 'HEAD';

	/**
	 * 
	 *
	 * 
	 */
	const POST = 'POST';

	/**
	 * 
	 *
	 * 
	 */
	const PUT = 'PUT';

	/**
	 * 
	 *
	 * 
	 */
	const DELETE = 'DELETE';

	/**
	 * 
	 *
	 * 
	 */
	const CONNECT = 'CONNECT';

	/**
	 * 
	 *
	 * 
	 */
	const OPTIONS = 'OPTIONS';

	/**
	 * 
	 *
	 * 
	 */
	const TRACE = 'TRACE';

	/**
	 * 
	 *
	 * 
	 */
	const PATCH = 'PATCH';

}

?>
