<?php

namespace VTCompose\Data;

use VTCompose\Collection\IList;
use VTCompose\Exception\ArgumentException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
class ExpressionArray implements IExpression {

	private $elementExpressions;
	private $elementType;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 * @param string 
	 * @throws ArgumentException .
	 */
	public function __construct(IList $elementExpressions, $elementType = '') {
		if ($elementExpressions->count() == 0 && StringUtils::length($elementType) == 0) {
			throw new ArgumentException('Element expression list and element type are empty.');
		}
	
		$this->elementExpressions = $elementExpressions;
		$this->elementType = $elementType;
		
		$this->sqlNotationCache = [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache[$numExistingPlaceholders])) {
			$originalNumPlaceholders = $numExistingPlaceholders;
			$elementExpressions = '';
			foreach ($this->elementExpressions as $elementExpression) {
				$elementExpressions .= $elementExpression->getSqlNotation($numExistingPlaceholders) . ', ';
			}
			$sqlNotation = 'ARRAY[' . StringUtils::substring($elementExpressions, 0, -2) . ']';
			if (StringUtils::length($this->elementType) > 0) {
				$sqlNotation .= '::' . $this->elementType . '[]';
			}
			$this->sqlNotationCache[$originalNumPlaceholders] = [$sqlNotation, $numExistingPlaceholders];
		} else {
			list($sqlNotation, $numExistingPlaceholders) = $this->sqlNotationCache[$numExistingPlaceholders];
		}
		return $sqlNotation;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		$parameters = [];
		foreach ($this->elementExpressions as $elementExpression) {
			$parameters = array_merge($parameters, $elementExpression->getParameters());
		}
		return $parameters;
	}

}

?>
