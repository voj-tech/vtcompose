<?php

namespace VTCompose\Data;

use ArrayAccess;
use Closure;
use VTCompose\Collection\ISet;
use VTCompose\Collection\Set;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\Exception;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class DataRow implements ArrayAccess {

	private $items;
	private $rowState;
	private $modifiedColumnIdSet;
	private $table;
	
	private $accessor;
	private $rowCollectionAccessor;
	
	private function throwRowStateInvalidException() {
		throw new Exception('Row state is invalid.');
	}
	
	private function throwDeletedRowInaccessibleException() {
		throw new DeletedRowInaccessibleException('The DataRow was deleted.');
	}
	
	private function getEquivalentRealVersion($version) {
		if ($version == DataRowVersion::DATA_ROW_VERSION_DEFAULT) {
			switch ($this->rowState) {
			case DataRowState::DETACHED:
				$version = DataRowVersion::PROPOSED;
				break;
			
			case DataRowState::UNCHANGED:
			case DataRowState::ADDED:
			case DataRowState::MODIFIED:
				$version = DataRowVersion::CURRENT;
				break;
				
			case DataRowState::DELETED:
				$version = DataRowVersion::ORIGINAL;
				break;
				
			default:
				$this->throwRowStateInvalidException();
			}
		}
		
		if ($this->rowState == DataRowState::UNCHANGED && $version == DataRowVersion::ORIGINAL) {
			return DataRowVersion::CURRENT;
		}
		
		return $version;
	}
	
	private function setRowState($rowState) {
		static $errorMessage = 'Attempted an illegal row state transition.';
	
		switch ($rowState) {
		case DataRowState::DETACHED:
			if ($this->rowState == DataRowState::ADDED) {
				$this->items[DataRowVersion::PROPOSED] = $this->items[DataRowVersion::CURRENT];
				unset($this->items[DataRowVersion::CURRENT]);
				
			} elseif ($this->rowState == DataRowState::DELETED) {
				$this->items[DataRowVersion::PROPOSED] = $this->items[DataRowVersion::ORIGINAL];
				unset($this->items[DataRowVersion::ORIGINAL]);
				
			} else {
				throw new Exception($errorMessage);
			}
			break;
		
		case DataRowState::UNCHANGED:
			if ($this->rowState == DataRowState::ADDED) {
			
			} elseif ($this->rowState == DataRowState::DELETED || $this->rowState == DataRowState::MODIFIED) {
				$this->items[DataRowVersion::CURRENT] = $this->items[DataRowVersion::ORIGINAL];
				unset($this->items[DataRowVersion::ORIGINAL]);
				
			} else {
				throw new Exception($errorMessage);
			}
			break;
		
		case DataRowState::ADDED:
			if ($this->rowState == DataRowState::DETACHED) {
				$this->items[DataRowVersion::CURRENT] = $this->items[DataRowVersion::PROPOSED];
				unset($this->items[DataRowVersion::PROPOSED]);
				
			} elseif ($this->rowState == DataRowState::UNCHANGED) {
			
			} else {
				throw new Exception($errorMessage);
			}
			break;
		
		case DataRowState::DELETED:
			if ($this->rowState == DataRowState::UNCHANGED) {
				$this->items[DataRowVersion::ORIGINAL] = $this->items[DataRowVersion::CURRENT];
				unset($this->items[DataRowVersion::CURRENT]);
				
			} elseif ($this->rowState == DataRowState::MODIFIED) {
				unset($this->items[DataRowVersion::CURRENT]);
				
			} else {
				throw new Exception($errorMessage);
			}
			break;
		
		case DataRowState::MODIFIED:
			if ($this->rowState == DataRowState::UNCHANGED) {
				$this->items[DataRowVersion::ORIGINAL] = $this->items[DataRowVersion::CURRENT];
				
			} else {
				throw new Exception($errorMessage);
			}
			break;
		
		default:
			$this->throwRowStateInvalidException();
		}
		
		$this->rowState = $rowState;
	}
	
	private function checkRowStateNotDetached() {
		if ($this->rowState == DataRowState::DETACHED) {
			throw new RowNotInTableException('The DataRow is detached.');
		}
	}
	
	private function checkRowStateNotDeleted() {
		if ($this->rowState == DataRowState::DELETED) {
			$this->throwDeletedRowInaccessibleException();
		}
	}
	
	private function checkTableInDataSet() {
		if ($this->table->getDataSet() == NULL) {
			throw new InvalidOperationException('The DataTable does not belong to a DataSet.');
		}
	}
	
	private function getColumn($columnId) {
		$columns = $this->table->getInfo()->getColumns();
		if (!isset($columns[$columnId])) {
			throw new ArgumentException('The column specified by column id cannot be found.');
		}
		
		return $columns[$columnId];
	}
	
	private function getParentRelationship($parentRelationshipId) {
		$parentRelationships = $this->table->getInfo()->getParentRelationships();
		if (!isset($parentRelationships[$parentRelationshipId])) {
			$message = 'The parent relationship specified by parent relationship id cannot be found.';
			throw new ArgumentException($message);
		}
		
		return $parentRelationships[$parentRelationshipId];
	}
	
	private function getParentRelationshipObject($parentRelationship) {
		if (is_int($parentRelationship) || is_string($parentRelationship)) {
			return $this->getParentRelationship($parentRelationship);
		}
		
		if ($parentRelationship->getReferencingTable() !== $this->table->getInfo()) {
			throw new ArgumentException('The parent relationship and row do not belong to the same table.');
		}
		
		return $parentRelationship;
	}
	
	private function realVersionIsValid($realVersion) {
		return isset($this->items[$realVersion]);
	}
	
	private function columnIdIsValid($realVersion, $columnId) {
		return array_key_exists($columnId, $this->items[$realVersion]);
	}
	
	private function validateRealVersion($realVersion) {
		if (!$this->realVersionIsValid($realVersion)) {
			throw new VersionNotFoundException('The DataRow does not have this version of data.');
		}
	}
	
	private function validateColumnId($realVersion, $columnId) {
		if (!$this->columnIdIsValid($realVersion, $columnId)) {
			throw new ArgumentException('The item specified by column id cannot be found.');
		}
	}
	
	private function validateTableId($tableId) {
		if (!$this->table->getDataSet()->getTables()->containsKey($tableId)) {
			throw new ArgumentException('The DataTable specified by table id cannot be found.');
		}
	}
	
	private function setItemValue($realVersion, $columnId, $value) {
		$items = $this->items[$realVersion];
		if ($this->columnIdIsValid($realVersion, $columnId) && $items[$columnId] === $value) {
			return;
		}
		
		if ($this->rowState == DataRowState::UNCHANGED) {
			$this->setRowState(DataRowState::MODIFIED);
		}
	
		$this->items[$realVersion][$columnId] = $value;
		
		if ($this->rowState == DataRowState::MODIFIED) {
			$this->modifiedColumnIdSet->add($columnId);
		}
	}
	
	private function removeFromTableAndSetDetached() {
		$this->rowCollectionAccessor->__invoke('removeInternal', $this);
		$this->setRowState(DataRowState::DETACHED);
	}
	
	/**
	 * @todo When there are set operations in the ISet interface use the intersect operation here.
	 */
	private function clearModifiedColumnIdsAndSetUnchanged($acceptChanges, ISet $columnIdSet = NULL) {
		$this->rowCollectionAccessor->__invoke('removeFromIndexes', $this);
		
		if ($columnIdSet != NULL) {
			if ($columnIdSet->count() == 0) {
				throw new ArgumentException('Column id set is empty.');
			}
			
			$originalItems = &$this->items[DataRowVersion::ORIGINAL];
			$currentItems = &$this->items[DataRowVersion::CURRENT];
			
			$columnIds = array_intersect($this->modifiedColumnIdSet->toArray(), $columnIdSet->toArray());
			foreach ($columnIds as $columnId) {
				if ($acceptChanges) {
					$originalItems[$columnId] = $currentItems[$columnId];
				} else {
					if ($this->columnIdIsValid(DataRowVersion::ORIGINAL, $columnId)) {
						$currentItems[$columnId] = $originalItems[$columnId];
					} else {
						unset($currentItems[$columnId]);
					}
				}
				
				$this->modifiedColumnIdSet->remove($columnId);
			}
		} else {
			if ($acceptChanges) {
				$this->items[DataRowVersion::ORIGINAL] = $this->items[DataRowVersion::CURRENT];
			}
			
			$this->modifiedColumnIdSet->clear();
		}
		
		if ($this->modifiedColumnIdSet->count() == 0) {
			$this->setRowState(DataRowState::UNCHANGED);
		}
		
		$this->rowCollectionAccessor->__invoke('addToIndexes', $this);
	}
	
	private function clearModifiedColumnIdsAndSetDeleted() {
		$this->rowCollectionAccessor->__invoke('removeFromIndexes', $this);
		$this->modifiedColumnIdSet->clear();
		$this->setRowState(DataRowState::DELETED);
		$this->rowCollectionAccessor->__invoke('addToIndexes', $this);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function setItem($columnId, $value) {
		$this->checkRowStateNotDeleted();
		
		$column = $this->getColumn($columnId);
		$value = $column->filterValue($value);
		
		if ($column->isKey()) {
			$this->rowCollectionAccessor->__invoke('removeFromIndexes', $this);
		}
		
		$realVersion = $this->getEquivalentRealVersion(DataRowVersion::DATA_ROW_VERSION_DEFAULT);
		$this->setItemValue($realVersion, $columnId, $value);
		
		if ($column->isKey()) {
			$this->rowCollectionAccessor->__invoke('addToIndexes', $this);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param int 
	 * @return mixed 
	 */
	public function getItem($columnId, $version) {
		$realVersion = $this->getEquivalentRealVersion($version);
		
		if ($realVersion == DataRowVersion::CURRENT) {
			$this->checkRowStateNotDeleted();
		}
	
		$this->getColumn($columnId);
		$this->validateRealVersion($realVersion);
		$this->validateColumnId($realVersion, $columnId);
		
		return $this->items[$realVersion][$columnId];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param array 
	 */
	public function setItemArray(array $array) {
		$this->checkRowStateNotDeleted();
		
		$updateIndexes = false;
		foreach ($array as $columnId => &$value) {
			$column = $this->getColumn($columnId);
			$value = $column->filterValue($value);
			if ($column->isKey()) {
				$updateIndexes = true;
			}
		}
		unset($value);
		
		if ($updateIndexes) {
			$this->rowCollectionAccessor->__invoke('removeFromIndexes', $this);
		}
		
		$realVersion = $this->getEquivalentRealVersion(DataRowVersion::DATA_ROW_VERSION_DEFAULT);
		foreach ($array as $columnId => $value) {
			$this->setItemValue($realVersion, $columnId, $value);
		}
		
		if ($updateIndexes) {
			$this->rowCollectionAccessor->__invoke('addToIndexes', $this);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getItemArray() {
		return $this->items[$this->getEquivalentRealVersion(DataRowVersion::DATA_ROW_VERSION_DEFAULT)];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getRowState() {
		return $this->rowState;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return ISet 
	 * @todo When there are read-only versions of collections return a read-only wrapper here.
	 */
	public function getModifiedColumnIdSet() {
		return clone $this->modifiedColumnIdSet;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return DataTable 
	 */
	public function getTable() {
		return $this->table;
	}
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}
	
	private function setRowCollectionAccessor(Closure $rowCollectionAccessor) {
		$this->rowCollectionAccessor = $rowCollectionAccessor;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param DataTable 
	 */
	public function __construct(DataTable $table) {
		$table->establishFriendshipWithRow($this, $this->getAccessor());
		
		$this->items = [DataRowVersion::PROPOSED => []];
		$this->rowState = DataRowState::DETACHED;
		$this->modifiedColumnIdSet = new Set();
		$this->table = $table;
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	private function copy(DataRow $row) {
		$originalRowState = $this->rowState;
	
		if ($originalRowState != DataRowState::DETACHED) {
			if ($row->rowState != DataRowState::DETACHED) {
				$this->rowCollectionAccessor->__invoke('removeFromIndexes', $this);
			} else {
				$this->rowCollectionAccessor->__invoke('removeInternal', $this);
			}
		}
		
		$this->items = $row->items;
		$this->rowState = $row->rowState;
		$this->modifiedColumnIdSet = clone $row->modifiedColumnIdSet;
		
		if ($row->rowState != DataRowState::DETACHED) {
			if ($originalRowState != DataRowState::DETACHED) {
				$this->rowCollectionAccessor->__invoke('addToIndexes', $this);
			} else {
				$index = $this->table->getRows()->count();
				$this->rowCollectionAccessor->__invoke('insertInternal', $index, $this);
			}
		}
	}
	
	/**
	 * 
	 *
	 * 
	 */
	public function delete() {
		if ($this->rowState == DataRowState::UNCHANGED || $this->rowState == DataRowState::MODIFIED) {
			$this->clearModifiedColumnIdsAndSetDeleted();
			
		} elseif ($this->rowState == DataRowState::ADDED) {
			$this->removeFromTableAndSetDetached();
			
		} else {
			$this->throwDeletedRowInaccessibleException();
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws InvalidOperationException .
	 */
	public function setAdded() {
		if ($this->rowState == DataRowState::UNCHANGED) {
			$this->setRowState(DataRowState::ADDED);
			$this->rowCollectionAccessor->__invoke('updateIndexes', $this);
			
		} elseif ($this->rowState == DataRowState::ADDED) {
			
		} else {
			throw new InvalidOperationException('Row state is not Unchanged or Added.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 * 
	 * @param ISet 
	 * @throws ArgumentException .
	 * @throws InvalidOperationException .
	 * @todo When there are set operations in the ISet interface use the intersect operation here.
	 */
	public function setModified(ISet $columnIdSet = NULL) {
		if ($this->rowState != DataRowState::UNCHANGED && $this->rowState != DataRowState::MODIFIED) {
			throw new InvalidOperationException('Row state is not Unchanged or Modified.');
		}
	
		$columnIds = array_keys($this->items[DataRowVersion::CURRENT]);
		if ($columnIdSet != NULL && count($columnIds) > 0) {
			$columnIds = array_intersect($columnIds, $columnIdSet->toArray());
			if (count($columnIds) == 0) {
				$message = 'Column id set does not contain a column id specifying an item that can be found.';
				throw new ArgumentException($message);
			}
		}
		
		if ($this->rowState == DataRowState::UNCHANGED) {
			$this->setRowState(DataRowState::MODIFIED);
		}
		
		foreach ($columnIds as $columnId) {
			$this->modifiedColumnIdSet->add($columnId);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param DataRow 
	 * @throws ArgumentException .
	 * @throws ArgumentException .
	 */
	public function setParentRow($parentRelationship, DataRow $parentRow = NULL) {
		$this->checkRowStateNotDetached();
	
		$parentRelationship = $this->getParentRelationshipObject($parentRelationship);
		$referencedColumnIds = $parentRelationship->getReferencedColumnIds();
		$values = [];
		
		if ($parentRow != NULL) {
			$parentRow->checkRowStateNotDetached();
			
			if ($parentRow->table->getInfo() !== $parentRelationship->getReferencedTable()) {
				throw new ArgumentException('The parent row does not belong to the referenced table of the ' .
						'parent relationship.');
			}
			
			$itemArray = $parentRow->getItemArray();
			foreach ($referencedColumnIds as $childColumnId => $parentColumnId) {
				if (!isset($itemArray[$parentColumnId])) {
					$message = 'The item specified by parent column id cannot be found or is NULL.';
					throw new ArgumentException($message);
				}
				
				$values[$childColumnId] = $itemArray[$parentColumnId];
			}
		} else {
			foreach ($referencedColumnIds as $childColumnId => $parentColumnId) {
				$values[$childColumnId] = NULL;
			}
		}
		
		$this->setItemArray($values);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param int 
	 * @return DataRow 
	 */
	public function getParentRow($parentRelationship, $version = DataRowVersion::DATA_ROW_VERSION_DEFAULT) {
		$this->checkRowStateNotDetached();
		$this->checkTableInDataSet();
		
		$parentRelationship = $this->getParentRelationshipObject($parentRelationship);
		$parentTableId = $parentRelationship->getReferencedTable()->getId();
		$this->validateTableId($parentTableId);
		
		$key = [];
		foreach ($parentRelationship->getReferencedColumnIds() as $childColumnId => $parentColumnId) {
			$key[$parentColumnId] = $this->getItem($childColumnId, $version);
			if ($key[$parentColumnId] === NULL) {
				return NULL;
			}
		}
		
		if ($version == DataRowVersion::DATA_ROW_VERSION_DEFAULT) {
			$version = DataRowVersion::CURRENT;
		}
		
		return $this->table->getDataSet()->getTables()[$parentTableId]->getRows()->find($key, $version);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return bool 
	 */
	public function hasVersion($version) {
		return $this->realVersionIsValid($this->getEquivalentRealVersion($version));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param int 
	 * @return bool 
	 */
	public function itemExists($columnId, $version) {
		$realVersion = $this->getEquivalentRealVersion($version);
		
		$this->getColumn($columnId);
		$this->validateRealVersion($realVersion);
		
		return $this->columnIdIsValid($realVersion, $columnId);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param ISet 
	 */
	public function acceptChanges(ISet $columnIdSet = NULL) {
		$this->checkRowStateNotDetached();
	
		if ($this->rowState == DataRowState::UNCHANGED) {
			
		} elseif ($this->rowState == DataRowState::ADDED) {
			$this->setRowState(DataRowState::UNCHANGED);
			$this->rowCollectionAccessor->__invoke('updateIndexes', $this);
			
		} elseif ($this->rowState == DataRowState::DELETED) {
			$this->removeFromTableAndSetDetached();
			
		} else {
			$acceptChanges = true;
			$this->clearModifiedColumnIdsAndSetUnchanged($acceptChanges, $columnIdSet);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param ISet 
	 */
	public function rejectChanges(ISet $columnIdSet = NULL) {
		$this->checkRowStateNotDetached();
	
		if ($this->rowState == DataRowState::UNCHANGED) {
			
		} elseif ($this->rowState == DataRowState::ADDED) {
			$this->removeFromTableAndSetDetached();
			
		} elseif ($this->rowState == DataRowState::DELETED) {
			$this->setRowState(DataRowState::UNCHANGED);
			$this->rowCollectionAccessor->__invoke('updateIndexes', $this);
			
		} else {
			$acceptChanges = false;
			$this->clearModifiedColumnIdsAndSetUnchanged($acceptChanges, $columnIdSet);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function offsetExists($offset): bool {
		return $this->itemExists($offset, DataRowVersion::DATA_ROW_VERSION_DEFAULT);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function offsetGet($offset): mixed {
		return $this->getItem($offset, DataRowVersion::DATA_ROW_VERSION_DEFAULT);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function offsetSet($offset, $value): void {
		$this->setItem($offset, $value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function offsetUnset($offset): void {
		$this->getColumn($offset);
		$realVersion = $this->getEquivalentRealVersion(DataRowVersion::DATA_ROW_VERSION_DEFAULT);
		$this->validateColumnId($realVersion, $offset);
		
		$this->rowCollectionAccessor->__invoke('removeFromIndexes', $this);
		
		if ($this->modifiedColumnIdSet->remove($offset) && $this->modifiedColumnIdSet->count() == 0) {
			$this->setRowState(DataRowState::UNCHANGED);
		}
		
		unset($this->items[$realVersion][$offset]);
		
		$this->rowCollectionAccessor->__invoke('addToIndexes', $this);
	}

}

?>
