<?php

namespace VTCompose\Data;

use VTCompose\Exception\InvalidOperationException;
use VTCompose\Text\RegularExpressions\Regex;

/**
 * 
 *
 * 
 */
class ExpressionLiteral implements IExpression {

	private $value;
	private $stringLiteralFormatter;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param callable 
	 */
	public function __construct($value, callable $stringLiteralFormatter = NULL) {
		$this->value = $value;
		$this->stringLiteralFormatter = $stringLiteralFormatter;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 * @throws InvalidOperationException .
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache)) {
			if (is_int($this->value)) {
				$this->sqlNotationCache = (string) $this->value;
				
			} elseif (is_string($this->value)) {
				if ($this->stringLiteralFormatter != NULL) {
					$stringLiteralFormatter = $this->stringLiteralFormatter;
					$this->sqlNotationCache = $stringLiteralFormatter($this->value);
				} elseif (self::getAsciiPrintableCharactersOnlyRegex()->isMatch($this->value)) {
					$this->sqlNotationCache = '\'' . str_replace('\'', '\'\'', $this->value) . '\'';
				} else {
					$message = 'String contains not only printable ASCII characters.';
					throw new InvalidOperationException($message);
				}
				
			} elseif (is_bool($this->value)) {
				$this->sqlNotationCache = $this->value ? '\'t\'' : '\'f\'';
				
			} elseif (is_null($this->value)) {
				$this->sqlNotationCache = 'NULL';
				
			} elseif (is_float($this->value)) {
				$string = (string) $this->value;
			
				switch ($string) {
				case 'INF':
					$this->sqlNotationCache = '\'Infinity\'';
					
				case '-INF':
					$this->sqlNotationCache = '\'-Infinity\'';
					
				case 'NAN':
					$this->sqlNotationCache = '\'NaN\'';
					
				default:
					$this->sqlNotationCache = $string;
				}
			} else {
				throw new InvalidOperationException('Value is not scalar or NULL.');
			}
		}
		
		return $this->sqlNotationCache;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		return [];
	}
	
	private static function getAsciiPrintableCharactersOnlyRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[ -~]*\\z');
		}
		
		return $regex;
	}

}

?>
