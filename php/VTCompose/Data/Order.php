<?php

namespace VTCompose\Data;

/**
 * 
 *
 * 
 */
class Order {

	private $expression;
	private $direction;
	private $nulls;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IExpression 
	 * @param int 
	 * @param int 
	 */
	public function __construct(IExpression $expression,
			$direction = OrderDirection::ASCENDING, $nulls = OrderNulls::LAST) {
		$this->expression = $expression;
		$this->direction = $direction;
		$this->nulls = $nulls;
		
		$this->sqlNotationCache = [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache[$numExistingPlaceholders])) {
			$originalNumPlaceholders = $numExistingPlaceholders;
			$sqlNotation = $this->expression->getSqlNotation($numExistingPlaceholders);
			if ($this->direction == OrderDirection::DESCENDING) {
				$sqlNotation .= ' DESC';
				if ($this->nulls == OrderNulls::LAST) {
					$sqlNotation .= ' NULLS LAST';
				}
			} else {
				if ($this->nulls == OrderNulls::FIRST) {
					$sqlNotation .= ' NULLS FIRST';
				}
			}
			$this->sqlNotationCache[$originalNumPlaceholders] = [$sqlNotation, $numExistingPlaceholders];
		} else {
			list($sqlNotation, $numExistingPlaceholders) = $this->sqlNotationCache[$numExistingPlaceholders];
		}
		return $sqlNotation;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		return $this->expression->getParameters();
	}

}

?>
