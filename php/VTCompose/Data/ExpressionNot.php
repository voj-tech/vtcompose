<?php

namespace VTCompose\Data;

/**
 * 
 *
 * 
 */
class ExpressionNot implements IBooleanExpression {

	private $childExpression;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IBooleanExpression 
	 */
	public function __construct(IBooleanExpression $childExpression) {
		$this->childExpression = $childExpression;
		
		$this->sqlNotationCache = [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache[$numExistingPlaceholders])) {
			$originalNumPlaceholders = $numExistingPlaceholders;
			$sqlNotation = 'NOT (' . $this->childExpression->getSqlNotation($numExistingPlaceholders) . ')';
			$this->sqlNotationCache[$originalNumPlaceholders] = [$sqlNotation, $numExistingPlaceholders];
		} else {
			list($sqlNotation, $numExistingPlaceholders) = $this->sqlNotationCache[$numExistingPlaceholders];
		}
		return $sqlNotation;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		return $this->childExpression->getParameters();
	}

}

?>
