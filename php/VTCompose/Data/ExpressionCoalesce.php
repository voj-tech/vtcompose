<?php

namespace VTCompose\Data;

use VTCompose\Exception\ArgumentException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
class ExpressionCoalesce implements IExpression {

	private $childExpressions;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IExpression 
	 * @throws ArgumentException .
	 */
	public function __construct(IExpression ...$childExpressions) {
		if (count($childExpressions) == 0) {
			throw new ArgumentException('No child expression.');
		}
		
		$this->childExpressions = $childExpressions;
		
		$this->sqlNotationCache = [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache[$numExistingPlaceholders])) {
			$originalNumPlaceholders = $numExistingPlaceholders;
			$childExpressions = '';
			foreach ($this->childExpressions as $childExpression) {
				$childExpressions .= $childExpression->getSqlNotation($numExistingPlaceholders) . ', ';
			}
			$sqlNotation = 'COALESCE(' . StringUtils::substring($childExpressions, 0, -2) . ')';
			$this->sqlNotationCache[$originalNumPlaceholders] = [$sqlNotation, $numExistingPlaceholders];
		} else {
			list($sqlNotation, $numExistingPlaceholders) = $this->sqlNotationCache[$numExistingPlaceholders];
		}
		return $sqlNotation;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		$parameters = [];
		foreach ($this->childExpressions as $childExpression) {
			$parameters = array_merge($parameters, $childExpression->getParameters());
		}
		return $parameters;
	}

}

?>
