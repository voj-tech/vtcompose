<?php

namespace VTCompose\Data;

/**
 * 
 *
 * 
 */
interface IExpression {

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0);

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters();

}

?>
