<?php

namespace VTCompose\Data\DatabaseConnection;

/**
 * 
 *
 * 
 */
abstract class Transaction {

	private $connection;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Connection 
	 */
	public function getConnection() {
		return $this->connection;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Connection 
	 */
	public function __construct(Connection $connection) {
		$this->connection = $connection;
	}

	/**
	 * 
	 *
	 * 
	 */
	abstract public function commit();
	
	/**
	 * 
	 *
	 * 
	 */
	abstract public function rollback();

}

?>
