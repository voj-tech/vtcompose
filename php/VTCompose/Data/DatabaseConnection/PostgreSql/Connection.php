<?php

namespace VTCompose\Data\DatabaseConnection\PostgreSql;

use VTCompose\Data\DatabaseConnection\CommandParameter\IStructuredParameter;
use VTCompose\Data\DatabaseConnection\Exception;
use VTCompose\Data\ExpressionLiteral;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 *
 * @todo Implement parsing and building of the connection string and alternative configuration of the
 * parameters through individual setters and getters.
 */
class Connection extends \VTCompose\Data\DatabaseConnection\Connection {

	private $connection;
	private $isOpen;
	
	private $connectionString;
	
	private $numPreparedStatements;
	private $stringLiteralFormatter;

	/**
	 * 
	 *
	 * 
	 *
	 * @param string connection string
	 */
	final protected function setConnectionStringImpl($connectionString) {
		$this->connectionString = $connectionString;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string the connection string
	 */
	public function getConnectionString() {
		return $this->connectionString;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool does the object represent an open connection?
	 */
	final public function isOpen() {
		return $this->isOpen;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function __construct($connectionString = '') {
		$this->setConnectionStringImpl($connectionString);
		$this->isOpen = false;
	}
	
	/**
	 * 
	 *
	 * Opening the connection is done by calling the openImpl() method.
	 */
	public function __clone() {
		if ($this->isOpen()) {
			$this->openImpl();
		}
	}
	
	/**
	 * Reestablishes the connection to a database server if the serialized object represents an open one.
	 *
	 * Reestablishing the connection is done by calling the openImpl() method.
	 */
	public function __wakeup() {
		if ($this->isOpen()) {
			$this->openImpl();
		}
	}
	
	/**
	 * Closes the connection to a database server if the object represents an open one.
	 *
	 * Closing the connection is done by calling the closeImpl() method.
	 */
	public function __destruct() {
		if ($this->isOpen()) {
			$this->closeImpl();
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws Exception if opening the connection fails.
	 */
	final protected function openImpl() {
		$this->connection = pg_pconnect($this->connectionString);
		if (!$this->connection) {
			throw new Exception('Unable to open a connection.');
		}
		
		$this->isOpen = true;
		$this->numPreparedStatements = 0;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws Exception if closing the connection fails.
	 */
	final protected function closeImpl() {
		$this->deallocate();
	
		if (!pg_close($this->connection)) {
			throw new Exception('Unable to close a connection.');
		}

		$this->connection = NULL;
		$this->isOpen = false;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return \VTCompose\Data\DatabaseConnection\Command 
	 */
	public function createCommand() {
		$command = new Command();
		$command->setConnection($this);
		return $command;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return \VTCompose\Data\DatabaseConnection\Transaction 
	 */
	public function startTransaction() {
		return new Transaction($this);
	}
	
	/**
	 * Performs an SQL statement.
	 *
	 * 
	 *
	 * @param string the query to be executed
	 * @return DataReader 
	 * @throws Exception if it is not able to dispatch the request.
	 * @throws Exception if no query result is available.
	 */
	final public function query($query) {
		if (!pg_send_query($this->connection, $query)) {
			throw new Exception('Unable to dispatch a request.');
		}
		
		$result = pg_get_result($this->connection);
		if (!$result) {
			throw new Exception('No result.');
		}
		
		return new DataReader($this, $result);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 */
	public function prepare($query) {
		$statementName = 'stmt' . $this->numPreparedStatements++;
		$this->query('PREPARE ' . pg_escape_identifier($this->connection, $statementName) . ' AS ' . $query);
		return $statementName;
	}
	
	private function getStringLiteralFormatter() {
		if (!isset($this->stringLiteralFormatter)) {
			$this->stringLiteralFormatter = function($data) {
				return pg_escape_literal($this->connection, $data);
			};
		}
		
		return $this->stringLiteralFormatter;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param array 
	 * @return DataReader 
	 */
	public function execute($statementName, array $parameters = []) {
		$restOfQuery = pg_escape_identifier($this->connection, $statementName);
		
		if (count($parameters) > 0) {
			$restOfQuery .= '(';
			$stringLiteralFormatter = $this->getStringLiteralFormatter();
			foreach ($parameters as $parameter) {
				if ($parameter instanceof IStructuredParameter) {
					$expression = $parameter->getExpression($stringLiteralFormatter);
				} else {
					$expression = new ExpressionLiteral($parameter, $stringLiteralFormatter);
				}
				$restOfQuery .= $expression->getSqlNotation() . ', ';
			}
			$restOfQuery = StringUtils::substring($restOfQuery, 0, -2) . ')';
		}

		return $this->query('EXECUTE ' . $restOfQuery);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	final public function deallocate($statementName = '') {
		$restOfQuery = StringUtils::length($statementName) > 0 ?
				pg_escape_identifier($this->connection, $statementName) : 'ALL';
		$this->query('DEALLOCATE ' . $restOfQuery);
	}

}

?>
