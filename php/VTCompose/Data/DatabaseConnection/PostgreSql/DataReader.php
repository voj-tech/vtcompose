<?php

namespace VTCompose\Data\DatabaseConnection\PostgreSql;

use PgSql\Result as PgSqlResult;
use VTCompose\Data\DatabaseConnection\Exception;
use VTCompose\Data\DatabaseConnection\SqlException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class DataReader extends \VTCompose\Data\DatabaseConnection\DataReader {

	private $result;
	private $numRows;
	private $numAffectedRows;
	
	private $values;

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 * @throws Exception .
	 */
	public function hasRows() {
		if (!isset($this->numRows)) {
			$this->numRows = pg_num_rows($this->result);
			if ($this->numRows < 0) {
				throw new Exception('Unable to retrieve the number of rows in a query result.');
			}
		}
		
		return $this->numRows > 0;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNumAffectedRows() {
		if (!isset($this->numAffectedRows)) {
			$this->numAffectedRows = pg_affected_rows($this->result);
		}
		
		return $this->numAffectedRows;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	final public function isClosed() {
		return $this->result === NULL;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param Connection 
	 * @param PgSqlResult 
	 * @throws Exception if retrieving the "SQLSTATE" code for the query result fails.
	 * @throws SqlException .
	 */
	public function __construct(Connection $connection, PgSqlResult $result) {
		parent::__construct($connection);
		
		static $array = [PGSQL_EMPTY_QUERY, PGSQL_BAD_RESPONSE, PGSQL_NONFATAL_ERROR, PGSQL_FATAL_ERROR];
		if (in_array(pg_result_status($result), $array)) {
			$sqlstateCode = pg_result_error_field($result, PGSQL_DIAG_SQLSTATE);
			if (!is_string($sqlstateCode)) {
				throw new Exception('Unable to retrieve the "SQLSTATE" code for a query result.');
			}
			
			throw new SqlException($sqlstateCode);
		}
		
		$this->result = $result;
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 */
	public function __destruct() {
		if (!$this->isClosed()) {
			$this->close();
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function read() {
		$this->values = pg_fetch_row($this->result);
		return $this->values !== false;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 * @throws Exception if there are no more rows in the query result or retrieving the values for the
	 * current row of the query result has failed.
	 */
	public function getValues() {
		if (!isset($this->values)) {
			$this->read();
		}
		
		if ($this->values === false) {
			throw new Exception('There are no more rows in a query result or there has been an error ' .
					'retrieving the values for the current row of the query result.');
		}
		
		return $this->values;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws Exception if freeing of the query result fails.
	 */
	final public function close() {
		if (!pg_free_result($this->result)) {
			throw new Exception('Unable to free a query result.');
		}
		
		$this->result = NULL;
	}

}

?>
