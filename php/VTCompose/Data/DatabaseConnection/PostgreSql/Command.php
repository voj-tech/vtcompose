<?php

namespace VTCompose\Data\DatabaseConnection\PostgreSql;

use VTCompose\Exception\InvalidOperationException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
class Command extends \VTCompose\Data\DatabaseConnection\Command {

	private $statementName;

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param Connection 
	 */
	public function __construct($commandText = '', Connection $connection = NULL) {
		parent::__construct($commandText, $connection);
		$this->reset();
	}
	
	/**
	 * 
	 *
	 * 
	 */
	final protected function reset() {
		$this->statementName = '';
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws InvalidOperationException .
	 */
	public function prepare() {
		if (StringUtils::length($this->getCommandText()) == 0) {
			throw new InvalidOperationException('The command text is empty.');
		}
	
		$this->statementName = $this->getConnection()->prepare($this->getCommandText());
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return DataReader 
	 * @throws InvalidOperationException .
	 */
	public function execute() {
		if (StringUtils::length($this->statementName) == 0) {
			throw new InvalidOperationException('Statement not prepared.');
		}
	
		return $this->getConnection()->execute($this->statementName, $this->getParameters()->toArray());
	}

}

?>
