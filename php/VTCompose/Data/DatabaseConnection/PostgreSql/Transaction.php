<?php

namespace VTCompose\Data\DatabaseConnection\PostgreSql;

/**
 * 
 *
 * 
 *
 * @todo Implement savepoints.
 */
class Transaction extends \VTCompose\Data\DatabaseConnection\Transaction {

	/**
	 * 
	 *
	 * 
	 *
	 * @param Connection 
	 */
	public function __construct(Connection $connection) {
		parent::__construct($connection);
		$this->getConnection()->query('BEGIN');
	}
	
	/**
	 * 
	 *
	 * 
	 */
	public function commit() {
		$this->getConnection()->query('COMMIT');
	}

	/**
	 * 
	 *
	 * 
	 */
	public function rollback() {
		$this->getConnection()->query('ROLLBACK');
	}

}

?>
