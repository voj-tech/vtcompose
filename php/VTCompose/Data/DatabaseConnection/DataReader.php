<?php

namespace VTCompose\Data\DatabaseConnection;

/**
 * 
 *
 * 
 */
abstract class DataReader {

	private $connection;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Connection 
	 */
	public function getConnection() {
		return $this->connection;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	abstract public function hasRows();

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	abstract public function getNumAffectedRows();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	abstract public function isClosed();

	/**
	 * 
	 *
	 * 
	 *
	 * @param Connection 
	 */
	public function __construct(Connection $connection) {
		$this->connection = $connection;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	abstract public function read();

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	abstract public function getValues();
	
	/**
	 * 
	 *
	 * 
	 */
	abstract public function close();

}

?>
