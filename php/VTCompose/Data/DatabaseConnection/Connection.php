<?php

namespace VTCompose\Data\DatabaseConnection;

use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
abstract class Connection {

	/**
	 * 
	 *
	 * 
	 *
	 * @param string connection string
	 */
	abstract protected function setConnectionStringImpl($connectionString);
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string connection string
	 * @throws InvalidOperationException if the object represents an open connection.
	 */
	public function setConnectionString($connectionString) {
		if ($this->isOpen()) {
			throw new InvalidOperationException('Object represents an open connection.');
		}
		
		$this->setConnectionStringImpl($connectionString);
	}
	
	/**
	 * Returns the connection string of the object.
	 *
	 * A non-empty connection string does not mean that the object represents an open connection.  Use the
	 * isOpen() method to check whether the object represents an open connection.
	 *
	 * @return string the connection string
	 */
	abstract public function getConnectionString();
	
	/**
	 * 
	 *
	 * 
	 */
	abstract protected function openImpl();
	
	/**
	 * 
	 *
	 * 
	 */
	abstract protected function closeImpl();

	/**
	 * Opens a connection to a database server.
	 *
	 * Opening the connection is done by calling the openImpl() method.
	 *
	 * @throws InvalidOperationException if the object represents an already open connection.
	 */
	public function open() {
		if ($this->isOpen()) {
			throw new InvalidOperationException('Object represents an already open connection.');
		}
		
		$this->openImpl();
	}
	
	/**
	 * Closes the connection to a database server.
	 *
	 * Open connection is closed automatically when destroying the object, however it can be done manually
	 * earlier to catch possible exceptions.  Closing the connection is done by calling the closeImpl()
	 * method.
	 *
	 * @throws InvalidOperationException if the object does not represent an open connection.
	 */
	public function close() {
		if (!$this->isOpen()) {
			throw new InvalidOperationException('Object does not represent an open connection.');
		}
		
		$this->closeImpl();
	}
	
	/**
	 * Checks whether the object represents an open connection.
	 *
	 * 
	 *
	 * @return bool does the object represent an open connection?
	 */
	abstract public function isOpen();

	/**
	 * 
	 *
	 * 
	 *
	 * @return Command 
	 */
	abstract public function createCommand();

	/**
	 * Begins a transaction block.
	 *
	 * 
	 *
	 * @return Transaction 
	 */
	abstract public function startTransaction();

}

?>
