<?php

namespace VTCompose\Data\DatabaseConnection;

use VTCompose\Exception\Exception;

/**
 * 
 *
 * Carries an additional value indicating the reason of the failure--the "SQLSTATE" code.
 */
class SqlException extends Exception {

	private $sqlstateCode;

	/**
	 * 
	 *
	 * Calls constructor of the parent class and sets the "SQLSTATE" code.
	 *
	 * @param string the "SQLSTATE" code
	 */
	public function __construct($sqlstateCode) {
		parent::__construct('Operation failed with "SQLSTATE" code ' . $sqlstateCode . '.');
		$this->sqlstateCode = $sqlstateCode;
	}

	/**
	 * Returns the "SQLSTATE" code associated with the exception.
	 *
	 * 
	 *
	 * @return string the "SQLSTATE" code
	 */
	public function getSqlstateCode() {
		return $this->sqlstateCode;
	}

}

?>
