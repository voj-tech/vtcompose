<?php

namespace VTCompose\Data\DatabaseConnection\CommandParameter;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;
use VTCompose\Data\ExpressionArray;
use VTCompose\Data\ExpressionLiteral;
use VTCompose\Data\IExpression;

/**
 * 
 *
 * 
 */
class ArrayParameter implements IStructuredParameter {

	private $elements;
	private $elementType;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 * @param string 
	 */
	public function __construct(IList $elements, $elementType = '') {
		$this->elements = $elements;
		$this->elementType = $elementType;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @return IExpression 
	 */
	public function getExpression(callable $stringLiteralFormatter) {
		$elementExpressions = new ArrayList();
		foreach ($this->elements as $element) {
			$elementExpressions->add(new ExpressionLiteral($element, $stringLiteralFormatter));
		}
		return new ExpressionArray($elementExpressions, $this->elementType);
	}

}

?>
