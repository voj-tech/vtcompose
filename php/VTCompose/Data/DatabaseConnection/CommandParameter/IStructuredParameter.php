<?php

namespace VTCompose\Data\DatabaseConnection\CommandParameter;

use VTCompose\Data\IExpression;

/**
 * 
 *
 * 
 */
interface IStructuredParameter {

	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @return IExpression 
	 */
	public function getExpression(callable $stringLiteralFormatter);

}

?>
