<?php

namespace VTCompose\Data\DatabaseConnection;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
abstract class Command {

	private $commandText;
	private $connection;
	private $parameters;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setCommandText($commandText) {
		if ($this->commandText != $commandText) {
			$this->commandText = $commandText;
			$this->reset();
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getCommandText() {
		return $this->commandText;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Connection 
	 */
	public function setConnection(Connection $connection) {
		if ($this->connection !== $connection) {
			$this->connection = $connection;
			$this->reset();
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Connection 
	 */
	public function getConnection() {
		return $this->connection;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setParameters(IList $parameters) {
		$this->parameters = $parameters;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getParameters() {
		return $this->parameters;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param Connection 
	 */
	public function __construct($commandText = '', Connection $connection = NULL) {
		$this->commandText = $commandText;
		$this->connection = $connection;
		$this->parameters = new ArrayList();
	}
	
	/**
	 * 
	 *
	 * 
	 */
	abstract protected function reset();
	
	/**
	 * 
	 *
	 * 
	 */
	abstract public function prepare();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return DataReader 
	 */
	abstract public function execute();

}

?>
