<?php

namespace VTCompose\Data;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;
use VTCompose\Collection\ISet;
use VTCompose\Collection\Set;
use VTCompose\Data\DatabaseConnection\Connection;
use VTCompose\Data\DatabaseModel\Column;
use VTCompose\Data\DatabaseModel\Database;
use VTCompose\Exception\ArgumentException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 *
 * @todo Once there's an addRange() method in the IList interface build command parameters as ArrayLists
 * straightaway.
 */
class DataAdapter {

	private $connection;
	private $commands;
	private $databaseModel;
	private $sortedTableColumns;
	private $rowPredicateCache;
	
	private $defaultColumnIdSets;
	private $defaultFollowedParentRelationshipIdSets;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Connection 
	 */
	public function getConnection() {
		return $this->connection;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Database 
	 */
	public function getDatabaseModel() {
		return $this->databaseModel;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param Connection 
	 * @param Database 
	 */
	public function __construct(Connection $connection, Database $databaseModel) {
		$this->connection = $connection;
		$this->commands = [];
		$this->databaseModel = $databaseModel;
		$this->sortedTableColumns = [];
		$this->rowPredicateCache = [];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 * @param ISet 
	 * @return int 
	 * @throws ArgumentException .
	 * @throws ArgumentException .
	 * @throws NumAffectedRowsException .
	 * @todo When there are set operations in the ISet interface use the intersect operation here.
	 */
	public function store($data, $tableId = NULL, ISet $columnIdSet = NULL) {	
		if ($data instanceof DataSet) {
			$dataTables = $data->getTables();
			$dataTable = $tableId !== NULL ? $dataTables[$tableId] : $dataTables->first()->getValue();
			$data = $dataTable->getRows();
		} else {
			if ($tableId !== NULL) {
				throw new ArgumentException('Table id is specified.');
			}
			
			if ($data instanceof DataTable) {
				$dataTable = $data;
				$data = $data->getRows();
			} else {
				foreach ($data as $dataRow) {
					if (isset($dataTable)) {
						if ($dataRow->getTable() !== $dataTable) {
							throw new ArgumentException('Not all DataRows belong to the same DataTable.');
						}
					} else {
						$dataTable = $dataRow->getTable();
					}
				}
			}
		}
		
		$table = $dataTable->getInfo();
		$tableId = $table->getId();
		
		if ($columnIdSet != NULL) {
			$this->validateColumnIdSet($tableId, $columnIdSet);
		} else {
			$columnIdSet = $this->getDefaultColumnIdSets()[$tableId];
		}
		
		$numAffectedDataRows = 0;
		
		$keyColumns = $table->getKeyColumns();
		
		foreach ($data as $dataRow) {
			switch ($dataRow->getRowState()) {
			case DataRowState::ADDED:
				$itemArray = $dataRow->getItemArray();
				$returningColumns = array_diff_key($keyColumns, $itemArray);
				$command = $this->getInsertCommand($tableId, $itemArray, $returningColumns);
				break;
			
			case DataRowState::DELETED:
				$returningColumns = [];
				$command = $this->getDeleteCommand($tableId, $this->getRowPredicate($dataRow));
				break;
			
			case DataRowState::MODIFIED:
				$modifiedColumnIdSet = $dataRow->getModifiedColumnIdSet();
				$columnIds = array_intersect($modifiedColumnIdSet->toArray(), $columnIdSet->toArray());
				$itemArray = array_intersect_key($dataRow->getItemArray(), array_flip($columnIds));
				$returningColumns = [];
				$command = $this->getUpdateCommand($tableId, $itemArray, $this->getRowPredicate($dataRow));
				break;
				
			default:
				continue 2;
			}
			
			$dataReader = $command->execute();
			if ($dataReader->getNumAffectedRows() != 1) {
				throw new NumAffectedRowsException('Unexpected number of affected rows.');
			}
			
			if (count($returningColumns) > 0) {
				$itemArray = [];
				$values = $dataReader->getValues();
				$index = 0;
				foreach ($returningColumns as $returningColumnId => $returningColumn) {
					$itemArray[$returningColumnId] = $returningColumn->toFrontendValue($values[$index++]);
				}
				$dataRow->setItemArray($itemArray);
			}
			
			$dataRow->acceptChanges($columnIdSet);
			$numAffectedDataRows++;
		}
		
		return $numAffectedDataRows;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param IBooleanExpression 
	 * @param array 
	 * @param array 
	 * @param mixed 
	 * @param int 
	 * @param int 
	 * @return DataSet 
	 */
	public function load($tableId, IBooleanExpression $condition = NULL,
			array $followedParentRelationshipIdSets = NULL, array $columnIdSets = NULL,
			$order = [], $limit = 0, $offset = 0) {
		if ($columnIdSets !== NULL) {
			foreach ($columnIdSets as $currentTableId => $columnIdSet) {
				$this->validateColumnIdSet($currentTableId, $columnIdSet);
			}
		} else {
			$columnIdSets = $this->getDefaultColumnIdSets();
		}
		
		$columns = [];
		$relationshipSetup = $this->buildParentRelationshipSetup($tableId, $followedParentRelationshipIdSets);
		$columnNames = '';
		
		foreach (array_intersect_key($columnIdSets, $relationshipSetup) as $currentTableId => $columnIdSet) {
			$this->validateColumnIdSet($currentTableId, $columnIdSet);
			
			$currentColumns = $this->getSortedTableColumns($currentTableId);
			$currentColumns = array_intersect_key($currentColumns, array_flip($columnIdSet->toArray()));
			$columns[$currentTableId] = $currentColumns;
			
			foreach ($currentColumns as $column) {
				$columnNames .= $column->getSqlNotation() . ', ';
			}
		}
		
		$tables = $this->databaseModel->getTables();
		
		$joinCommandTextPart = $this->buildJoinCommandTextPart($relationshipSetup);
		
		$commandText = 'SELECT ' . StringUtils::substring($columnNames, 0, -2) .
				' FROM ' . $tables[$tableId]->getSchemaQualifiedName() . $joinCommandTextPart;
		
		$numExistingPlaceholders = 0;
		
		if ($condition != NULL) {
			$commandText .= ' WHERE ' . $condition->getSqlNotation($numExistingPlaceholders);
			$parameters = $condition->getParameters();
		} else {
			$parameters = [];
		}

		if (!is_array($order)) {
			$order = [$order];
		}
		
		if (count($order) > 0) {
			$commandText .= ' ORDER BY ';
			foreach ($order as $orderPart) {
				$commandText .= $orderPart->getSqlNotation($numExistingPlaceholders) . ', ';
				$parameters = array_merge($parameters, $orderPart->getParameters());
			}
			$commandText = StringUtils::substring($commandText, 0, -2);
		}

		if ($limit > 0) {
			$commandText .= ' LIMIT $' . ++$numExistingPlaceholders;
			$parameters[] = $limit;
		}

		if ($offset > 0) {
			$commandText .= ' OFFSET $' . $numExistingPlaceholders;
			$parameters[] = $offset;
		}
		
		$dataReader = $this->getCommand($commandText, new ArrayList($parameters))->execute();
		
		$dataSet = new DataSet();
		
		$dataTables = $dataSet->getTables();
		foreach ($columns as $currentTableId => $currentColumns) {
			$dataTables->add(new DataTable($tables[$currentTableId]));
		}
		
		while ($dataReader->read()) {
			$values = $dataReader->getValues();
			$index = 0;
			
			foreach ($columns as $currentTableId => $currentColumns) {
				if ($values[$index] !== NULL) {
					$itemArray = [];
					foreach ($currentColumns as $columnId => $column) {
						$itemArray[$columnId] = $column->toFrontendValue($values[$index++]);
					}
					
					$dataTable = $dataTables[$currentTableId];
					$dataRow = $dataTable->newRow();
					$dataRow->setItemArray($itemArray);
					$dataTable->getRows()->add($dataRow);
					$dataRow->acceptChanges();
				} else {
					$index += count($currentColumns);
				}
			}
		}
		
		return $dataSet;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param IBooleanExpression 
	 * @param array 
	 * @return int 
	 */
	public function countRows($tableId, IBooleanExpression $condition = NULL,
			array $followedParentRelationshipIdSets = NULL) {
		$table = $this->databaseModel->getTables()[$tableId];
		$relationshipSetup = $this->buildParentRelationshipSetup($tableId, $followedParentRelationshipIdSets);
		$joinCommandTextPart = $this->buildJoinCommandTextPart($relationshipSetup);
		
		$commandText = 'SELECT count(1) FROM ' . $table->getSchemaQualifiedName() . $joinCommandTextPart;
		
		if ($condition != NULL) {
			$commandText .= ' WHERE ' . $condition->getSqlNotation();
			$parameters = $condition->getParameters();
		} else {
			$parameters = [];
		}
		
		$values = $this->getCommand($commandText, new ArrayList($parameters))->execute()->getValues();
		return (int) reset($values);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param array 
	 * @return int 
	 */
	public function performInsertCommand($tableId, array $data) {
		return $this->getInsertCommand($tableId, $data)->execute()->getNumAffectedRows();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param array 
	 * @param IBooleanExpression 
	 * @return int 
	 */
	public function performUpdateCommand($tableId, array $data, IBooleanExpression $condition = NULL) {
		return $this->getUpdateCommand($tableId, $data, $condition)->execute()->getNumAffectedRows();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param IBooleanExpression 
	 * @return int 
	 */
	public function performDeleteCommand($tableId, IBooleanExpression $condition = NULL) {
		return $this->getDeleteCommand($tableId, $condition)->execute()->getNumAffectedRows();
	}
	
	private function validateColumnIdSet($tableId, ISet $columnIdSet) {
		foreach ($this->databaseModel->getTables()[$tableId]->getKeyColumns() as $keyColumnId => $keyColumn) {
			if (!$columnIdSet->contains($keyColumnId)) {
				throw new ArgumentException('Column id set does not contain a key column id.');
			}
		}
	}
	
	private function getDefaultColumnIdSets() {
		if (!isset($this->defaultColumnIdSets)) {
			$this->defaultColumnIdSets = [];
			foreach ($this->databaseModel->getTables() as $tableId => $table) {
				$this->defaultColumnIdSets[$tableId] = new Set(array_keys($table->getColumns()));
			}
		}
		
		return $this->defaultColumnIdSets;
	}
	
	private function getDefaultFollowedParentRelationshipIdSets() {
		if (!isset($this->defaultFollowedParentRelationshipIdSets)) {
			$this->defaultFollowedParentRelationshipIdSets = [];
			foreach ($this->databaseModel->getTables() as $tableId => $table) {
				$parentRelationshipIds = array_keys($table->getParentRelationships());
				$this->defaultFollowedParentRelationshipIdSets[$tableId] = new Set($parentRelationshipIds);
			}
		}
		
		return $this->defaultFollowedParentRelationshipIdSets;
	}
	
	private function getRowPredicate(DataRow $dataRow) {
		$table = $dataRow->getTable()->getInfo();
		$tableId = $table->getId();
		$keyColumns = $table->getKeyColumns();
		
		if (!isset($this->rowPredicateCache[$tableId])) {
			$predicate = NULL;
			$expressionsParameter = [];
			foreach ($keyColumns as $keyColumnId => $keyColumn) {
				$expressionsParameter[$keyColumnId] = new ExpressionParameter();
				$expression = new ExpressionEquals($keyColumn, $expressionsParameter[$keyColumnId]);
				$predicate = $predicate != NULL ? new ExpressionAnd($predicate, $expression) : $expression;
			}
			$this->rowPredicateCache[$tableId] = [$predicate, $expressionsParameter];
		} else {
			list($predicate, $expressionsParameter) = $this->rowPredicateCache[$tableId];
		}
		
		foreach ($expressionsParameter as $keyColumnId => $expressionParameter) {
			$keyPart = $dataRow->getItem($keyColumnId, DataRowVersion::ORIGINAL);
			$expressionParameter->setValue($keyColumns[$keyColumnId]->toBackendValue($keyPart));
		}
		
		return $predicate;
	}
	
	private function getInsertCommand($tableId, array $data, array $returningColumns = []) {
		$this->validateData($data);
		
		$this->buildCommandParts($tableId, $data, $returningColumns,
				$schemaQualifiedTableName, $columnNames, $placeholders, $parameters, $returningColumnNames);
		
		$commandText = 'INSERT INTO ' . $schemaQualifiedTableName .
				' (' . $columnNames . ') VALUES (' . $placeholders . ')';
		
		if (StringUtils::length($returningColumnNames) > 0) {
			$commandText .= ' RETURNING ' . $returningColumnNames;
		}
		
		return $this->getCommand($commandText, new ArrayList($parameters));
	}
	
	private function getUpdateCommand($tableId, array $data, IBooleanExpression $condition = NULL,
			array $returningColumns = []) {
		$this->validateData($data);
		
		$this->buildCommandParts($tableId, $data, $returningColumns,
				$schemaQualifiedTableName, $columnNames, $placeholders, $parameters, $returningColumnNames);
		
		$commandText = 'UPDATE ' . $schemaQualifiedTableName .
				' SET (' . $columnNames . ') = (' . $placeholders . ')';
		
		if ($condition != NULL) {
			$numExistingPlaceholders = count($parameters);
			$commandText .= ' WHERE ' . $condition->getSqlNotation($numExistingPlaceholders);
			$parameters = array_merge($parameters, $condition->getParameters());
		}
		
		if (StringUtils::length($returningColumnNames) > 0) {
			$commandText .= ' RETURNING ' . $returningColumnNames;
		}
		
		return $this->getCommand($commandText, new ArrayList($parameters));
	}
	
	private function getDeleteCommand($tableId, IBooleanExpression $condition = NULL,
			array $returningColumns = []) {
		$data = [];
		$this->buildCommandParts($tableId, $data, $returningColumns,
				$schemaQualifiedTableName, $columnNames, $placeholders, $parameters, $returningColumnNames);
				
		$commandText = 'DELETE FROM ' . $schemaQualifiedTableName;
		
		if ($condition != NULL) {
			$numExistingPlaceholders = count($parameters);
			$commandText .= ' WHERE ' . $condition->getSqlNotation($numExistingPlaceholders);
			$parameters = array_merge($parameters, $condition->getParameters());
		}
		
		if (StringUtils::length($returningColumnNames) > 0) {
			$commandText .= ' RETURNING ' . $returningColumnNames;
		}
		
		return $this->getCommand($commandText, new ArrayList($parameters));
	}
	
	private function validateData(array $data) {
		if (count($data) == 0) {
			throw new ArgumentException('Data is empty.');
		}
	}
	
	private function getCommand($commandText, IList $parameters) {
		$commandTextHash = md5($commandText);
		if (!isset($this->commands[$commandTextHash])) {
			$command = $this->connection->createCommand();
			$command->setCommandText($commandText);
			$command->prepare();
			$this->commands[$commandTextHash] = $command;
		} else {
			$command = $this->commands[$commandTextHash];
		}

		$command->setParameters($parameters);
		return $command;
	}
	
	private function buildCommandParts($tableId, array $data, array $returningColumns,
			&$schemaQualifiedTableName, &$columnNames, &$placeholders, &$parameters, &$returningColumnNames) {
		$table = $this->databaseModel->getTables()[$tableId];
		$columns = $table->getColumns();
		
		$schemaQualifiedTableName = $table->getSchemaQualifiedName();
		$columnNames = $placeholders = $returningColumnNames = '';
		$parameters = [];
		
		if (count($data) > 0) {
			foreach ($data as $columnId => $value) {
				$columnNames .= $columns[$columnId]->getName() . ', ';
				$parameters[] = $columns[$columnId]->toBackendValue($value);
				$placeholders .= '$' . count($parameters) . ', ';
			}
			
			$columnNames = StringUtils::substring($columnNames, 0, -2);
			$placeholders = StringUtils::substring($placeholders, 0, -2);
		}
		
		if (count($returningColumns) > 0) {
			foreach ($returningColumns as $returningColumn) {
				$returningColumnNames .= $returningColumn->getName() . ', ';
			}
			
			$returningColumnNames = StringUtils::substring($returningColumnNames, 0, -2);
		}
	}

	private function buildParentRelationshipSetup($tableId, array $followedParentRelationshipIdSets = NULL) {
		if ($followedParentRelationshipIdSets === NULL) {
			$followedParentRelationshipIdSets = $this->getDefaultFollowedParentRelationshipIdSets();
		}
		
		$parentRelationshipSetup = [];
		
		$tables = $this->databaseModel->getTables();
		$tableStack = [[$tableId, true]];
		
		while (count($tableStack) > 0) {
			list($currentTableId, $rowsAreMandatory) = array_pop($tableStack);
			
			$parentRelationshipSetup[$currentTableId] = [];
			
			$columns = $tables[$currentTableId]->getColumns();
			
			$parentRelationships = $tables[$currentTableId]->getParentRelationships();
			foreach ($parentRelationships as $parentRelationshipId => $parentRelationship) {
				$referencedTableId = $parentRelationship->getReferencedTable()->getId();
				
				if (isset($parentRelationshipSetup[$referencedTableId])) {
					continue;
				}
				
				$followedParentRelationshipIdSet = $followedParentRelationshipIdSets[$currentTableId];
				if (!$followedParentRelationshipIdSet->contains($parentRelationshipId)) {
					continue;
				}
				
				$parentRowsAreMandatory = $rowsAreMandatory;
				$referencedColumnIds = $parentRelationship->getReferencedColumnIds();
				reset($referencedColumnIds);
				while ($parentRowsAreMandatory && ($columnId = key($referencedColumnIds)) !== NULL) {
					if ($columns[$columnId]->allowsNull()) {
						$parentRowsAreMandatory = false;
					} else {
						next($referencedColumnIds);
					}
				}
				
				$parentRelationshipSetup[$currentTableId][] = [$parentRelationship, $parentRowsAreMandatory];
				array_push($tableStack, [$referencedTableId, $parentRowsAreMandatory]);
			}
		}
		
		return $parentRelationshipSetup;
	}
	
	private function buildJoinCommandTextPart(array $parentRelationshipSetup) {
		$joinCommandTextPart = '';
		
		$tables = $this->databaseModel->getTables();
		
		foreach ($parentRelationshipSetup as $currentTableId => $parentRelationshipTuples) {
			$columns = $tables[$currentTableId]->getColumns();
			
			foreach ($parentRelationshipTuples as $parentRelationshipTuple) {
				list($parentRelationship, $parentRowsAreMandatory) = $parentRelationshipTuple;
				$referencedTable = $parentRelationship->getReferencedTable();
				$referencedColumns = $referencedTable->getColumns();
				
				$joinCondition = NULL;
				foreach ($parentRelationship->getReferencedColumnIds() as $columnId => $referencedColumnId) {
					$column = $columns[$columnId];
					$referencedColumn = $referencedColumns[$referencedColumnId];
					$expression = new ExpressionEquals($column, $referencedColumn);
					if ($joinCondition != NULL) {
						$joinCondition = new ExpressionAnd($joinCondition, $expression);
					} else {
						$joinCondition = $expression;
					}
				}
				
				if (!$parentRowsAreMandatory) {
					$joinCommandTextPart .= ' LEFT';
				}
				
				$joinCommandTextPart .= ' JOIN ' . $referencedTable->getSchemaQualifiedName() .
						' ON ' . $joinCondition->getSqlNotation();
			}
		}
		
		return $joinCommandTextPart;
	}
	
	private function getSortedTableColumns($tableId) {
		if (!isset($this->sortedTableColumns[$tableId])) {
			$columns = [];
			$columnList = new ArrayList($this->databaseModel->getTables()[$tableId]->getColumns());
			foreach ($columnList->orderByDescending(self::getTableColumnKeySelector()) as $column) {
				$columns[$column->getId()] = $column;
			}
			
			$this->sortedTableColumns[$tableId] = $columns;
		}
		
		return $this->sortedTableColumns[$tableId];
	}
	
	private static function getTableColumnKeySelector() {
		static $keySelector = NULL;
		if ($keySelector == NULL) {
			$keySelector = function(Column $column) { return $column->isKey(); };
		}
		
		return $keySelector;
	}

}

?>
