<?php

namespace VTCompose\Data\DatabaseModel;

use VTCompose\Time\Time;

/**
 * 
 *
 * 
 */
class TimeColumn extends IntegerColumn {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		$schemaQualifiedTableName = $this->getTable()->getSchemaQualifiedName();
		return 'extract(epoch FROM ' . $schemaQualifiedTableName . '.' . $this->getName() . ')';
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function toBackendValue($value) {
		return $value !== NULL ? Time::formatTimestamp($value, 'UTC', 'Y-m-d H:i:s') : NULL;
	}
	
}

?>
