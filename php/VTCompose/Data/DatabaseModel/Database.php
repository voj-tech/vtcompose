<?php

namespace VTCompose\Data\DatabaseModel;

/**
 * 
 *
 * 
 */
abstract class Database {

	private $schemas;
	private $tables;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getSchemas() {
		if (!isset($this->schemas)) {
			$this->schemas = [];
			
			foreach ($this->createSchemas() as $schema) {
				$this->schemas[$schema->getId()] = $schema;
			}
		}
		
		return $this->schemas;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getTables() {
		if (!isset($this->tables)) {
			$this->tables = [];
			
			foreach ($this->getSchemas() as $schema) {
				foreach ($schema->getTables() as $table) {
					$this->tables[$table->getId()] = $table;
				}
			}
			
			foreach ($this->createTables() as $table) {
				$this->tables[$table->getId()] = $table;
			}
		}
		
		return $this->tables;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createSchemas() {
		return [];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createTables() {
		return [];
	}

}

?>
