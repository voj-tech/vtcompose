<?php

namespace VTCompose\Data\DatabaseModel;

/**
 * 
 *
 * 
 */
abstract class Schema {

	private $database;
	private $id;
	private $qualifyObjectNames;
	
	private $tables;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Database 
	 */
	public function getDatabase() {
		return $this->database;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function objectNamesNeedQualification() {
		return $this->qualifyObjectNames;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getTables() {
		if (!isset($this->tables)) {
			$this->tables = [];
			
			foreach ($this->createTables() as $table) {
				$this->tables[$table->getId()] = $table;
			}
		}
		
		return $this->tables;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	abstract public function getName();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Database 
	 * @param mixed 
	 * @param bool 
	 */
	public function __construct(Database $database, $id, $qualifyObjectNames = true) {
		$this->database = $database;
		$this->id = $id;
		$this->qualifyObjectNames = $qualifyObjectNames;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	abstract protected function createTables();

}

?>
