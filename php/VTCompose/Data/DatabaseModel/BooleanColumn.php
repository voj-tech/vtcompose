<?php

namespace VTCompose\Data\DatabaseModel;

use VTCompose\Data\IBooleanExpression;

/**
 * 
 *
 * 
 */
class BooleanColumn extends Column implements IBooleanExpression {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	protected function toInternalValue($value) {
		return (bool) $value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function toFrontendValue($value) {
		return $value !== NULL ? $value == 't' : NULL;
	}
	
}

?>
