<?php

namespace VTCompose\Data\DatabaseModel;

/**
 * 
 *
 * 
 */
class FloatingPointColumn extends Column {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	protected function toInternalValue($value) {
		return (float) $value;
	}
	
}

?>
