<?php

namespace VTCompose\Data\DatabaseModel;

/**
 * 
 *
 * 
 */
class Relationship {
	
	private $referencingTable;
	private $id;
	private $referencedTable;
	private $referencedColumnIds;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Table 
	 */
	public function getReferencingTable() {
		return $this->referencingTable;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Table 
	 */
	public function getReferencedTable() {
		return $this->referencedTable;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getReferencedColumnIds() {
		return $this->referencedColumnIds;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Table 
	 * @param mixed 
	 * @param Table 
	 * @param array 
	 */
	public function __construct(Table $referencingTable, $id, Table $referencedTable,
			array $referencedColumnIds) {
		$this->referencingTable = $referencingTable;
		$this->id = $id;
		$this->referencedTable = $referencedTable;
		$this->referencedColumnIds = $referencedColumnIds;
	}
	
}

?>
