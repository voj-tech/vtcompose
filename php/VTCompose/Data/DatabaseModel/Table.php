<?php

namespace VTCompose\Data\DatabaseModel;

/**
 * 
 *
 * 
 */
abstract class Table {

	private $database;
	private $id;
	private $schema;
	
	private $columns;
	private $keyColumns;
	private $parentRelationships;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Database 
	 */
	public function getDatabase() {
		return $this->database;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Schema 
	 */
	public function getSchema() {
		return $this->schema;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getColumns() {
		if (!isset($this->columns)) {
			$this->columns = [];
			
			foreach ($this->createColumns() as $column) {
				$this->columns[$column->getId()] = $column;
			}
		}
		
		return $this->columns;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getKeyColumns() {
		if (!isset($this->keyColumns)) {
			$this->keyColumns = [];
			
			foreach ($this->getColumns() as $columnId => $column) {
				if ($column->isKey()) {
					$this->keyColumns[$columnId] = $column;
				}
			}
		}
		
		return $this->keyColumns;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParentRelationships() {
		if (!isset($this->parentRelationships)) {
			$this->parentRelationships = [];
			
			foreach ($this->createParentRelationships() as $relationship) {
				$this->parentRelationships[$relationship->getId()] = $relationship;
			}
		}
		
		return $this->parentRelationships;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	abstract public function getName();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getSchemaQualifiedName() {
		$name = $this->getName();
		if ($this->schema != NULL && $this->schema->objectNamesNeedQualification()) {
			$name = $this->schema->getName() . '.' . $name;
		}
		return $name;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Database 
	 * @param mixed 
	 * @param Schema 
	 */
	public function __construct(Database $database, $id, Schema $schema = NULL) {
		$this->database = $database;
		$this->id = $id;
		$this->schema = $schema;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	abstract protected function createColumns();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	abstract protected function createParentRelationships();

}

?>
