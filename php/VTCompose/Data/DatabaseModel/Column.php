<?php

namespace VTCompose\Data\DatabaseModel;

use VTCompose\Data\IExpression;
use VTCompose\Exception\ArgumentException;

/**
 * 
 *
 * 
 */
abstract class Column implements IExpression {

	private $table;
	private $id;
	private $name;
	private $allowNull;
	private $key;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Table 
	 */
	public function getTable() {
		return $this->table;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function allowsNull() {
		return $this->allowNull;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isKey() {
		return $this->key;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param Table 
	 * @param mixed 
	 * @param string 
	 * @param bool 
	 * @param bool 
	 */
	public function __construct(Table $table, $id, $name, $allowNull = true, $key = false) {
		$this->table = $table;
		$this->id = $id;
		$this->name = $name;
		$this->allowNull = $key ? false : $allowNull;
		$this->key = $key;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		return $this->table->getSchemaQualifiedName() . '.' . $this->name;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		return [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	abstract protected function toInternalValue($value);

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 * @throws ArgumentException .
	 */
	public function filterValue($value) {
		if ($value !== NULL) {
			return $this->toInternalValue($value);
		} elseif ($this->allowNull) {
			return NULL;
		} else {
			throw new ArgumentException('NULL value is not allowed.');
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function toFrontendValue($value) {
		return $value !== NULL ? $this->toInternalValue($value) : NULL;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function toBackendValue($value) {
		return $value;
	}

}

?>
