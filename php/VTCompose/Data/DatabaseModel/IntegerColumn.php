<?php

namespace VTCompose\Data\DatabaseModel;

/**
 * 
 *
 * 
 */
class IntegerColumn extends Column {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	protected function toInternalValue($value) {
		return (int) $value;
	}
	
}

?>
