<?php

namespace VTCompose\Data;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class OrderDirection extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const ASCENDING = 0;

	/**
	 * 
	 *
	 * 
	 */
	const DESCENDING = 1;

}

?>
