<?php

namespace VTCompose\Data;

/**
 * 
 *
 * 
 */
class ExpressionMatchesRegex implements IBooleanExpression {

	private $stringExpression;
	private $patternExpression;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IExpression 
	 * @param IExpression 
	 */
	public function __construct(IExpression $stringExpression, IExpression $patternExpression) {
		$this->stringExpression = $stringExpression;
		$this->patternExpression = $patternExpression;
		
		$this->sqlNotationCache = [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache[$numExistingPlaceholders])) {
			$originalNumPlaceholders = $numExistingPlaceholders;
			$sqlNotation = '(' . $this->stringExpression->getSqlNotation($numExistingPlaceholders) . ') ' .
					'~ (' . $this->patternExpression->getSqlNotation($numExistingPlaceholders) . ')';
			$this->sqlNotationCache[$originalNumPlaceholders] = [$sqlNotation, $numExistingPlaceholders];
		} else {
			list($sqlNotation, $numExistingPlaceholders) = $this->sqlNotationCache[$numExistingPlaceholders];
		}
		return $sqlNotation;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		$stringExpressionParameters = $this->stringExpression->getParameters();
		$patternExpressionParameters = $this->patternExpression->getParameters();
		return array_merge($stringExpressionParameters, $patternExpressionParameters);
	}

}

?>
