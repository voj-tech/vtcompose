<?php

namespace VTCompose\Data;

/**
 * 
 *
 * 
 */
class ExpressionIsNull implements IBooleanExpression {

	private $childExpression;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IExpression 
	 */
	public function __construct(IExpression $childExpression) {
		$this->childExpression = $childExpression;
		
		$this->sqlNotationCache = [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache[$numExistingPlaceholders])) {
			$originalNumPlaceholders = $numExistingPlaceholders;
			$childExpressionSqlNotation = $this->childExpression->getSqlNotation($numExistingPlaceholders);
			$sqlNotation = '(' . $childExpressionSqlNotation . ') IS NULL';
			$this->sqlNotationCache[$originalNumPlaceholders] = [$sqlNotation, $numExistingPlaceholders];
		} else {
			list($sqlNotation, $numExistingPlaceholders) = $this->sqlNotationCache[$numExistingPlaceholders];
		}
		return $sqlNotation;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		return $this->childExpression->getParameters();
	}

}

?>
