<?php

namespace VTCompose\Data\HashFunction;

/**
 * 
 *
 * 
 */
abstract class HashFunction implements IHashFunction {

	private $hashSize;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getHashSize() {
		return $this->hashSize;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	abstract protected function getAlgo();

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function __construct($hashSize) {
		$this->hashSize = $hashSize;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 */
	public function computeHash($data) {
		return hash($this->getAlgo(), $data);
	}

}

?>
