<?php

namespace VTCompose\Data\HashFunction;

use VTCompose\Exception\ArgumentOutOfRangeException;

/**
 * 
 *
 * 
 */
abstract class Fnv1HashFunction extends HashFunction {

	private static function hashSizeIsValid($hashSize) {
		return $hashSize == 32 || $hashSize == 64;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @throws ArgumentOutOfRangeException .
	 */
	public function __construct($hashSize = 64) {
		if (!self::hashSizeIsValid($hashSize)) {
			throw new ArgumentOutOfRangeException('Hash size is invalid.');
		}
		
		parent::__construct($hashSize);
	}

}

?>
