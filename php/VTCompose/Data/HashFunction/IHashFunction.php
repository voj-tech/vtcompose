<?php

namespace VTCompose\Data\HashFunction;

/**
 * 
 *
 * 
 */
interface IHashFunction {

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getHashSize();

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 */
	public function computeHash($data);
	
}

?>
