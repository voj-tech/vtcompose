<?php

namespace VTCompose\Data;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class DataRowVersion extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const ORIGINAL = 0x0100;
	
	/**
	 * 
	 *
	 * 
	 */
	const CURRENT = 0x0200;
	
	/**
	 * 
	 *
	 * 
	 */
	const PROPOSED = 0x0400;
	
	/**
	 * 
	 *
	 * 
	 */
	const DATA_ROW_VERSION_DEFAULT = self::CURRENT | self::PROPOSED;

}

?>
