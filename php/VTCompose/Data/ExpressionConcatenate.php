<?php

namespace VTCompose\Data;

/**
 * 
 *
 * 
 */
class ExpressionConcatenate implements IExpression {

	private $leftExpression;
	private $rightExpression;
	
	private $sqlNotationCache;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IExpression 
	 * @param IExpression 
	 */
	public function __construct(IExpression $leftExpression, IExpression $rightExpression) {
		$this->leftExpression = $leftExpression;
		$this->rightExpression = $rightExpression;
		
		$this->sqlNotationCache = [];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		if (!isset($this->sqlNotationCache[$numExistingPlaceholders])) {
			$originalNumPlaceholders = $numExistingPlaceholders;
			$leftExpressionSqlNotation = $this->leftExpression->getSqlNotation($numExistingPlaceholders);
			$rightExpressionSqlNotation = $this->rightExpression->getSqlNotation($numExistingPlaceholders);
			$sqlNotation = '(' . $leftExpressionSqlNotation . ') || (' . $rightExpressionSqlNotation . ')';
			$this->sqlNotationCache[$originalNumPlaceholders] = [$sqlNotation, $numExistingPlaceholders];
		} else {
			list($sqlNotation, $numExistingPlaceholders) = $this->sqlNotationCache[$numExistingPlaceholders];
		}
		return $sqlNotation;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		return array_merge($this->leftExpression->getParameters(), $this->rightExpression->getParameters());
	}

}

?>
