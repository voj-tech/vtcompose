<?php

namespace VTCompose\Data;

use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class DataSet {

	private $tables;
	
	private function validateRowStates($rowStates) {
		if (($rowStates & ~(DataRowState::ADDED | DataRowState::DELETED | DataRowState::MODIFIED)) != 0) {
			$message = 'Row states specify a bit other than bits for row states Added, Deleted and Modified.';
			throw new ArgumentException($message);
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return DataTableCollection 
	 */
	public function getTables() {
		return $this->tables;
	}

	/**
	 * 
	 *
	 * 
	 */
	public function __construct() {
		$this->tables = new DataTableCollection($this);
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return bool 
	 */
	public function hasChanges($rowStates = DataRowState::ADDED | DataRowState::DELETED |
			DataRowState::MODIFIED) {
		$this->validateRowStates($rowStates);
		
		foreach ($this->tables as $table) {
			foreach ($table->getRows() as $row) {
				if (($row->getRowState() & $rowStates) != 0) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return DataSet 
	 */
	public function getChanges($rowStates = DataRowState::ADDED | DataRowState::DELETED |
			DataRowState::MODIFIED) {
		$this->validateRowStates($rowStates);
		
		$tables = [];
		foreach ($this->tables as $table) {
			$changes = $table->getChanges($rowStates);
			if ($changes != NULL) {
				$tables[] = $changes;
			}
		}
		
		if (count($tables) == 0) {
			return NULL;
		}
		
		$dataSet = new self();
		$dataSetTables = $dataSet->getTables();
		foreach ($tables as $table) {
			$dataSetTables->add($table);
		}
		
		return $dataSet;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param array 
	 */
	public function acceptChanges(array $columnIdSets = NULL) {
		foreach ($this->tables as $tableId => $table) {
			$table->acceptChanges($columnIdSets !== NULL ? $columnIdSets[$tableId] : NULL);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param array 
	 */
	public function rejectChanges(array $columnIdSets = NULL) {
		foreach ($this->tables as $tableId => $table) {
			$table->rejectChanges($columnIdSets !== NULL ? $columnIdSets[$tableId] : NULL);
		}
	}

}

?>
