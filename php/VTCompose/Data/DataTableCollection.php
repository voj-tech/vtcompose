<?php

namespace VTCompose\Data;

use Closure;
use Traversable;
use VTCompose\Collection\Dictionary;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\IDictionary;
use VTCompose\Collection\KeyValuePair;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class DataTableCollection implements IDictionary {

	use Enumerable;
	
	private $tables;
	private $dataSet;
	
	private static function getTableAccessors() {
		static $tableAccessors = NULL;
		if ($tableAccessors == NULL) {
			$tableAccessors = new Dictionary();
		}
		
		return $tableAccessors;
	}
	
	private static function tableIsRegistered(DataTable $table) {
		return self::getTableAccessors()->containsKey($table);
	}
	
	private static function validateTableAsRegistered(DataTable $table) {
		if (!self::tableIsRegistered($table)) {
			throw new InvalidOperationException('The DataTable is unregistered.');
		}
	}
	
	private static function validateTableAsUnregistered(DataTable $table) {
		if (self::tableIsRegistered($table)) {
			throw new InvalidOperationException('The DataTable is registered.');
		}
	}
	
	private static function registerTableInternal(DataTable $table, Closure $tableAccessor) {
		self::getTableAccessors()->addAt($table, $tableAccessor);
	}
	
	private static function unregisterTableInternal(DataTable $table) {
		self::getTableAccessors()->removeAt($table);
	}
	
	private function valueIsTable($value) {
		return $value instanceof DataTable;
	}
	
	private function keyMatchesTableId($key, DataTable $table) {
		return $key === $table->getInfo()->getId();
	}
	
	private function validateValue($value) {
		if ($this->valueIsTable($value)) {
			return;
		}
		
		if (!$value instanceof KeyValuePair) {
			throw new ArgumentException('Value is not an instance of KeyValuePair or DataTable.');
		}
		
		$keyValuePairValue = $value->getValue();
		if (!$keyValuePairValue instanceof DataTable) {
			throw new ArgumentException('KeyValuePair value is not an instance of DataTable.');
		}
		
		if (!$this->keyMatchesTableId($value->getKey(), $keyValuePairValue)) {
			throw new ArgumentException('KeyValuePair key does not match the table id.');
		}
	}
	
	private function validateValueAsTable($value) {
		if (!$this->valueIsTable($value)) {
			throw new ArgumentException('Value is not an instance of DataTable.');
		}
	}
	
	private function validateTableForAddition(DataTable $table) {
		if ($table->getDataSet() != NULL) {
			throw new ArgumentException('The DataTable already belongs to a DataSet.');
		}
		
		if ($this->tables->containsKey($table->getInfo()->getId())) {
			throw new DuplicateIdException('Table id is already present in the collection.');
		}
	}
	
	private function addKeyValuePair(KeyValuePair $keyValuePair) {
		$table = $keyValuePair->getValue();
		self::validateTableAsRegistered($table);
	
		$this->tables->add($keyValuePair);
		
		$tableAccessors = self::getTableAccessors();
		$tableAccessors[$table]('setDataSet', $this->dataSet);
	}
	
	private function addTable(DataTable $table) {
		$this->addKeyValuePair(new KeyValuePair($table->getInfo()->getId(), $table));
	}
	
	private function removeTable(DataTable $table) {
		$this->tables->removeAt($table->getInfo()->getId());
		
		$tableAccessors = self::getTableAccessors();
		$tableAccessors[$table]('setDataSet', NULL);
	}
	
	private function removeKeyValuePair(KeyValuePair $keyValuePair) {
		$this->removeTable($keyValuePair->getValue());
	}
	
	private function removeByTableId($tableId) {
		$this->removeTable($this->tables[$tableId]);
	}

	/**
	 * @internal
	 */
	public function __construct(DataSet $dataSet) {
		if ($dataSet->getTables() != NULL) {
			throw new InvalidOperationException('The DataSet already has a DataTableCollection.');
		}
		
		$this->tables = new Dictionary();
		$this->dataSet = $dataSet;
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return ICollection 
	 */
	public function getKeys() {
		return $this->tables->getKeys();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return ICollection 
	 */
	public function getValues() {
		return $this->tables->getValues();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->validateValue($value);
		
		$valueIsTable = $this->valueIsTable($value);
		
		$this->validateTableForAddition($valueIsTable ? $value : $value->getValue());
		
		if ($valueIsTable) {
			$this->addTable($value);
			return;
		}
		
		$this->addKeyValuePair($value);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	public function addAt($key, $value) {
		$this->validateValueAsTable($value);
		
		if (!$this->keyMatchesTableId($key, $value)) {
			throw new ArgumentException('Key does not match the table id.');
		}
		
		$this->validateTableForAddition($value);
		
		$this->addTable($value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		foreach ($this->tables->toArray() as $keyValuePair) {
			$this->removeKeyValuePair($keyValuePair);
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		$this->validateValue($value);
		
		if ($this->valueIsTable($value)) {
			$dataSet = $value->getDataSet();
			return $dataSet != NULL && $dataSet->getTables() === $this;
		}
		
		return $this->tables->contains($value);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function containsKey($key) {
		return $this->tables->containsKey($key);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		if (!$this->contains($value)) {
			return false;
		}
		
		if ($this->valueIsTable($value)) {
			$this->removeTable($value);
			return true;
		}
		
		$this->removeKeyValuePair($value);
		
		return true;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function removeAt($key) {
		if (!$this->containsKey($key)) {
			return false;
		}

		$this->removeByTableId($key);

		return true;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->tables->toArray();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		return $this->tables->getIterator();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function offsetExists($offset): bool {
		return $this->containsKey($offset);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function offsetGet($offset): mixed {
		return $this->tables[$offset];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function offsetSet($offset, $value): void {
		if ($offset === NULL) {
			$this->add($value);
			return;
		}
		
		$this->addAt($offset, $value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function offsetUnset($offset): void {
		$this->removeByTableId($offset);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->tables->count();
	}
	
	/**
	 * @internal
	 */
	public static function registerTable(DataTable $table, Closure $tableAccessor) {
		self::validateTableAsUnregistered($table);
		
		self::registerTableInternal($table, $tableAccessor);
	}
	
	/**
	 * @internal
	 */
	public static function unregisterTable(DataTable $table) {
		self::validateTableAsRegistered($table);
		
		$dataSet = $table->getDataSet();
		if ($dataSet != NULL) {
			$dataSet->getTables()->removeTable($table);
		}
		
		self::unregisterTableInternal($table);
	}
	
	/**
	 * 
	 *
	 * Calling this method will effectively allow to delete the DataTable objects from memory.
	 *
	 * @throws InvalidOperationException .
	 */
	public function unregisterTables() {
		foreach ($this->tables->toArray() as $keyValuePair) {
			$table = $keyValuePair->getValue();
			$this->removeTable($table);
			self::unregisterTableInternal($table);
		}
	}

}

?>
