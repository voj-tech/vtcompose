<?php

namespace VTCompose\Data;

use Closure;
use VTCompose\Collection\Dictionary;
use VTCompose\Collection\ISet;
use VTCompose\Data\DatabaseModel\Table;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class DataTable {

	private $info;
	private $rows;
	private $dataSet;
	
	private $accessor;
	private $rowAccessors;
	
	private function importRowInternal(DataRow $row) {
		$this->rowAccessors[new DataRow($this)]('copy', $row);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Table 
	 */
	public function getInfo() {
		return $this->info;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return DataRowCollection 
	 */
	public function getRows() {
		return $this->rows;
	}
	
	private function setDataSet(DataSet $dataSet = NULL) {
		$this->dataSet = $dataSet;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return DataSet 
	 */
	public function getDataSet() {
		return $this->dataSet;
	}
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param Table 
	 */
	public function __construct(Table $info) {
		DataTableCollection::registerTable($this, $this->getAccessor());
	
		$this->info = $info;
		$this->rows = new DataRowCollection($this);
		
		$this->rowAccessors = new Dictionary();
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return DataRow 
	 */
	public function newRow() {
		return new DataRow($this);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param DataRow 
	 * @throws ArgumentException .
	 */
	public function importRow(DataRow $row) {
		if ($row->getTable()->info !== $this->info) {
			throw new ArgumentException('The row does not belong to this table.');
		}
		
		if ($row->getRowState() == DataRowState::DETACHED) {
			return;
		}
		
		$this->importRowInternal($row);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return DataTable 
	 * @throws ArgumentException .
	 */
	public function getChanges($rowStates = DataRowState::ADDED | DataRowState::DELETED |
			DataRowState::MODIFIED) {
		if (($rowStates & ~(DataRowState::ADDED | DataRowState::DELETED | DataRowState::MODIFIED)) != 0) {
			$message = 'Row states specify a bit other than bits for row states Added, Deleted and Modified.';
			throw new ArgumentException($message);
		}
		
		$rows = [];
		foreach ($this->rows as $row) {
			if (($row->getRowState() & $rowStates) != 0) {
				$rows[] = $row;
			}
		}
		
		if (count($rows) == 0) {
			return NULL;
		}
		
		$table = new self($this->info);
		foreach ($rows as $row) {
			$table->importRowInternal($row);
		}
		
		return $table;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param ISet 
	 */
	public function acceptChanges(ISet $columnIdSet = NULL) {
		foreach ($this->rows->toArray() as $row) {
			$row->acceptChanges($columnIdSet);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param ISet 
	 */
	public function rejectChanges(ISet $columnIdSet = NULL) {
		foreach ($this->rows->toArray() as $row) {
			$row->rejectChanges($columnIdSet);
		}
	}
	
	/**
	 * @internal
	 */
	public function establishFriendshipWithRow(DataRow $row, Closure $rowAccessor) {
		if ($row->getTable() != NULL) {
			throw new InvalidOperationException('The DataRow is already friend with a DataTable.');
		}
		
		$this->rowAccessors->addAt($row, $rowAccessor);
		$this->rows->establishFriendshipWithRow($row, $rowAccessor);
	}
	
	/**
	 * 
	 *
	 * Calling this method will effectively allow to delete the object from memory.
	 */
	public function unregister() {
		DataTableCollection::unregisterTable($this);
	}

}

?>
