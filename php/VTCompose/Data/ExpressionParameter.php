<?php

namespace VTCompose\Data;

/**
 * 
 *
 * 
 */
class ExpressionParameter implements IExpression {

	private $value;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function setValue($value) {
		$this->value = $value;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function __construct($value = NULL) {
		$this->value = $value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return string 
	 */
	public function getSqlNotation(&$numExistingPlaceholders = 0) {
		return '$' . ++$numExistingPlaceholders;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function getParameters() {
		return [$this->value];
	}

}

?>
