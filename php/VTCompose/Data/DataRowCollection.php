<?php

namespace VTCompose\Data;

use Closure;
use Traversable;
use VTCompose\Collection\ArrayList;
use VTCompose\Collection\Dictionary;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\IList;
use VTCompose\Collection\Set;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotSupportedException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
class DataRowCollection implements IList {

	use Enumerable;

	private static $encodedKeyPartSeparator = ',';

	private $rows;
	private $indexes;
	private $table;

	private $accessor;
	private $rowAccessors;
	
	private static function getRowKey(DataRow $row, $version = DataRowVersion::DATA_ROW_VERSION_DEFAULT) {
		$key = [];
		foreach ($row->getTable()->getInfo()->getKeyColumns() as $keyColumnId => $keyColumn) {
			if (!$row->itemExists($keyColumnId, $version)) {
				return [];
			}
			
			$key[$keyColumnId] = $row->getItem($keyColumnId, $version);
		}
		
		return $key;
	}
	
	private function encodeKey($key) {
		$keyColumnIds = array_keys($this->table->getInfo()->getKeyColumns());
		
		if (!is_array($key)) {
			if (count($keyColumnIds) > 1) {
				throw new ArgumentException('The table has more than one key column.');
			}
			
			$key = [reset($keyColumnIds) => $key];
		} elseif (count($key) > count($keyColumnIds)) {
			$message = 'The table has less key columns than the number of specified key parts.';
			throw new ArgumentException($message);
		}
		
		$encodedKey = '';
		foreach ($keyColumnIds as $keyColumnId) {
			if (!isset($key[$keyColumnId])) {
				throw new ArgumentException('Key is missing a key part.');
			}
			
			$encodedKeyPart = addcslashes($key[$keyColumnId], self::$encodedKeyPartSeparator . '\\');
			$encodedKey .= $encodedKeyPart . self::$encodedKeyPartSeparator;
		}
		
		return StringUtils::substring($encodedKey, 0, -1);
	}
	
	private function getEncodedRowKey(DataRow $row, $version = DataRowVersion::DATA_ROW_VERSION_DEFAULT) {
		$key = self::getRowKey($row, $version);
		return count($key) > 0 ? $this->encodeKey($key) : NULL;
	}
	
	private function validateValue($value) {
		if (!$value instanceof DataRow) {
			throw new ArgumentException('Value is not an instance of DataRow.');
		}
		
		if ($value->getTable()->getRows() !== $this) {
			throw new ArgumentException('The DataRow does not belong to this DataTable.');
		}
	}
	
	private function addToIndex($version, $encodedKey, DataRow $row) {
		$index = $this->indexes[$version];
		if ($index->containsKey($encodedKey)) {
			$index[$encodedKey]->add($row);
		} else {
			$index->addAt($encodedKey, new Set([$row]));
		}
	}
	
	private function removeFromIndex($version, $encodedKey, DataRow $row) {
		$this->indexes[$version][$encodedKey]->remove($row);
	}
	
	private function getFromIndex($version, $encodedKey) {
		$index = $this->indexes[$version];
		return $index->containsKey($encodedKey) ? $index[$encodedKey]->firstOrNull() : NULL;
	}
	
	private function addToIndexes(DataRow $row) {
		foreach ($this->indexes as $version => $index) {
			if (!$row->hasVersion($version)) {
				continue;
			}
			
			$encodedKey = $this->getEncodedRowKey($row, $version);
			if ($encodedKey === NULL) {
				continue;
			}
			
			$this->addToIndex($version, $encodedKey, $row);
		}
	}
	
	private function removeFromIndexes(DataRow $row) {
		foreach ($this->indexes as $version => $index) {
			if (!$row->hasVersion($version)) {
				continue;
			}
			
			$encodedKey = $this->getEncodedRowKey($row, $version);
			if ($encodedKey === NULL) {
				continue;
			}
			
			$this->removeFromIndex($version, $encodedKey, $row);
		}
	}
	
	private function updateIndexes(DataRow $row) {
		$encodedKey = $this->getEncodedRowKey($row);
		if ($encodedKey === NULL) {
			return;
		}
		
		foreach ($this->indexes as $version => $index) {
			if ($row->hasVersion($version)) {
				$this->addToIndex($version, $encodedKey, $row);
			} else {
				$this->removeFromIndex($version, $encodedKey, $row);
			}
		}
	}
	
	private function insertInternal($index, DataRow $row) {
		$this->rows->insert($index, $row);
		$this->addToIndexes($row);
	}
	
	private function removeInternal(DataRow $row) {
		$this->rows->remove($row);
		$this->removeFromIndexes($row);
	}
	
	private function insertRow($index, DataRow $row) {
		$rowAccessor = $this->rowAccessors[$row];
		switch ($row->getRowState()) {
		case DataRowState::DETACHED:
			$rowAccessor('setRowState', DataRowState::ADDED);
			$this->insertInternal($index, $row);
			return;
		
		case DataRowState::UNCHANGED:
		case DataRowState::ADDED:
		case DataRowState::DELETED:
		case DataRowState::MODIFIED:
			throw new ArgumentException('The DataRow is already attached.');
		
		default:
			$rowAccessor('throwRowStateInvalidException');
		}
	}
	
	private function removeRow(DataRow $row) {
		$rowAccessor = $this->rowAccessors[$row];
		switch ($row->getRowState()) {
		case DataRowState::DETACHED:
			return false;
		
		case DataRowState::UNCHANGED:
		case DataRowState::MODIFIED:
			$rowAccessor('clearModifiedColumnIdsAndSetDeleted');
		
		case DataRowState::ADDED:
		case DataRowState::DELETED:
			$rowAccessor('removeFromTableAndSetDetached');
			return true;
		
		default:
			$rowAccessor('throwRowStateInvalidException');
		}
	}
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}

	/**
	 * @internal
	 */
	public function __construct(DataTable $table) {
		if ($table->getRows() != NULL) {
			throw new InvalidOperationException('The DataTable already has a DataRowCollection.');
		}
	
		$this->rows = new ArrayList();
		$this->indexes = [
			DataRowVersion::ORIGINAL	=> new Dictionary(),
			DataRowVersion::CURRENT		=> new Dictionary(),
		];
		$this->table = $table;
		
		$this->rowAccessors = new Dictionary();
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->validateValue($value);
		$this->insertRow($this->rows->count(), $value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		foreach ($this->rows->toArray() as $row) {
			$this->removeRow($row);
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		$this->validateValue($value);
		return $this->rows->contains($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return int 
	 */
	public function indexOf($value) {
		$this->validateValue($value);
		return $this->rows->indexOf($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param mixed 
	 */
	public function insert($index, $value) {
		$this->validateValue($value);
		$this->insertRow($index, $value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		$this->validateValue($value);
		return $this->removeRow($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function removeAt($index) {
		$this->removeRow($this->rows[$index]);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->rows->toArray();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		return $this->rows->getIterator();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function offsetExists($offset): bool {
		return isset($this->rows[$offset]);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function offsetGet($offset): mixed {
		return $this->rows[$offset];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 * @throws NotSupportedException .
	 */
	public function offsetSet($offset, $value): void {
		if ($offset === NULL) {
			$this->add($value);
			return;
		}
	
		throw new NotSupportedException('Assigning a row to an offset is not a valid operation.');
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws NotSupportedException .
	 */
	public function offsetUnset($offset): void {
		$row = $this->rows[$offset];
		
		if ($offset == $this->rows->count() - 1) {
			$this->removeRow($row);
			return;
		}
		
		throw new NotSupportedException('Unset operation is valid only on the last row.');
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->rows->count();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param int 
	 * @return DataRow 
	 * @throws ArgumentException .
	 */
	public function find($key, $version = DataRowVersion::CURRENT) {
		if ($version != DataRowVersion::ORIGINAL && $version != DataRowVersion::CURRENT) {
			throw new ArgumentException('Version is not Original or Current.');
		}
		
		return $this->getFromIndex($version, $this->encodeKey($key));
	}
	
	/**
	 * @internal
	 */
	public function establishFriendshipWithRow(DataRow $row, Closure $rowAccessor) {
		if ($row->getTable() != NULL) {
			throw new InvalidOperationException('The DataRow is already friend with a DataTable.');
		}
		
		$this->rowAccessors->addAt($row, $rowAccessor);
		$rowAccessor('setRowCollectionAccessor', $this->getAccessor());
	}

}

?>
