<?php

namespace VTCompose\Data;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class DataRowState extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const DETACHED = 0x01;

	/**
	 * 
	 *
	 * 
	 */
	const UNCHANGED = 0x02;

	/**
	 * 
	 *
	 * 
	 */
	const ADDED = 0x04;

	/**
	 * 
	 *
	 * 
	 */
	const DELETED = 0x08;

	/**
	 * 
	 *
	 * 
	 */
	const MODIFIED = 0x10;

}

?>
