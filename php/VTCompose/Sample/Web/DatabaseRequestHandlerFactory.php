<?php

namespace VTCompose\Sample\Web;

use VTCompose\Collection\Set;
use VTCompose\Data\DataAdapter;
use VTCompose\Data\ExpressionAnd;
use VTCompose\Data\ExpressionConcatenate;
use VTCompose\Data\ExpressionEquals;
use VTCompose\Data\ExpressionIsNull;
use VTCompose\Data\ExpressionLike;
use VTCompose\Data\ExpressionLiteral;
use VTCompose\Data\ExpressionMatchesRegex;
use VTCompose\Data\ExpressionOr;
use VTCompose\Data\ExpressionParameter;
use VTCompose\Data\Order;
use VTCompose\Data\OrderDirection;
use VTCompose\Data\OrderNulls;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\Exception;
use VTCompose\Sample\DatabaseModel\TableId;
use VTCompose\Sample\DatabaseModel\WebRequestHandlersColumnId as ColumnId;
use VTCompose\StringUtilities as StringUtils;
use VTCompose\Web\IRequestHandler;
use VTCompose\Web\IRequestHandlerFactory;

/**
 * 
 *
 * 
 */
class DatabaseRequestHandlerFactory implements IRequestHandlerFactory {

	private static $tableId = TableId::WEB_REQUEST_HANDLERS;

	private $dataAdapter;
	private $namespacePrefix;
	
	private $columnIdSets;
	private $pathExpression;
	private $httpMethodExpression;
	private $hostExpression;
	private $condition;
	private $order;
	
	private function getColumnIdSets() {
		if (!isset($this->columnIdSets)) {
			$this->columnIdSets = [self::$tableId => new Set([ColumnId::ID, ColumnId::CLASS_NAME])];
		}
		
		return $this->columnIdSets;
	}
	
	private function getColumns() {
		return $this->dataAdapter->getDatabaseModel()->getTables()[self::$tableId]->getColumns();
	}
	
	private function getPathExpression() {
		if (!isset($this->pathExpression)) {
			$this->pathExpression = new ExpressionParameter();
		}
		
		return $this->pathExpression;
	}
	
	private function getHttpMethodExpression() {
		if (!isset($this->httpMethodExpression)) {
			$this->httpMethodExpression = new ExpressionParameter();
		}
		
		return $this->httpMethodExpression;
	}
	
	private function getHostExpression() {
		if (!isset($this->hostExpression)) {
			$this->hostExpression = new ExpressionParameter();
		}
		
		return $this->hostExpression;
	}
	
	private function getCondition() {
		if (!isset($this->condition)) {
			$columns = $this->getColumns();
			
			$column = $columns[ColumnId::PATH_REGEX];
			$pathMatchesRegexExpression = new ExpressionMatchesRegex($this->getPathExpression(), $column);
			
			$column = $columns[ColumnId::HTTP_METHOD];
			$methodIsNullExpression = new ExpressionIsNull($column);
			$methodEqualsExpression = new ExpressionEquals($column, $this->getHttpMethodExpression());
			
			$column = $columns[ColumnId::HOST];
			$hostIsNullExpression = new ExpressionIsNull($column);
			$concatenateExpression = new ExpressionConcatenate(new ExpressionLiteral('%'), $column);
			$hostLikeExpression = new ExpressionLike($this->getHostExpression(), $concatenateExpression);
			
			$methodEqualsExpression = new ExpressionOr($methodIsNullExpression, $methodEqualsExpression);
			$hostLikeExpression = new ExpressionOr($hostIsNullExpression, $hostLikeExpression);
			
			$httpMethodAndHostExpression = new ExpressionAnd($methodEqualsExpression, $hostLikeExpression);
			$this->condition = new ExpressionAnd($pathMatchesRegexExpression, $httpMethodAndHostExpression);
		}
	
		return $this->condition;
	}
	
	private function getOrder() {
		if (!isset($this->order)) {
			$columns = $this->getColumns();
			$this->order = [
				new Order($columns[ColumnId::HTTP_METHOD], OrderDirection::ASCENDING, OrderNulls::LAST),
				new Order($columns[ColumnId::HOST], OrderDirection::ASCENDING, OrderNulls::LAST),
				new Order($columns[ColumnId::RANK], OrderDirection::ASCENDING, OrderNulls::LAST),
			];
		}
	
		return $this->order;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setNamespacePrefix($namespacePrefix) {
		$this->namespacePrefix = $namespacePrefix;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespacePrefix() {
		return $this->namespacePrefix;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param DataAdapter 
	 * @param string 
	 */
	public function __construct(DataAdapter $dataAdapter, $namespacePrefix = '') {
		$this->dataAdapter = $dataAdapter;
		$this->namespacePrefix = $namespacePrefix;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param string 
	 * @return IRequestHandler 
	 * @throws ArgumentException .
	 * @throws Exception .
	 */
	public function createHandler($path, $httpMethod, $host) {
		$this->getPathExpression()->setValue($path);
		$this->getHttpMethodExpression()->setValue($httpMethod);
		$this->getHostExpression()->setValue($host);
		
		$followedParentRelationshipIdSets = NULL;
		$limit = 1;
		$dataSet = $this->dataAdapter->load(self::$tableId, $this->getCondition(),
				$followedParentRelationshipIdSets, $this->getColumnIdSets(), $this->getOrder(), $limit);
		
		$dataTables = $dataSet->getTables();
		$dataTable = $dataTables[self::$tableId];
		$dataTables->unregisterTables();
		
		$dataRows = $dataTable->getRows();
		if ($dataRows->count() == 0) {
			$message = 'Cannot find any handler matching specified path, HTTP method and host.';
			throw new ArgumentException($message);
		}

		$className = $dataRows->single()[ColumnId::CLASS_NAME];
		if (StringUtils::length($this->namespacePrefix) > 0) {
			$className = $this->namespacePrefix . '\\' . $className;
		}
		
		$requestHandler = new $className();
		if (!$requestHandler instanceof IRequestHandler) {
			$message = 'Class name is not a name of a class that implements the IRequestHandler interface.';
			throw new Exception($message);
		}
		
		return $requestHandler;
	}

}

?>
