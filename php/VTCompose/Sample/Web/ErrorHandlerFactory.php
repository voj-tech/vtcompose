<?php

namespace VTCompose\Sample\Web;

use VTCompose\Exception\Exception;
use VTCompose\Web\IErrorHandlerFactory;
use VTCompose\Web\IRequestHandler;

/**
 * 
 *
 * 
 */
class ErrorHandlerFactory implements IErrorHandlerFactory {

	private $errorHandlerClassName;

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function __construct($errorHandlerClassName) {
		$this->errorHandlerClassName = $errorHandlerClassName;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IRequestHandler 
	 * @throws Exception .
	 */
	public function createErrorHandler() {
		$className = $this->errorHandlerClassName;
		$requestHandler = new $className();
		if (!$requestHandler instanceof IRequestHandler) {
			$message = 'Class name is not a name of a class that implements the IRequestHandler interface.';
			throw new Exception($message);
		}
		
		return $requestHandler;
	}

}

?>
