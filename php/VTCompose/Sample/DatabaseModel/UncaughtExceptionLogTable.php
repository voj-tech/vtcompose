<?php

namespace VTCompose\Sample\DatabaseModel;

use VTCompose\Data\DatabaseModel\IntegerColumn;
use VTCompose\Data\DatabaseModel\StringColumn;
use VTCompose\Data\DatabaseModel\Table;

/**
 * 
 *
 * 
 */
class UncaughtExceptionLogTable extends Table {

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return 'uncaught_exception_log';
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createColumns() {
		return [
			new IntegerColumn($this, UncaughtExceptionLogColumnId::ID, 'id', false, true),
			new StringColumn($this, UncaughtExceptionLogColumnId::CLASS_NAME, 'class_name', false),
			new StringColumn($this, UncaughtExceptionLogColumnId::MESSAGE, 'message', false),
			new StringColumn($this, UncaughtExceptionLogColumnId::CODE, 'code', false),
			new StringColumn($this, UncaughtExceptionLogColumnId::FILENAME, 'filename', false),
			new IntegerColumn($this, UncaughtExceptionLogColumnId::LINE_NUMBER, 'line_number', false),
			new StringColumn($this, UncaughtExceptionLogColumnId::STACK_TRACE, 'stack_trace', false),
			new IntegerColumn($this, UncaughtExceptionLogColumnId::INNER_EXCEPTION_ID, 'inner_exception_id'),
			new StringColumn($this, UncaughtExceptionLogColumnId::TIME, 'time', false),
		];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createParentRelationships() {
		return [];
	}

}

?>
