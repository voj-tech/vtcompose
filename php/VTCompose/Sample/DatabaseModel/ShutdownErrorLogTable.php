<?php

namespace VTCompose\Sample\DatabaseModel;

use VTCompose\Data\DatabaseModel\IntegerColumn;
use VTCompose\Data\DatabaseModel\StringColumn;
use VTCompose\Data\DatabaseModel\Table;

/**
 * 
 *
 * 
 */
class ShutdownErrorLogTable extends Table {

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return 'shutdown_error_log';
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createColumns() {
		return [
			new IntegerColumn($this, ShutdownErrorLogColumnId::ID, 'id', false, true),
			new StringColumn($this, ShutdownErrorLogColumnId::MESSAGE, 'message', false),
			new IntegerColumn($this, ShutdownErrorLogColumnId::SEVERITY, 'severity', false),
			new StringColumn($this, ShutdownErrorLogColumnId::FILENAME, 'filename', false),
			new IntegerColumn($this, ShutdownErrorLogColumnId::LINE_NUMBER, 'line_number', false),
			new StringColumn($this, ShutdownErrorLogColumnId::TIME, 'time', false),
		];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createParentRelationships() {
		return [];
	}

}

?>
