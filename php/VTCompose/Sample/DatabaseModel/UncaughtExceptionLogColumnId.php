<?php

namespace VTCompose\Sample\DatabaseModel;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class UncaughtExceptionLogColumnId extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const ID = 0;
	
	/**
	 * 
	 *
	 * 
	 */
	const CLASS_NAME = 1;
	
	/**
	 * 
	 *
	 * 
	 */
	const MESSAGE = 2;
	
	/**
	 * 
	 *
	 * 
	 */
	const CODE = 3;
	
	/**
	 * 
	 *
	 * 
	 */
	const FILENAME = 4;
	
	/**
	 * 
	 *
	 * 
	 */
	const LINE_NUMBER = 5;
	
	/**
	 * 
	 *
	 * 
	 */
	const STACK_TRACE = 6;
	
	/**
	 * 
	 *
	 * 
	 */
	const INNER_EXCEPTION_ID = 7;
	
	/**
	 * 
	 *
	 * 
	 */
	const TIME = 8;

}

?>
