<?php

namespace VTCompose\Sample\DatabaseModel;

use VTCompose\Data\DatabaseModel\IntegerColumn;
use VTCompose\Data\DatabaseModel\StringColumn;
use VTCompose\Data\DatabaseModel\Table;

/**
 * 
 *
 * 
 */
class WebRequestHandlersTable extends Table {

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return 'web_request_handlers';
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createColumns() {
		return [
			new IntegerColumn($this, WebRequestHandlersColumnId::ID, 'id', false, true),
			new StringColumn($this, WebRequestHandlersColumnId::PATH_REGEX, 'path_regex', false),
			new StringColumn($this, WebRequestHandlersColumnId::HTTP_METHOD, 'http_method'),
			new StringColumn($this, WebRequestHandlersColumnId::HOST, 'host'),
			new IntegerColumn($this, WebRequestHandlersColumnId::RANK, 'rank'),
			new StringColumn($this, WebRequestHandlersColumnId::CLASS_NAME, 'class_name', false),
		];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createParentRelationships() {
		return [];
	}

}

?>
