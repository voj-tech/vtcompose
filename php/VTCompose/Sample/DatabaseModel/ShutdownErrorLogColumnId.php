<?php

namespace VTCompose\Sample\DatabaseModel;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class ShutdownErrorLogColumnId extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const ID = 0;
	
	/**
	 * 
	 *
	 * 
	 */
	const MESSAGE = 1;
	
	/**
	 * 
	 *
	 * 
	 */
	const SEVERITY = 2;
	
	/**
	 * 
	 *
	 * 
	 */
	const FILENAME = 3;
	
	/**
	 * 
	 *
	 * 
	 */
	const LINE_NUMBER = 4;
	
	/**
	 * 
	 *
	 * 
	 */
	const TIME = 5;

}

?>
