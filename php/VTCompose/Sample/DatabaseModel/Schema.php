<?php

namespace VTCompose\Sample\DatabaseModel;

/**
 * 
 *
 * 
 */
class Schema extends \VTCompose\Data\DatabaseModel\Schema {

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return 'vtcompose';
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	protected function createTables() {
		$database = $this->getDatabase();
	
		return [
			new WebRequestHandlersTable($database, TableId::WEB_REQUEST_HANDLERS, $this),
			new UncaughtExceptionLogTable($database, TableId::UNCAUGHT_EXCEPTION_LOG, $this),
			new ShutdownErrorLogTable($database, TableId::SHUTDOWN_ERROR_LOG, $this),
		];
	}

}

?>
