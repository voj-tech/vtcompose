<?php

namespace VTCompose\Sample\DatabaseModel;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class WebRequestHandlersColumnId extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const ID = 0;
	
	/**
	 * 
	 *
	 * 
	 */
	const PATH_REGEX = 1;
	
	/**
	 * 
	 *
	 * 
	 */
	const HTTP_METHOD = 2;
	
	/**
	 * 
	 *
	 * 
	 */
	const HOST = 3;
	
	/**
	 * 
	 *
	 * 
	 */
	const RANK = 4;
	
	/**
	 * 
	 *
	 * 
	 */
	const CLASS_NAME = 5;

}

?>
