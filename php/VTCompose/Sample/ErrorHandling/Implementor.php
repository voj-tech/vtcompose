<?php

namespace VTCompose\Sample\ErrorHandling;

use Throwable;
use VTCompose\Data\DataAdapter;
use VTCompose\Data\DatabaseConnection\Transaction;
use VTCompose\Data\DataTable;
use VTCompose\ErrorHandling\IExceptionHandlerImplementor;
use VTCompose\ErrorHandling\IShutdownErrorHandlerImplementor;
use VTCompose\Http\Header\Factory;
use VTCompose\Http\StatusCode;
use VTCompose\Logging\ILogger;
use VTCompose\Logging\Severity;
use VTCompose\Sample\DatabaseModel\ShutdownErrorLogColumnId;
use VTCompose\Sample\DatabaseModel\TableId;
use VTCompose\Sample\DatabaseModel\UncaughtExceptionLogColumnId;

/**
 * 
 *
 * 
 *
 * @todo When the functions dealing with HTTP are wrapped in a specialized set of classes use them here.
 */
class Implementor implements IExceptionHandlerImplementor, IShutdownErrorHandlerImplementor {
	
	private $dataAdapter;
	private $logger;
	private $errorPageFilename;
	private $errorPageHeaders;
	
	private static function getHttpHeaderFactory() {
		static $httpHeaderFactory = NULL;
		if ($httpHeaderFactory == NULL) {
			$httpHeaderFactory = new Factory();
		}
		
		return $httpHeaderFactory;
	}
	
	private function log($severity, $message, ...$parameters) {
		try {
			$this->logger->log($severity, $message, ...$parameters);
		} catch (Throwable $exception) {
		}
	}
	
	/**
	 * @todo Move to a common place.
	 */
	private function getFullyQualifiedClassName($object) {
		$fullyQualifiedClassName = get_class($object);
		if ($fullyQualifiedClassName === false) {
			$this->log(Severity::ERROR, 'Failed to get the name of the class of an object.');
		}
		
		return $fullyQualifiedClassName;
	}
	
	private function outputHasBeenSent() {
		try {
			return headers_sent();
		} catch (Throwable $exception) {
			$message = 'Caught exception with message "%s" while calling headers_sent().';
			$this->log(Severity::ERROR, $message, $exception->getMessage());
			return true;
		}
	}
	
	private function setHttpResponseStatusCode($statusCode) {
		try {
			$previousStatusCode = http_response_code($statusCode);
		} catch (Throwable $exception) {
			$message = 'Caught exception with message "%s" while calling http_response_code() for status ' .
					'code = %d.';
			$this->log(Severity::ERROR, $message, $exception->getMessage(), $statusCode);
			return;
		}
		
		if ($previousStatusCode === false) {
			$this->log(Severity::ERROR, 'Failed to set the HTTP response status code.');
		}
	}
	
	private function clearHttpHeaders() {
		try {
			header_remove();
		} catch (Throwable $exception) {
			$message = 'Caught exception with message "%s" while calling header_remove().';
			$this->log(Severity::ERROR, $message, $exception->getMessage());
		}
	}
	
	private function addHttpHeader($fieldName, $fieldValue) {
		try {
			$string = self::getHttpHeaderFactory()->createHeader($fieldName, $fieldValue)->toString();
		} catch (Throwable $exception) {
			$this->log(Severity::ERROR, $exception);
			return;
		}
		
		try {
			$replace = false;
			header($string, $replace);
		} catch (Throwable $exception) {
			$message = 'Caught exception with message "%s" while calling header() for string = "%s".';
			$this->log(Severity::ERROR, $message, $exception->getMessage(), $string);
		}
	}
	
	private function outputFile($filename) {
		try {
			$numBytesRead = readfile($filename);
		} catch (Throwable $exception) {
			$message = 'Caught exception with message "%s" while calling readfile() for filename = "%s".';
			$this->log(Severity::ERROR, $message, $exception->getMessage(), $filename);
			return;
		}
		
		if ($numBytesRead === false) {
			$this->log(Severity::ERROR, 'Failed to output a file.');
		}
	}
	
	private function outputErrorPageIfPossible() {
		if ($this->outputHasBeenSent()) {
			return;
		}
		
		$this->setHttpResponseStatusCode(StatusCode::INTERNAL_SERVER_ERROR);
		
		$this->clearHttpHeaders();
		foreach ($this->errorPageHeaders as $fieldName => $fieldValue) {
			$this->addHttpHeader($fieldName, $fieldValue);
		}
		
		$this->outputFile($this->errorPageFilename);
	}
	
	private function openConnectionIfNotOpen() {
		$connection = $this->dataAdapter->getConnection();
		if ($connection->isOpen()) {
			return;
		}
		
		$connection->open();
	}
	
	private function closeConnectionIfOpen() {
		try {
			$connection = $this->dataAdapter->getConnection();
			if (!$connection->isOpen()) {
				return;
			}
			
			$connection->close();
		} catch (Throwable $exception) {
			$this->log(Severity::ERROR, $exception);
		}
	}
	
	private function rollbackTransaction(Transaction $transaction) {
		try {
			$transaction->rollback();
		} catch (Throwable $exception) {
			$this->log(Severity::ERROR, $exception);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param DataAdapter 
	 * @param ILogger 
	 * @param string 
	 * @param array 
	 */
	public function __construct(DataAdapter $dataAdapter, ILogger $logger,
			$errorPageFilename, array $errorPageHeaders) {
		$this->dataAdapter = $dataAdapter;
		$this->logger = $logger;
		$this->errorPageFilename = $errorPageFilename;
		$this->errorPageHeaders = $errorPageHeaders;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param Throwable 
	 */
	public function handleException(Throwable $exception) {
		$this->outputErrorPageIfPossible();
		
		$this->log(Severity::ERROR, $exception);
		
		try {
			$this->openConnectionIfNotOpen();
			$transaction = $this->dataAdapter->getConnection()->startTransaction();
		
			$tables = $this->dataAdapter->getDatabaseModel()->getTables();
			$dataTable = new DataTable($tables[TableId::UNCAUGHT_EXCEPTION_LOG]);
			
			$currentException = $exception;
			while ($currentException != NULL) {
				$fullyQualifiedClassName = $this->getFullyQualifiedClassName($currentException);
				$array = [
					UncaughtExceptionLogColumnId::CLASS_NAME	=> $fullyQualifiedClassName,
					UncaughtExceptionLogColumnId::MESSAGE		=> $currentException->getMessage(),
					UncaughtExceptionLogColumnId::CODE			=> $currentException->getCode(),
					UncaughtExceptionLogColumnId::FILENAME		=> $currentException->getFile(),
					UncaughtExceptionLogColumnId::LINE_NUMBER	=> $currentException->getLine(),
					UncaughtExceptionLogColumnId::STACK_TRACE	=> $currentException->getTraceAsString(),
				];
				
				$dataRow = $dataTable->newRow();
				$dataRow->setItemArray($array);
				$dataTable->getRows()->add($dataRow);
				
				$currentException = $currentException->getPrevious();
			}
			
			$this->dataAdapter->store($dataTable);
			
			$rows = $dataTable->getRows();
			for ($i = 1; $i < $rows->count(); $i++) {
				$innerExceptionId = $rows[$i][UncaughtExceptionLogColumnId::ID];
				$rows[$i - 1]->setItem(UncaughtExceptionLogColumnId::INNER_EXCEPTION_ID, $innerExceptionId);
			}
			
			$this->dataAdapter->store($dataTable);
			
			$transaction->commit();
		} catch (Throwable $anotherException) {
			$this->log(Severity::ERROR, $anotherException);
			
			if (isset($transaction)) {
				$this->rollbackTransaction($transaction);
			}
		} finally {
			$this->closeConnectionIfOpen();
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 * @param string 
	 * @param int 
	 */
	public function handleError($message, $severity, $filename, $lineNumber) {
		$this->outputErrorPageIfPossible();
		
		$logMessage = 'Detected error at shutdown ' .
				'(message = "%s", severity = %d, filename = "%s", line number = %d).';
		$this->log(Severity::ERROR, $logMessage, $message, $severity, $filename, $lineNumber);
		
		try {
			$this->openConnectionIfNotOpen();
		
			$array = [
				ShutdownErrorLogColumnId::MESSAGE		=> $message,
				ShutdownErrorLogColumnId::SEVERITY		=> $severity,
				ShutdownErrorLogColumnId::FILENAME		=> $filename,
				ShutdownErrorLogColumnId::LINE_NUMBER	=> $lineNumber,
			];
			
			$tables = $this->dataAdapter->getDatabaseModel()->getTables();
			$dataTable = new DataTable($tables[TableId::SHUTDOWN_ERROR_LOG]);
			$dataRow = $dataTable->newRow();
			$dataRow->setItemArray($array);
			$dataTable->getRows()->add($dataRow);
			$this->dataAdapter->store($dataTable);
		} catch (Throwable $exception) {
			$this->log(Severity::ERROR, $exception);
		} finally {
			$this->closeConnectionIfOpen();
		}
	}

}

?>
