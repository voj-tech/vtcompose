<?php

namespace VTCompose;

use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\Exception;
use VTCompose\Exception\NotSupportedException;
use VTCompose\StringUtilities as StringUtils;
use VTCompose\Text\RegularExpressions\Regex;

/**
 * 
 *
 * 
 */
final class Environment {

	private static function getVariableNameIsValidRegex() {
		static $regex = NULL;
		if ($regex == NULL) {
			$regex = new Regex('^[^=]+\\z');
		}
		
		return $regex;
	}

	private static function variableNameIsValid($variable) {
		$variableLength = StringUtils::length($variable);
		return $variableLength < 32767 && self::getVariableNameIsValidRegex()->isMatch($variable);
	}
	
	private static function variableValueIsValid($value) {
		return $value === NULL || StringUtils::length($value) < 32767;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @throws ArgumentException .
	 * @throws ArgumentException .
	 * @throws Exception .
	 */
	public static function setEnvironmentVariable($variable, $value) {
		if (!self::variableNameIsValid($variable)) {
			throw new ArgumentException('Variable name is invalid.');
		}
		
		if (!self::variableValueIsValid($value)) {
			throw new ArgumentException('Variable value is invalid.');
		}
		
		if (!putenv($variable . ($value !== NULL ? '=' . $value : ''))) {
			throw new Exception('Unable to set an environment variable.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 */
	public static function getEnvironmentVariable($variable) {
		$value = getenv($variable);
		return $value !== false ? $value : NULL;
	}
	
	private function __construct() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}

}

?>
