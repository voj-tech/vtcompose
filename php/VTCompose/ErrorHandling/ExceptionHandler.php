<?php

namespace VTCompose\ErrorHandling;

/**
 * 
 *
 * 
 */
class ExceptionHandler {

	private $implementor;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IExceptionHandlerImplementor 
	 */
	public function __construct(IExceptionHandlerImplementor $implementor) {
		$this->implementor = $implementor;
	}

	/**
	 * 
	 *
	 * 
	 */
	public function register() {
		set_exception_handler([$this->implementor, 'handleException']);
	}

}

?>
