<?php

namespace VTCompose\ErrorHandling;

use Exception;

/**
 * 
 *
 * 
 *
 * @todo Override the __toString() method to include the severity in the string representation.
 */
class ErrorException extends Exception {

	private $severity;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getSeverity() {
		return $this->severity;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 * @param string 
	 * @param int 
	 */
	public function __construct($message, $severity, $filename, $lineNumber) {
		$code = 0;
		parent::__construct($message, $code);
		$this->file = $filename;
		$this->line = $lineNumber;
		$this->severity = $severity;
	}

}

?>
