<?php

namespace VTCompose\ErrorHandling;

/**
 * 
 *
 * 
 */
class ErrorHandler {

	private $shutdownErrorHandlerImplementor;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IShutdownErrorHandlerImplementor 
	 */
	public function __construct(IShutdownErrorHandlerImplementor $shutdownErrorHandlerImplementor = NULL) {
		$this->shutdownErrorHandlerImplementor = $shutdownErrorHandlerImplementor;
	}

	/**
	 * 
	 *
	 * 
	 */
	public function register() {
		$errorHandler = function($errno, $errstr, $errfile, $errline) {
			if (($errno & error_reporting()) == 0) {
				return true;
			}
			
			throw new ErrorException($errstr, $errno, $errfile, $errline);
		};
	
		set_error_handler($errorHandler);
		
		if ($this->shutdownErrorHandlerImplementor != NULL) {
			$shutdownFunction = function() {
				$error = error_get_last();
				if (!isset($error['message']) || !isset($error['type']) ||
						!isset($error['file']) || !isset($error['line'])) {
					return;
				}
				
				$message = $error['message'];
				$severity = $error['type'];
				$filename = $error['file'];
				$lineNumber = $error['line'];
				$shutdownErrorHandlerImplementor = $this->shutdownErrorHandlerImplementor;
				$shutdownErrorHandlerImplementor->handleError($message, $severity, $filename, $lineNumber);
			};
		
			register_shutdown_function($shutdownFunction);
		}
	}

}

?>
