<?php

namespace VTCompose\Collection;

/**
 * 
 *
 * 
 */
interface IOrderedEnumerable extends IEnumerable {

	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param IComparer 
	 * @return IOrderedEnumerable 
	 */
	public function thenBy(callable $keySelector, IComparer $comparer = NULL);
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param IComparer 
	 * @return IOrderedEnumerable 
	 */
	public function thenByDescending(callable $keySelector, IComparer $comparer = NULL);

}

?>
