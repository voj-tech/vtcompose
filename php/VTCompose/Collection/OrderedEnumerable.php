<?php

namespace VTCompose\Collection;

use Traversable;
use VTCompose\Exception\Exception;

/**
 * @internal
 * @todo Optimise sorting by computing and caching keys ahead, i.e. not computing them each time a comparison
 * is made.
 */
class OrderedEnumerable implements IOrderedEnumerable {

	use Enumerable;
	
	private $source;
	private $descending;
	private $keySelector;
	private $comparer;
	private $parent;
	
	private $compareFunction;

	public function __construct(IEnumerable $source, $descending, callable $keySelector,
			IComparer $comparer = NULL, OrderedEnumerable $parent = NULL) {
		$this->source = $source;
		$this->descending = $descending;
		$this->keySelector = $keySelector;
		$this->comparer = $comparer != NULL ? $comparer : Comparer::getDefault();
		$this->parent = $parent;
	}
	
	public function thenBy(callable $keySelector, IComparer $comparer = NULL) {
		$descending = false;
		$parent = $this;
		return new self($this->source, $descending, $keySelector, $comparer, $parent);
	}
	
	public function thenByDescending(callable $keySelector, IComparer $comparer = NULL) {
		$descending = true;
		$parent = $this;
		return new self($this->source, $descending, $keySelector, $comparer, $parent);
	}
	
	private function createCompareFunction(callable $childCompareFunction = NULL) {
		$compareFunction = function($x, $y) use ($childCompareFunction) {
			$keySelector = $this->keySelector;
			
			$result = $this->comparer->compare($keySelector($x[1]), $keySelector($y[1]));
			if ($result == 0) {
				return $childCompareFunction != NULL ? $childCompareFunction($x, $y) : $x[0] - $y[0];
			}
			
			return $this->descending ? -$result : $result;
		};
		
		if ($this->parent != NULL) {
			return $this->parent->createCompareFunction($compareFunction);
		} else {
			return $compareFunction;
		}
	}
	
	private function getCompareFunction() {
		if (!isset($this->compareFunction)) {
			$this->compareFunction = $this->createCompareFunction();
		}
		
		return $this->compareFunction;
	}

	public function getIterator(): Traversable {
		$array = $this->source->toArray();
		
		$index = 0;
		foreach ($array as &$item) {
			$item = [$index++, $item];
		}
		unset($item);
		
		if (!usort($array, $this->getCompareFunction())) {
			throw new Exception('Unable to sort an array.');
		}
		
		foreach ($array as $item) {
			yield $item[1];
		}
	}

}

?>
