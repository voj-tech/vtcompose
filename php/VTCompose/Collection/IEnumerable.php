<?php

namespace VTCompose\Collection;

use Countable;
use IteratorAggregate;

/**
 * 
 *
 * 
 *
 * @todo Add where() method.
 */
interface IEnumerable extends IteratorAggregate, Countable {

	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param IComparer 
	 * @return IOrderedEnumerable 
	 */
	public function orderBy(callable $keySelector, IComparer $comparer = NULL);
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param IComparer 
	 * @return IOrderedEnumerable 
	 */
	public function orderByDescending(callable $keySelector, IComparer $comparer = NULL);

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray();

	/**
	 * 
	 *
	 * 
	 *
	 * @return ArrayList 
	 */
	public function toArrayList();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param callable 
	 * @return Dictionary 
	 */
	public function toDictionary(callable $keySelector, callable $valueSelector = NULL);
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 * @todo Add optional predicate parameter.
	 */
	public function first();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 * @todo Add optional predicate parameter.
	 */
	public function firstOrNull();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 * @todo Add optional predicate parameter.
	 */
	public function last();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 * @todo Add optional predicate parameter.
	 */
	public function lastOrNull();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 * @todo Add optional predicate parameter.
	 */
	public function single();
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 * @todo Add optional predicate parameter.
	 */
	public function singleOrNull();

}

?>
