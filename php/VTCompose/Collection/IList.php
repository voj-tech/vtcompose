<?php

namespace VTCompose\Collection;

use ArrayAccess;

/**
 * 
 *
 * 
 */
interface IList extends ICollection, ArrayAccess {

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return int 
	 */
	public function indexOf($value);

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param mixed 
	 */
	public function insert($index, $value);

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function removeAt($index);

}

?>
