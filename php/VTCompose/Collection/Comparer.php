<?php

namespace VTCompose\Collection;

/**
 * 
 *
 * 
 */
final class Comparer implements IComparer {

	/**
	 * 
	 *
	 * 
	 *
	 * @return IComparer 
	 */
	public static function getDefault() {
		static $comparer = NULL;
		if ($comparer == NULL) {
			$comparer = new self();
		}
		
		return $comparer;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 * @return int 
	 */
	public function compare($x, $y) {
		if ($x == $y) {
			return 0;
		}
		
		return $x < $y ? -1 : 1;
	}

}

?>
