<?php

namespace VTCompose\Collection;

/**
 * 
 *
 * 
 */
final class KeyValuePair {

	private $key;
	private $value;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function __construct($key, $value) {
		$this->key = $key;
		$this->value = $value;
	}

}

?>
