<?php

namespace VTCompose\Collection;

use IteratorAggregate;
use Traversable;
use VTCompose\Exception\ArgumentException;

/**
 * 
 *
 * 
 */
class Set implements ISet {

	use Enumerable;

	private $data;
	
	private static function encodeValue($value) {
		if (is_scalar($value) || is_null($value)) {
			if ((string) (int) $value === $value || is_float($value) || is_bool($value)) {
				$value = (int) $value;
			}
			return 's' . $value;
		} elseif (is_object($value)) {
			return 'o' . spl_object_hash($value);
		} else {
			throw new ArgumentException('Value is of an unsupported type.');
		}
	}
	
	private function encodedValueIsValid($encodedValue) {
		return array_key_exists($encodedValue, $this->data);
	}
	
	private function initializeData() {
		$this->data = [];
	}
	
	private function addToData($value) {
		$this->data[self::encodeValue($value)] = $value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	public function __construct($collection = []) {
		if ($collection instanceof self) {
			$this->data = $collection->data;
		} elseif (is_array($collection) || $collection instanceof IteratorAggregate) {
			if ($collection instanceof IDictionary) {
				$collection = $collection->toArray();
			}
		
			$this->initializeData();
			foreach ($collection as $value) {
				$this->addToData($value);
			}
		} else {
			throw new ArgumentException('Argument is of an unsupported type.');
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->addToData($value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		$this->initializeData();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		return $this->encodedValueIsValid(self::encodeValue($value));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		$encodedValue = self::encodeValue($value);

		if (!$this->encodedValueIsValid($encodedValue)) {
			return false;
		}

		unset($this->data[$encodedValue]);

		return true;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->data;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		foreach ($this->data as $value) {
			yield $value;
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return count($this->data);
	}

}

?>
