<?php

namespace VTCompose\Collection;

/**
 * 
 *
 * 
 */
interface ICollection extends IEnumerable {

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value);

	/**
	 * 
	 *
	 * 
	 */
	public function clear();

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value);

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value);

}

?>
