<?php

namespace VTCompose\Collection;

use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
trait Enumerable {

	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param IComparer 
	 * @return IOrderedEnumerable 
	 */
	public function orderBy(callable $keySelector, IComparer $comparer = NULL) {
		$descending = false;
		return new OrderedEnumerable($this, $descending, $keySelector, $comparer);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param IComparer 
	 * @return IOrderedEnumerable 
	 */
	public function orderByDescending(callable $keySelector, IComparer $comparer = NULL) {
		$descending = true;
		return new OrderedEnumerable($this, $descending, $keySelector, $comparer);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		$array = [];
		foreach ($this as $value) {
			$array[] = $value;
		}
		return $array;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return ArrayList 
	 */
	public function toArrayList() {
		return new ArrayList($this);
	}
	
	private static function getIdentityFunction() {
		static $function = NULL;
		if ($function == NULL) {
			$function = function($x) { return $x; };
		}
		
		return $function;
	}
	
	private function getSequence() {
		return $this instanceof IDictionary ? $this->toArray() : $this;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param callable 
	 * @param callable 
	 * @return Dictionary 
	 */
	public function toDictionary(callable $keySelector, callable $valueSelector = NULL) {
		if ($valueSelector == NULL) {
			$valueSelector = self::getIdentityFunction();
		}
		
		$dictionary = new Dictionary();
		foreach ($this->getSequence() as $value) {
			$dictionary->addAt($keySelector($value), $valueSelector($value));
		}
		return $dictionary;
	}
	
	private function throwSequenceEmptyException() {
		throw new InvalidOperationException('Sequence is empty.');
	}
	
	private function throwSequenceContainsMoreThanOneElementException() {
		throw new InvalidOperationException('Sequence contains more than one element.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function first() {
		if ($this instanceof IList) {
			if ($this->count() > 0) {
				return $this[0];
			}
		} else {
			foreach ($this->getSequence() as $value) {
				return $value;
			}
		}
		
		$this->throwSequenceEmptyException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function firstOrNull() {
		if ($this instanceof IList) {
			if ($this->count() > 0) {
				return $this[0];
			}
		} else {
			foreach ($this->getSequence() as $value) {
				return $value;
			}
		}
		
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function last() {
		if ($this instanceof IList) {
			$count = $this->count();
			if ($count > 0) {
				return $this[$count - 1];
			}
		} else {
			$nonEmpty = false;
			foreach ($this->getSequence() as $value) {
				$result = $value;
				$nonEmpty = true;
			}
			if ($nonEmpty) {
				return $result;
			}
		}
		
		$this->throwSequenceEmptyException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function lastOrNull() {
		if ($this instanceof IList) {
			$count = $this->count();
			if ($count > 0) {
				return $this[$count - 1];
			}
		} else {
			$nonEmpty = false;
			foreach ($this->getSequence() as $value) {
				$result = $value;
				$nonEmpty = true;
			}
			if ($nonEmpty) {
				return $result;
			}
		}
		
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function single() {
		if ($this instanceof IList) {
			$count = $this->count();
			if ($count == 1) {
				return $this[0];
			} elseif ($count > 1) {
				$this->throwSequenceContainsMoreThanOneElementException();
			}
		} else {
			$nonEmpty = false;
			foreach ($this->getSequence() as $value) {
				$result = $value;
				if ($nonEmpty) {
					$this->throwSequenceContainsMoreThanOneElementException();
				}
				$nonEmpty = true;
			}
			if ($nonEmpty) {
				return $result;
			}
		}
		
		$this->throwSequenceEmptyException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function singleOrNull() {
		if ($this instanceof IList) {
			$count = $this->count();
			if ($count == 1) {
				return $this[0];
			} elseif ($count > 1) {
				$this->throwSequenceContainsMoreThanOneElementException();
			}
		} else {
			$nonEmpty = false;
			foreach ($this->getSequence() as $value) {
				$result = $value;
				if ($nonEmpty) {
					$this->throwSequenceContainsMoreThanOneElementException();
				}
				$nonEmpty = true;
			}
			if ($nonEmpty) {
				return $result;
			}
		}
		
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 * @todo Add optional predicate parameter.
	 */
	public function count(): int {
		$count = 0;
		foreach ($this as $value) {
			$count++;
		}
		return $count;
	}

}

?>
