<?php

namespace VTCompose\Collection;

use IteratorAggregate;
use Traversable;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\KeyNotFoundException;

/**
 * 
 *
 * 
 */
class Dictionary implements IDictionary {

	use Enumerable;

	private $data;

	private $keys;
	private $values;

	private $nextIntegerKey;

	private static function encodeKey(&$key) {
		if (is_scalar($key) || is_null($key)) {
			if ((string) (int) $key === $key || is_float($key) || is_bool($key)) {
				$key = (int) $key;
			}
			return 's' . $key;
		} elseif (is_object($key)) {
			return 'o' . spl_object_hash($key);
		} else {
			throw new ArgumentException('Key is of an unsupported type.');
		}
	}

	private function encodedKeyIsValid($encodedKey) {
		return isset($this->data[$encodedKey]);
	}

	private function validateValue($value) {
		if (!$value instanceof KeyValuePair) {
			throw new ArgumentException('Value is not an instance of KeyValuePair.');
		}
	}

	private function validateEncodedKey($encodedKey) {
		if (!$this->encodedKeyIsValid($encodedKey)) {
			throw new KeyNotFoundException('Key is not present in the dictionary.');
		}
	}

	private function encodedKeyOf(KeyValuePair $value) {
		return array_search($value, $this->data, true);
	}
	
	private function initializeData() {
		$this->data = $this->keys = $this->values = [];
		$this->nextIntegerKey = 0;
	}

	private function setAt($encodedKey, KeyValuePair $value) {
		$this->data[$encodedKey] = $value;
		$this->keys[$encodedKey] = $value->getKey();
		$this->values[$encodedKey] = $value->getValue();
	}

	private function unsetAt($encodedKey) {
		unset($this->data[$encodedKey]);
		unset($this->keys[$encodedKey]);
		unset($this->values[$encodedKey]);
	}
	
	private function addToData($value) {
		$this->validateValue($value);

		$key = $value->getKey();
		$encodedKey = self::encodeKey($key);

		$this->setAt($encodedKey, $value);

		if (is_int($key) && $key >= $this->nextIntegerKey) {
			$this->nextIntegerKey = $key + 1;
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	public function __construct($collection = []) {
		if ($collection instanceof self) {
			$this->data = $collection->data;
			$this->keys = $collection->keys;
			$this->values = $collection->values;
			$this->nextIntegerKey = $collection->nextIntegerKey;
		} elseif (is_array($collection) || $collection instanceof IteratorAggregate) {
			$this->initializeData();
			foreach ($collection as $value) {
				$this->addToData($value);
			}
		} else {
			throw new ArgumentException('Argument is of an unsupported type.');
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return ICollection 
	 * @todo When there are read-only versions of collections keep the key collection as a member variable and
	 * return a read-only wrapper here.
	 */
	public function getKeys() {
		return new Collection($this->keys);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return ICollection 
	 * @todo When there are read-only versions of collections keep the value collection as a member variable
	 * and return a read-only wrapper here.
	 */
	public function getValues() {
		return new Collection($this->values);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->addToData($value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function addAt($key, $value) {
		$this->addToData(new KeyValuePair($key, $value));
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		$this->initializeData();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		$this->validateValue($value);
		return $this->encodedKeyOf($value) !== false;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function containsKey($key) {
		return $this->encodedKeyIsValid(self::encodeKey($key));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		if (!$this->contains($value)) {
			return false;
		}

		return $this->removeAt($value->getKey());
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function removeAt($key) {
		$encodedKey = self::encodeKey($key);

		if (!$this->encodedKeyIsValid($encodedKey)) {
			return false;
		}

		$this->unsetAt($encodedKey);

		return true;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->data;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		foreach ($this->data as $keyValuePair) {
			yield $keyValuePair->getKey() => $keyValuePair->getValue();
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function offsetExists($offset): bool {
		return $this->containsKey($offset);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function offsetGet($offset): mixed {
		$encodedKey = self::encodeKey($offset);
		$this->validateEncodedKey($encodedKey);
		return $this->data[$encodedKey]->getValue();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function offsetSet($offset, $value): void {
		if ($offset === NULL) {
			$offset = $this->nextIntegerKey;
		}

		$this->addAt($offset, $value);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function offsetUnset($offset): void {
		$encodedKey = self::encodeKey($offset);
		$this->validateEncodedKey($encodedKey);
		$this->unsetAt($encodedKey);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return count($this->data);
	}

}

?>
