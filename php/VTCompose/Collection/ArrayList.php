<?php

namespace VTCompose\Collection;

use IteratorAggregate;
use Traversable;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\ArgumentOutOfRangeException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class ArrayList implements IList {

	use Enumerable;

	private $data;

	private function indexIsValid($index, $customMax = NULL) {
		$max = $customMax !== NULL ? $customMax : count($this->data) - 1;
		return $index >= 0 && $index <= $max;
	}

	private function validateOffset($offset) {
		if (!is_int($offset)) {
			throw new ArgumentException('Offset must be of type int.');
		}
	}

	private function validateIndex($index, $customMax = NULL) {
		if (!$this->indexIsValid($index, $customMax)) {
			throw new ArgumentOutOfRangeException('Index is out of range.');
		}
	}
	
	private function initializeData() {
		$this->data = [];
	}
	
	private function addToData($value) {
		$this->data[] = $value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	public function __construct($collection = []) {
		if ($collection instanceof ICollection) {
			$collection = $collection->toArray();
		}

		if (is_array($collection)) {
			$this->data = array_values($collection);
		} elseif ($collection instanceof IteratorAggregate) {
			$this->initializeData();
			foreach ($collection as $value) {
				$this->addToData($value);
			}
		} else {
			throw new ArgumentException('Argument is of an unsupported type.');
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->addToData($value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		$this->initializeData();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		return $this->indexOf($value) >= 0;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return int 
	 */
	public function indexOf($value) {
		$result = array_search($value, $this->data, true);
		return $result !== false ? $result : -1;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param mixed 
	 */
	public function insert($index, $value) {
		$this->validateIndex($index, count($this->data));
		array_splice($this->data, $index, 0, [$value]);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		$index = $this->indexOf($value);

		if ($index < 0) {
			return false;
		}

		$this->removeAt($index);

		return true;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 */
	public function removeAt($index) {
		$this->validateIndex($index);
		array_splice($this->data, $index, 1);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->data;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		foreach ($this->data as $index => $value) {
			yield $index => $value;
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function offsetExists($offset): bool {
		$this->validateOffset($offset);
		return $this->indexIsValid($offset);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return mixed 
	 */
	public function &offsetGet($offset): mixed {
		$this->validateOffset($offset);
		$this->validateIndex($offset);
		return $this->data[$offset];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function offsetSet($offset, $value): void {
		if ($offset === NULL) {
			$this->add($value);
			return;
		}

		$this->validateOffset($offset);
		$this->validateIndex($offset);
		$this->data[$offset] = $value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws NotSupportedException .
	 */
	public function offsetUnset($offset): void {
		$this->validateOffset($offset);
		$this->validateIndex($offset);

		if ($offset == count($this->data) - 1) {
			unset($this->data[$offset]);
			return;
		}

		throw new NotSupportedException('Unset operation is valid only on the last element.');
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return count($this->data);
	}

}

?>
