<?php

namespace VTCompose\Collection;

use ArrayAccess;

/**
 * 
 *
 * 
 */
interface IDictionary extends ICollection, ArrayAccess {

	/**
	 * 
	 *
	 * 
	 *
	 * @return ICollection 
	 */
	public function getKeys();

	/**
	 * 
	 *
	 * 
	 *
	 * @return ICollection 
	 */
	public function getValues();

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param mixed 
	 */
	public function addAt($key, $value);

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function containsKey($key);

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function removeAt($key);

}

?>
