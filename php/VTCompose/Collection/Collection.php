<?php

namespace VTCompose\Collection;

use IteratorAggregate;
use Traversable;
use VTCompose\Exception\ArgumentException;

/**
 * 
 *
 * 
 */
class Collection implements ICollection {

	use Enumerable;

	private $data;

	private function keyOf($value) {
		return array_search($value, $this->data, true);
	}
	
	private function initializeData() {
		$this->data = [];
	}
	
	private function addToData($value) {
		$this->data[] = $value;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws ArgumentException .
	 */
	public function __construct($collection = []) {
		if ($collection instanceof ICollection) {
			$collection = $collection->toArray();
		}

		if (is_array($collection)) {
			$this->data = $collection;
		} elseif ($collection instanceof IteratorAggregate) {
			$this->initializeData();
			foreach ($collection as $value) {
				$this->addToData($value);
			}
		} else {
			throw new ArgumentException('Argument is of an unsupported type.');
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function add($value) {
		$this->addToData($value);
	}

	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		$this->initializeData();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function contains($value) {
		return $this->keyOf($value) !== false;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @return bool 
	 */
	public function remove($value) {
		$key = $this->keyOf($value);

		if ($key === false) {
			return false;
		}

		unset($this->data[$key]);

		return true;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public function toArray() {
		return $this->data;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		foreach ($this->data as $value) {
			yield $value;
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return count($this->data);
	}

}

?>
