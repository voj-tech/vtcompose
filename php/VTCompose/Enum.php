<?php

namespace VTCompose;

use ReflectionClass;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\NotSupportedException;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
abstract class Enum {
	
	private static function getConstants() {
		static $constants = [];

		if (!isset($constants[static::class])) {
			$constants[static::class] = (new ReflectionClass(static::class))->getConstants();
		}

		return $constants[static::class];
	}
	
	private static function getConstantsNamesToLower() {
		static $constants = [];

		if (!isset($constants[static::class])) {
			$constants[static::class] = [];
			foreach (self::getConstants() as $name => $value) {
				$name = StringUtils::toLower($name);
				if (!isset($constants[static::class][$name])) {
					$constants[static::class][$name] = $value;
				}
			}
		}

		return $constants[static::class];
	}
	
	private static function getConstantsValuesToLower() {
		static $constants = [];

		if (!isset($constants[static::class])) {
			$constants[static::class] = [];
			foreach (self::getConstants() as $name => $value) {
				$constants[static::class][$name] = StringUtils::toLower($value);
			}
		}

		return $constants[static::class];
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param bool 
	 * @return string 
	 */
	public static function getName($value, $ignoreCase = false) {
		if ($ignoreCase) {
			$value = StringUtils::toLower($value);
			$constants = self::getConstantsValuesToLower();
		} else {
			$constants = self::getConstants();
		}
		
		$name = array_search($value, $constants, true);
		return $name !== false ? $name : NULL;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public static function getNames() {
		return array_keys(self::getConstants());
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return array 
	 */
	public static function getValues() {
		return array_values(self::getConstants());
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param bool 
	 * @return bool 
	 */
	public static function isDefined($value, $ignoreCase = false) {
		return self::getName($value, $ignoreCase) !== NULL;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param bool 
	 * @return mixed 
	 * @throws ArgumentException .
	 */
	public static function parse($name, $ignoreCase = false) {
		if (!self::tryParse($name, $result, $ignoreCase)) {
			throw new ArgumentException('Specified name does not represent an Enum class constant.');
		}
		
		return $result;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param mixed 
	 * @param bool 
	 * @return bool 
	 */
	public static function tryParse($name, &$result, $ignoreCase = false) {
		$name = trim($name);
		if ($ignoreCase) {
			$name = StringUtils::toLower($name);
			$constants = self::getConstantsNamesToLower();
		} else {
			$constants = self::getConstants();
		}
		
		if (array_key_exists($name, $constants)) {
			$result = $constants[$name];
			return true;
		} else {
			return false;
		}
	}
	
	private function __construct() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}

}

?>
