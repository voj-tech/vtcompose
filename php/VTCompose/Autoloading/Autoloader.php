<?php

namespace VTCompose\Autoloading;

/**
 * 
 *
 * 
 */
class Autoloader {

	/**
	 * Registers autoloader with SPL autoloader stack.
	 *
	 * 
	 */
	public function register() {
		spl_autoload_register([$this, 'loadClass']);
	}

	private function loadClass($fullyQualifiedClassName) {
		require_once str_replace('\\', '/', $fullyQualifiedClassName) . '.php';
	}

}

?>
