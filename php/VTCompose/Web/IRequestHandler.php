<?php

namespace VTCompose\Web;

use VTCompose\Http\Request;
use VTCompose\Http\Response;

/**
 * 
 *
 * 
 */
interface IRequestHandler {

	/**
	 * 
	 *
	 * 
	 *
	 * @param Request 
	 * @param Context 
	 * @return Response 
	 */
	public function handle(Request $request, Context $context);

}

?>
