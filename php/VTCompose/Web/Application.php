<?php

namespace VTCompose\Web;

use VTCompose\Environment;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\Exception;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Http\Header\Factory;
use VTCompose\Http\Header\FieldName;
use VTCompose\Http\Request;
use VTCompose\Http\Response;
use VTCompose\Http\StatusCode;
use VTCompose\Http\Utilities as HttpUtils;
use VTCompose\IO\IOException;
use VTCompose\Logging\ILogger;
use VTCompose\Logging\Severity;

/**
 * 
 *
 * 
 *
 * @todo When the functions dealing with HTTP are wrapped in a specialized set of classes use them here.
 */
class Application {

	private $requestHandlerFactory;
	private $errorHandlerFactory;
	private $logger;
	
	private $request;
	
	private $environmentVariableCache;

	private function getRequest() {
		if (!isset($this->request)) {
			$this->request = $this->createRequest();
		}
		
		return $this->request;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param IRequestHandlerFactory 
	 * @param IErrorHandlerFactory 
	 * @param ILogger 
	 */
	public function __construct(IRequestHandlerFactory $requestHandlerFactory,
			IErrorHandlerFactory $errorHandlerFactory = NULL, ILogger $logger = NULL) {
		$this->requestHandlerFactory = $requestHandlerFactory;
		$this->errorHandlerFactory = $errorHandlerFactory;
		$this->logger = $logger;
		
		$this->environmentVariableCache = [];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws Exception .
	 */
	public function run() {
		$request = $this->getRequest();
		$context = new Context();
		
		if ($this->requestIsRedirected($httpStatusCode) && $httpStatusCode != StatusCode::OK) {
			$requestHandler = $this->createErrorHandler();
			
			if (!HttpUtils::statusCodeIndicatesError($httpStatusCode)) {
				$httpStatusCode = StatusCode::INTERNAL_SERVER_ERROR;
			}
			
			$context->setHttpStatusCode($httpStatusCode);
		} else {
			$headers = $request->getHeaders();
		
			$path = self::extractPathFromRequestUri($request->getUri());
			$method = $request->getMethod();
			$host = $headers->containsKey(FieldName::HOST) ? $headers[FieldName::HOST]->getFieldValue() : '';
			
			try {
				$requestHandler = $this->requestHandlerFactory->createHandler($path, $method, $host);
			} catch (ArgumentException $exception) {
				$message = 'Caught exception with message "%s" while creating an IRequestHandler instance ' .
						'for path = "%s", method = "%s" and host = "%s".';
				$this->log(Severity::INFO, $message, $exception->getMessage(), $path, $method, $host);
				$requestHandler = $this->createErrorHandler();
				$context->setHttpStatusCode(StatusCode::NOT_FOUND);
			}
		}
		
		$response = $requestHandler->handle($request, $context);
		
		if (http_response_code($response->getStatusCode()) === false) {
			throw new Exception('Failed to set the HTTP response status code.');
		}
		
		foreach ($response->getHeaders() as $header) {
			$replace = false;
			header($header->toString(), $replace);
		}
		
		$body = $response->getBody();
		if ($body !== NULL) {
			echo $body;
		}
	}
	
	private function createErrorHandler() {
		if ($this->errorHandlerFactory == NULL) {
			throw new InvalidOperationException('The IErrorHandlerFactory pointer is NULL.');
		}
		
		return $this->errorHandlerFactory->createErrorHandler();
	}
	
	private function createRequest() {
		$requestIsRedirected = $this->requestIsRedirected();
		$method = $requestIsRedirected ? $this->getEnvironmentVariable('REDIRECT_REQUEST_METHOD') : NULL;
		if ($method === NULL) {
			$method = $this->getEnvironmentVariable('REQUEST_METHOD');
		}
	
		$request = new Request($method, $this->getEnvironmentVariable('REQUEST_URI'));
		
		$headers = $request->getHeaders();
		$headerFactory = self::getHttpHeaderFactory();
		
		foreach (apache_request_headers() as $fieldName => $fieldValue) {
			$ignoreCase = true;
			$constantName = FieldName::getName($fieldName, $ignoreCase);
			if ($constantName !== NULL) {
				$fieldName = FieldName::parse($constantName);
			}

			try {
				$header = $headerFactory->createHeader($fieldName, $fieldValue);
			} catch (ArgumentException $exception) {
				$message = 'Caught exception with message "%s" while creating a Http\\Header\\Header ' .
						'instance for field name = "%s" and field value = "%s".';
				$this->log(Severity::INFO, $message, $exception->getMessage(), $fieldName, $fieldValue);
				$header = NULL;
			}
			
			if ($header != NULL) {
				$headers->addAt($header->getFieldName(), $header);
			}
		}
		
		$body = file_get_contents('php://input');
		if ($body === false) {
			throw new IOException('Failed to read the HTTP request body.');
		}
		
		$request->setBody($body);
		
		return $request;
	}
	
	private function requestIsRedirected(&$httpStatusCode = NULL) {
		$redirectStatus = $this->getEnvironmentVariable('REDIRECT_STATUS');
		$httpStatusCode = (int) $redirectStatus;
		return $redirectStatus !== NULL;
	}
	
	private function getEnvironmentVariable($variable) {
		if (!array_key_exists($variable, $this->environmentVariableCache)) {
			$this->environmentVariableCache[$variable] = Environment::getEnvironmentVariable($variable);
		}
		
		return $this->environmentVariableCache[$variable];
	}
	
	private function log($severity, $message, ...$parameters) {
		if ($this->logger == NULL) {
			return;
		}
		
		$this->logger->log($severity, $message, ...$parameters);
	}
	
	private static function extractPathFromRequestUri($requestUri) {
		$requestUriParts = explode('?', $requestUri, 2);
		return rawurldecode($requestUriParts[0]);
	}
	
	private static function getHttpHeaderFactory() {
		static $httpHeaderFactory = NULL;
		if ($httpHeaderFactory == NULL) {
			$httpHeaderFactory = new Factory();
		}
		
		return $httpHeaderFactory;
	}

}

?>
