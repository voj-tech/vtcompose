<?php

namespace VTCompose\Text\RegularExpressions;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class RegexGroup extends RegexCapture {

	private $captures;

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function __construct($captures) {
		if (is_array($captures)) {
			$captures = new ArrayList($captures);
		}
	
		$lastCapture = $captures->lastOrNull();
		if ($lastCapture != NULL) {
			$value = $lastCapture->getValue();
			$index = $lastCapture->getIndex();
		} else {
			$value = '';
			$index = 0;
		}
		
		parent::__construct($value, $index);
		$this->captures = $captures;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 * @todo When there are read-only versions of collections keep the captures collection as a member
	 * read-only variable.
	 */
	public function getCaptures() {
		return clone $this->captures;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isSuccess() {
		return $this->captures->count() > 0;
	}

}

?>
