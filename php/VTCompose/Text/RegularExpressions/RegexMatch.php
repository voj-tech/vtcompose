<?php

namespace VTCompose\Text\RegularExpressions;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class RegexMatch extends RegexGroup {

	private $groups;

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 */
	public function __construct($groups) {
		if (is_array($groups)) {
			$groups = new ArrayList($groups);
		}

		$firstGroup = $groups->firstOrNull();
		parent::__construct($firstGroup != NULL ? $firstGroup->getCaptures() : new ArrayList());
		$this->groups = $groups;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 * @todo When there are read-only versions of collections keep the groups collection as a member read-only
	 * variable.
	 */
	public function getGroups() {
		return clone $this->groups;
	}

}

?>
