<?php

namespace VTCompose\Text\RegularExpressions;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class RegexOptions extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const NONE = 0x00;

	/**
	 * 
	 *
	 * 
	 */
	const IGNORE_CASE = 0x01;
	
	/**
	 * 
	 *
	 * 
	 */
	const MULTILINE = 0x02;
	
	/**
	 * 
	 *
	 * 
	 */
	const SINGLELINE = 0x04;
	
	/**
	 * 
	 *
	 * 
	 */
	const IGNORE_PATTERN_WHITESPACE = 0x08;

}

?>
