<?php

namespace VTCompose\Text\RegularExpressions;

/**
 * 
 *
 * 
 */
class RegexCapture {

	private $value;
	private $index;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 */
	public function __construct($value, $index) {
		$this->value = $value;
		$this->index = $index;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getValue() {
		return $this->value;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getIndex() {
		return $this->index;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function toString() {
		return $this->getValue();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function __toString() {
		return $this->toString();
	}

}

?>
