<?php

namespace VTCompose\Text\RegularExpressions;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 */
class Regex {

	private static $delimiter = '/';

	private $pattern;
	private $options;

	private $internalPattern;

	private function getInternalPattern() {
		if (!isset($this->internalPattern)) {
			$escapedPattern = addcslashes($this->pattern, self::$delimiter);
			$this->internalPattern = self::$delimiter . $escapedPattern . self::$delimiter;

			static $modifiers = [
				RegexOptions::IGNORE_CASE				=> 'i',
				RegexOptions::MULTILINE					=> 'm',
				RegexOptions::SINGLELINE				=> 's',
				RegexOptions::IGNORE_PATTERN_WHITESPACE	=> 'x',
			];

			foreach ($modifiers as $option => $modifier) {
				if ($this->options & $option) {
					$this->internalPattern .= $modifier;
				}
			}

			$this->internalPattern .= 'u';
		}

		return $this->internalPattern;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 */
	public function __construct($pattern, $options = RegexOptions::NONE) {
		$this->pattern = $pattern;
		$this->options = $options;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function toString() {
		return $this->pattern;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function __toString() {
		return $this->toString();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getOptions() {
		return $this->options;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 * @return RegexMatch 
	 * @throws Exception .
	 */
	public function match($input, $offset = 0) {
		$numMatches = preg_match($this->getInternalPattern(), $input, $matches, PREG_OFFSET_CAPTURE, $offset);
		if ($numMatches === false) {
			throw new Exception('An error occurred performing a regular expression match.');
		}

		$groups = [];
		foreach ($matches as $match) {
			$groups[] = new RegexGroup([new RegexCapture($match[0], $match[1])]);
		}
		
		return new RegexMatch($groups);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 * @return IList 
	 * @throws Exception .
	 */
	public function matches($input, $offset = 0) {
		$flags = PREG_OFFSET_CAPTURE | PREG_SET_ORDER;
		$numMatches = preg_match_all($this->getInternalPattern(), $input, $globalMatches, $flags, $offset);
		if ($numMatches === false) {
			throw new Exception('An error occurred performing a global regular expression match.');
		}

		$result = new ArrayList();
		foreach ($globalMatches as $matches) {
			$groups = [];
			foreach ($matches as $match) {
				$groups[] = new RegexGroup([new RegexCapture($match[0], $match[1])]);
			}
			$result->add(new RegexMatch($groups));
		}
		
		return $result;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param int 
	 * @return bool 
	 */
	public function isMatch($input, $offset = 0) {
		return $this->match($input, $offset)->isSuccess();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param int 
	 * @return string 
	 * @throws Exception .
	 */
	public function replace($input, $replacement, $count = -1) {
		$result = preg_replace($this->getInternalPattern(), $replacement, $input, $count);
		if ($result === NULL) {
			throw new Exception('An error occurred performing a regular expression search and replace.');
		}
		
		return $result;
	}

}

?>
