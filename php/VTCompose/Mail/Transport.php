<?php

namespace VTCompose\Mail;

use IteratorAggregate;
use VTCompose\StringUtilities as StringUtils;

/**
 * 
 *
 * 
 */
class Transport implements ITransport {

	private static function mimeHeaderEncode($string) {
		return '=?UTF-8?Q?' . quoted_printable_encode($string) . '?=';
	}

	private static function formatMailbox(Mailbox $mailbox) {
		if (StringUtils::length($mailbox->getDisplayName()) > 0) {
			$encodedDisplayName = self::mimeHeaderEncode($mailbox->getDisplayName());
			return sprintf('%s <%s>', $encodedDisplayName, $mailbox->getAddress());
		} else {
			return $mailbox->getAddress();
		}
	}

	private static function formatMailboxes(IteratorAggregate $mailboxes) {
		$result = '';
		foreach ($mailboxes as $mailbox) {
			$result .= self::formatMailbox($mailbox) . ', ';
		}
		return StringUtils::length($result) > 0 ? StringUtils::substring($result, 0, -2) : '';
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param Message 
	 * @throws TransportException .
	 */
	public function sendMessage(Message $message) {
		$toString = self::formatMailboxes($message->getToMailboxes());
		$subject = $message->getSubject();
		$body = $message->getBody();
		$additionalHeaders =
			'MIME-Version: 1.0' . StringUtils::CRLF .
			'Content-Type: ' . $message->getContentType() . '; charset=UTF-8' . StringUtils::CRLF .
			'Content-Transfer-Encoding: 8bit' . StringUtils::CRLF;
		
		$fromMailboxes = $message->getFromMailboxes();
		
		$envelopeSender = $fromMailboxes->firstOrNull();
		if ($envelopeSender != NULL) {
			$additionalHeaders .= 'Return-Path: ' . $envelopeSender->getAddress() . StringUtils::CRLF;
			$additionalParameters = '-f' . $envelopeSender->getAddress();
		} else {
			$additionalParameters = '';
		}
		
		$additionalHeaders .= 'From: ' . self::formatMailboxes($fromMailboxes) . StringUtils::CRLF;

		if (!mail($toString, $subject, $body, $additionalHeaders, $additionalParameters)) {
			throw new TransportException('Unable to send an e-mail.');
		}
	}

}

?>
