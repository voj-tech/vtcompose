<?php

namespace VTCompose\Mail;

/**
 * 
 *
 * 
 */
class Mailbox {

	private $address;
	private $displayName;

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 */
	public function __construct($address = '', $displayName = '') {
		$this->address = $address;
		$this->displayName = $displayName;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setAddress($address) {
		$this->address = $address;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getAddress() {
		return $this->address;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setDisplayName($displayName) {
		$this->displayName = $displayName;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getDisplayName() {
		return $this->displayName;
	}

}

?>
