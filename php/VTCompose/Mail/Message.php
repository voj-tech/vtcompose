<?php

namespace VTCompose\Mail;

use VTCompose\Collection\ArrayList;
use VTCompose\Collection\IList;

/**
 * 
 *
 * 
 *
 * @todo Implement multipart MIME messages.
 */
class Message {

	private $fromMailboxes;
	private $toMailboxes;
	private $subject;
	private $body;
	private $contentType;

	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 * @param IList 
	 * @param string 
	 * @param string 
	 * @param string 
	 */
	public function __construct(IList $fromMailboxes = NULL, IList $toMailboxes = NULL,
			$subject = '', $body = '', $contentType = 'text/plain') {
		$this->fromMailboxes = $fromMailboxes != NULL ? $fromMailboxes : new ArrayList();
		$this->toMailboxes = $toMailboxes != NULL ? $toMailboxes : new ArrayList();
		$this->subject = $subject;
		$this->body = $body;
		$this->contentType = $contentType;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setFromMailboxes(IList $fromMailboxes) {
		$this->fromMailboxes = $fromMailboxes;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getFromMailboxes() {
		return $this->fromMailboxes;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param IList 
	 */
	public function setToMailboxes(IList $toMailboxes) {
		$this->toMailboxes = $toMailboxes;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return IList 
	 */
	public function getToMailboxes() {
		return $this->toMailboxes;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setSubject($subject) {
		$this->subject = $subject;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getSubject() {
		return $this->subject;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setBody($body) {
		$this->body = $body;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getBody() {
		return $this->body;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setContentType($contentType) {
		$this->contentType = $contentType;
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getContentType() {
		return $this->contentType;
	}

}

?>
