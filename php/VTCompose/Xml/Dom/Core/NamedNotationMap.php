<?php

namespace VTCompose\Xml\Dom\Core;

use DOMNamedNodeMap;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class NamedNotationMap extends NamedNodeMap {

	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DocumentType $documentType,
			DOMNamedNodeMap $domNamedNodeMap) {
		if ($documentType->getNotations() != NULL) {
			throw new InvalidOperationException('The DocumentType already has a NamedNotationMap.');
		}
		
		parent::__construct($implementation, $domNamedNodeMap);
	}

}

?>
