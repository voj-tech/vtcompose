<?php

namespace VTCompose\Xml\Dom\Core;

use DOMDocument;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class Document extends Node {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return '#document';
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws InvalidOperationException .
	 */
	public function setNodeValue($nodeValue) {
		throw new InvalidOperationException('Document node is not supposed to have a value.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::DOCUMENT;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getParentNode() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Document 
	 */
	public function getOwnerDocument() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setTextContent($textContent) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTextContent() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return DocumentType 
	 */
	public function getDocumentType() {
		return $this->wrapDomNode($this->domNode->doctype);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Implementation 
	 */
	public function getImplementation() {
		return parent::getImplementation();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Element 
	 */
	public function getDocumentElement() {
		return $this->wrapDomNode($this->domNode->documentElement);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getXmlEncoding() {
		return $this->domNode->xmlEncoding;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 */
	public function setXmlStandalone($xmlStandalone) {
		$this->domNode->xmlStandalone = $xmlStandalone;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isXmlStandalone() {
		return $this->domNode->xmlStandalone;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setXmlVersion($xmlVersion) {
		$this->domNode->xmlVersion = $xmlVersion;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getXmlVersion() {
		return $this->domNode->xmlVersion;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 */
	public function setStrictErrorChecking($strictErrorChecking) {
		$this->domNode->strictErrorChecking = $strictErrorChecking;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function hasStrictErrorChecking() {
		return $this->domNode->strictErrorChecking;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setDocumentUri($documentUri) {
		$this->domNode->documentURI = $documentUri;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getDocumentUri() {
		return $this->domNode->documentURI;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 */
	public function setPreserveWhitespace($preserveWhitespace) {
		$this->domNode->preserveWhiteSpace = $preserveWhitespace;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function preservesWhitespace() {
		return $this->domNode->preserveWhiteSpace;
	}

	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMDocument $domDocument) {
		parent::__construct($implementation, $domDocument);
		$this->domNode->encoding = 'UTF-8';
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Element 
	 * @throws Exception .
	 */
	public function createElement($tagName) {
		$domElement = $this->domNode->createElement($tagName);
		if (!$domElement) {
			throw new Exception('Unable to create a DOMElement.');
		}
		
		return $this->wrapDomNode($domElement);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return Element 
	 * @throws Exception .
	 */
	public function createElementNS($namespaceUri, $qualifiedName) {
		$domElement = $this->domNode->createElementNS($namespaceUri, $qualifiedName);
		if (!$domElement) {
			throw new Exception('Unable to create a DOMElement with an associated namespace.');
		}
		
		return $this->wrapDomNode($domElement);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return DocumentFragment 
	 * @throws Exception .
	 */
	public function createDocumentFragment() {
		$domDocumentFragment = $this->domNode->createDocumentFragment();
		if (!$domDocumentFragment) {
			throw new Exception('Unable to create a DOMDocumentFragment.');
		}
		
		return $this->wrapDomNode($domDocumentFragment);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Text 
	 * @throws Exception .
	 */
	public function createTextNode($data) {
		$domText = $this->domNode->createTextNode($data);
		if (!$domText) {
			throw new Exception('Unable to create a DOMText.');
		}
		
		return $this->wrapDomNode($domText);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Comment 
	 * @throws Exception .
	 */
	public function createComment($data) {
		$domComment = $this->domNode->createComment($data);
		if (!$domComment) {
			throw new Exception('Unable to create a DOMComment.');
		}
		
		return $this->wrapDomNode($domComment);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return CDataSection 
	 * @throws Exception .
	 */
	public function createCDataSection($data) {
		$domCDataSection = $this->domNode->createCDATASection($data);
		if (!$domCDataSection) {
			throw new Exception('Unable to create a DOMCdataSection.');
		}
		
		return $this->wrapDomNode($domCDataSection);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return ProcessingInstruction 
	 * @throws Exception .
	 */
	public function createProcessingInstruction($target, $data) {
		$domProcessingInstruction = $this->domNode->createProcessingInstruction($target, $data);
		if (!$domProcessingInstruction) {
			throw new Exception('Unable to create a DOMProcessingInstruction.');
		}
		
		return $this->wrapDomNode($domProcessingInstruction);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Attribute 
	 * @throws Exception .
	 */
	public function createAttribute($name) {
		$domAttribute = $this->domNode->createAttribute($name);
		if (!$domAttribute) {
			throw new Exception('Unable to create a DOMAttr.');
		}
		
		return $this->wrapDomNode($domAttribute);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return Attribute 
	 * @throws Exception .
	 */
	public function createAttributeNS($namespaceUri, $qualifiedName) {
		$domAttribute = $this->domNode->createAttributeNS($namespaceUri, $qualifiedName);
		if (!$domAttribute) {
			throw new Exception('Unable to create a DOMAttr with an associated namespace.');
		}
		
		return $this->wrapDomNode($domAttribute);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return EntityReference 
	 * @throws Exception .
	 */
	public function createEntityReference($name) {
		$domEntityReference = $this->domNode->createEntityReference($name);
		if (!$domEntityReference) {
			throw new Exception('Unable to create a DOMEntityReference.');
		}
		
		return $this->wrapDomNode($domEntityReference);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return NodeList 
	 */
	public function getElementsByTagName($tagName) {
		return new NodeList($this->getImplementation(), $this->domNode->getElementsByTagName($tagName));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return NodeList 
	 */
	public function getElementsByTagNameNS($namespaceUri, $localName) {
		$domNodeList = $this->domNode->getElementsByTagNameNS($namespaceUri, $localName);
		return new NodeList($this->getImplementation(), $domNodeList);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @param bool 
	 * @return Node 
	 * @throws Exception .
	 * @todo Implement from scratch instead of calling the underlying DOM implementation.
	 */
	public function importNode(Node $importedNode, $deep) {
		$domNode = $this->domNode->importNode($importedNode->domNode, $deep);
		if (!$domNode) {
			throw new Exception('Unable to import a DOMNode.');
		}
		
		return $this->wrapDomNode($domNode);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Element 
	 */
	public function getElementById($elementId) {
		return $this->wrapDomNode($this->domNode->getElementById($elementId));
	}
	
	/**
	 * 
	 *
	 * 
	 */
	public function normalizeDocument() {
		$this->domNode->normalizeDocument();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws Exception .
	 */
	public function save($filename) {
		if ($this->domNode->save($filename) === false) {
			throw new Exception('Unable to save a DOMDocument.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws Exception .
	 */
	public function load($filename) {
		if (!$this->domNode->load($filename)) {
			throw new Exception('Unable to load a DOMDocument.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws Exception .
	 */
	public function loadXml($xml) {
		if (!$this->domNode->loadXML($xml)) {
			throw new Exception('Unable to load a DOMDocument from a string.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws Exception .
	 */
	public function validate() {
		if (!$this->domNode->validate()) {
			throw new Exception('The document is not valid.');
		}
	}

}

?>
