<?php

namespace VTCompose\Xml\Dom\Core;

use DOMText;

/**
 * 
 *
 * 
 */
class Text extends CharacterData {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return '#text';
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::TEXT;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getWholeText() {
		return $this->domNode->wholeText;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMText $domText) {
		parent::__construct($implementation, $domText);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return Text 
	 * @throws Exception .
	 */
	public function splitText($offset) {
		$domText = $this->domNode->splitText($offset);
		if (!$domText) {
			throw new Exception('Unable to split a DOMText.');
		}
		
		return $this->wrapDomNode($domText);
	}

}

?>
