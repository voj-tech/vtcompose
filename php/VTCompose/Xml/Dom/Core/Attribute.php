<?php

namespace VTCompose\Xml\Dom\Core;

use DOMAttr;
use VTCompose\Exception\NotImplementedException;

/**
 * 
 *
 * 
 */
class Attribute extends Node {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return $this->getName();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setNodeValue($nodeValue) {
		$this->setValue($nodeValue);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return $this->getValue();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::ATTRIBUTE;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getParentNode() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getPreviousSibling() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getNextSibling() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return $this->domNode->name;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 * @todo Implement.
	 */
	public function isSpecified() {
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setValue($value) {
		$this->domNode->value = $value;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getValue() {
		return $this->domNode->value;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Element 
	 */
	public function getOwnerElement() {
		return $this->wrapDomNode($this->domNode->ownerElement);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function isId() {
		return $this->domNode->isId();
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMAttr $domAttribute) {
		parent::__construct($implementation, $domAttribute);
	}

}

?>
