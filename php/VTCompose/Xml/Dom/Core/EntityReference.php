<?php

namespace VTCompose\Xml\Dom\Core;

use DOMEntityReference;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class EntityReference extends Node {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws InvalidOperationException .
	 */
	public function setNodeValue($nodeValue) {
		throw new InvalidOperationException('EntityReference node is not supposed to have a value.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::ENTITY_REFERENCE;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMEntityReference $domEntityReference) {
		parent::__construct($implementation, $domEntityReference);
	}

}

?>
