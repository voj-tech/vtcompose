<?php

namespace VTCompose\Xml\Dom\Core;

use DOMNotation;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class Notation extends Node {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws InvalidOperationException .
	 */
	public function setNodeValue($nodeValue) {
		throw new InvalidOperationException('Notation node is not supposed to have a value.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::NOTATION;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getParentNode() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setTextContent($textContent) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTextContent() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPublicId() {
		return $this->domNode->publicId;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getSystemId() {
		return $this->domNode->systemId;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMNotation $domNotation) {
		parent::__construct($implementation, $domNotation);
	}

}

?>
