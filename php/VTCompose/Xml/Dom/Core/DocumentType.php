<?php

namespace VTCompose\Xml\Dom\Core;

use DOMDocumentType;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class DocumentType extends Node {

	private $entities;
	private $notations;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return $this->getName();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws InvalidOperationException .
	 */
	public function setNodeValue($nodeValue) {
		throw new InvalidOperationException('DocumentType node is not supposed to have a value.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::DOCUMENT_TYPE;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setTextContent($textContent) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTextContent() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getName() {
		return $this->domNode->name;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return NamedNodeMap 
	 */
	public function getEntities() {
		return $this->entities;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return NamedNodeMap 
	 */
	public function getNotations() {
		return $this->notations;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPublicId() {
		return $this->domNode->publicId;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getSystemId() {
		return $this->domNode->systemId;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getInternalSubset() {
		return $this->domNode->internalSubset;
	}

	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMDocumentType $domDocumentType) {
		parent::__construct($implementation, $domDocumentType);
		$this->entities = new NamedEntityMap($implementation, $this, $domDocumentType->entities);
		$this->notations = new NamedNotationMap($implementation, $this, $domDocumentType->notations);
	}

}

?>
