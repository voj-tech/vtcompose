<?php

namespace VTCompose\Xml\Dom\Core;

use Closure;
use DOMNode;
use Traversable;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\IEnumerable;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
abstract class Node implements IEnumerable {

	use Enumerable;
	
	protected $domNode;

	private $childNodes;
	private $implementation;
	
	private $accessor;
	private $implementationAccessor;
	
	protected function wrapDomNode(DOMNode $domNode = NULL) {
		return $domNode != NULL ? $this->implementationAccessor->__invoke('getNode', $domNode) : NULL;
	}
	
	protected function getDomNode() {
		return $this->domNode;
	}
	
	/**
	 * @internal
	 */
	public function getImplementation() {
		return $this->implementation;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return $this->domNode->nodeName;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setNodeValue($nodeValue) {
		$this->domNode->nodeValue = $nodeValue;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return $this->domNode->nodeValue;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return $this->domNode->nodeType;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getParentNode() {
		return $this->wrapDomNode($this->domNode->parentNode);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return NodeList 
	 */
	public function getChildNodes() {
		return $this->childNodes;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getFirstChild() {
		return $this->wrapDomNode($this->domNode->firstChild);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getLastChild() {
		return $this->wrapDomNode($this->domNode->lastChild);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getPreviousSibling() {
		return $this->wrapDomNode($this->domNode->previousSibling);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getNextSibling() {
		return $this->wrapDomNode($this->domNode->nextSibling);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return NamedNodeMap 
	 */
	public function getAttributes() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Document 
	 * @todo Keep the ownerDocument Document object as a member variable and return it here instead of
	 * wrapping the underlying DOMDocument.
	 */
	public function getOwnerDocument() {
		return $this->wrapDomNode($this->domNode->ownerDocument);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return $this->domNode->namespaceURI;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
		$this->domNode->prefix = $prefix;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return $this->domNode->prefix;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return $this->domNode->localName;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getBaseUri() {
		return $this->domNode->baseURI;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setTextContent($textContent) {
		$this->domNode->textContent = $textContent;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTextContent() {
		return $this->domNode->textContent;
	}
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}
	
	private function setImplementationAccessor(Closure $implementationAccessor) {
		$this->implementationAccessor = $implementationAccessor;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMNode $domNode) {
		$implementation->establishFriendshipWithNode($this, $this->getAccessor());
		
		$this->domNode = $domNode;
		
		$this->childNodes = new ChildNodeList($implementation, $this, $domNode->childNodes);
		$this->implementation = $implementation;
		
		$this->implementationAccessor->__invoke('registerDomNode', $domNode, $this);
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @param Node 
	 * @return Node 
	 * @throws Exception .
	 */
	public function insertBefore(Node $newChild, Node $referenceChild = NULL) {
		$referenceChildDomNode = $referenceChild != NULL ? $referenceChild->domNode : NULL;
		$domNode = $this->domNode->insertBefore($newChild->domNode, $referenceChildDomNode);
		if (!$domNode) {
			throw new Exception('Unable to insert a child DOMNode before a reference child DOMNode.');
		}
		
		return $this->wrapDomNode($domNode);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @param Node 
	 * @return Node 
	 * @throws Exception .
	 */
	public function replaceChild(Node $newChild, Node $oldChild) {
		$domNode = $this->domNode->replaceChild($newChild->domNode, $oldChild->domNode);
		if (!$domNode) {
			throw new Exception('Unable to replace a child DOMNode.');
		}
		
		return $this->wrapDomNode($domNode);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @return Node 
	 * @throws Exception .
	 */
	public function removeChild(Node $oldChild) {
		$domNode = $this->domNode->removeChild($oldChild->domNode);
		if (!$domNode) {
			throw new Exception('Unable to remove a child DOMNode.');
		}
		
		return $this->wrapDomNode($domNode);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @return Node 
	 * @throws Exception .
	 */
	public function appendChild(Node $newChild) {
		$domNode = $this->domNode->appendChild($newChild->domNode);
		if (!$domNode) {
			throw new Exception('Unable to append a child DOMNode.');
		}
		
		return $this->wrapDomNode($domNode);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function hasChildNodes() {
		return $this->domNode->hasChildNodes();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param bool 
	 * @return Node 
	 * @throws Exception .
	 * @todo Implement from scratch instead of calling the underlying DOM implementation.
	 */
	public function cloneNode($deep) {
		$domNode = $this->domNode->cloneNode($deep);
		if (!$domNode) {
			throw new Exception('Unable to clone a DOMNode.');
		}
		
		return $this->wrapDomNode($domNode);
	}
	
	/**
	 * 
	 *
	 * 
	 */
	public function normalize() {
		$this->domNode->normalize();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return bool 
	 */
	public function isSupported($feature, $version) {
		return $this->implementation->hasFeature($feature, $version);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function hasAttributes() {
		return false;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @return bool 
	 */
	public function isSameNode(Node $otherNode) {
		return $this->domNode->isSameNode($otherNode->domNode);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 */
	public function lookupPrefix($namespaceUri) {
		return $this->domNode->lookupPrefix($namespaceUri);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function isDefaultNamespace($namespaceUri) {
		return $this->domNode->isDefaultNamespace($namespaceUri);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 */
	public function lookupNamespaceUri($prefix) {
		return $this->domNode->lookupNamespaceURI($prefix);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function first() {
		return $this->childNodes->first();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function firstOrNull() {
		return $this->childNodes->firstOrNull();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function last() {
		return $this->childNodes->last();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function lastOrNull() {
		return $this->childNodes->lastOrNull();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function single() {
		return $this->childNodes->single();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function singleOrNull() {
		return $this->childNodes->singleOrNull();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		return $this->childNodes->getIterator();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->childNodes->count();
	}

}

?>
