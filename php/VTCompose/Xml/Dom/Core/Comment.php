<?php

namespace VTCompose\Xml\Dom\Core;

use DOMComment;

/**
 * 
 *
 * 
 */
class Comment extends CharacterData {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return '#comment';
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::COMMENT;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMComment $domComment) {
		parent::__construct($implementation, $domComment);
	}

}

?>
