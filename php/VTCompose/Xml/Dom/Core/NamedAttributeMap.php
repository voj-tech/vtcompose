<?php

namespace VTCompose\Xml\Dom\Core;

use DOMNamedNodeMap;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class NamedAttributeMap extends NamedNodeMap {

	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, Element $element,
			DOMNamedNodeMap $domNamedNodeMap) {
		if ($element->getAttributes() != NULL) {
			throw new InvalidOperationException('The Element already has a NamedAttributeMap.');
		}
		
		parent::__construct($implementation, $domNamedNodeMap);
	}

}

?>
