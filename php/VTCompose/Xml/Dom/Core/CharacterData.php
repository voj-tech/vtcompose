<?php

namespace VTCompose\Xml\Dom\Core;

/**
 * 
 *
 * 
 */
abstract class CharacterData extends Node {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setNodeValue($nodeValue) {
		$this->setData($nodeValue);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return $this->getData();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setTextContent($textContent) {
		$this->setData($textContent);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTextContent() {
		return $this->getData();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setData($data) {
		$this->domNode->data = $data;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getData() {
		return $this->domNode->data;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getLength() {
		return $this->domNode->length;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param int 
	 * @return string 
	 */
	public function substringData($offset, $count) {
		return $this->domNode->substringData($offset, $count);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function appendData($data) {
		$this->domNode->appendData($data);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param string 
	 */
	public function insertData($offset, $data) {
		$this->domNode->insertData($offset, $data);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param int 
	 */
	public function deleteData($offset, $count) {
		$this->domNode->deleteData($offset, $count);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @param int 
	 * @param string 
	 */
	public function replaceData($offset, $count, $data) {
		$this->domNode->replaceData($offset, $count, $data);
	}

}

?>
