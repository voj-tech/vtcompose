<?php

namespace VTCompose\Xml\Dom\Core;

use DOMNodeList;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class ChildNodeList extends NodeList {
	
	private $parentNode;
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, Node $parentNode,
			DOMNodeList $domNodeList = NULL) {
		if ($parentNode->getChildNodes() != NULL) {
			throw new InvalidOperationException('The Node already has a ChildNodeList.');
		}
		
		parent::__construct($implementation, $domNodeList);
		$this->parentNode = $parentNode;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function firstOrNull() {
		return $this->parentNode->getFirstChild();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function lastOrNull() {
		return $this->parentNode->getLastChild();
	}

}

?>
