<?php

namespace VTCompose\Xml\Dom\Core;

use DOMNamedNodeMap;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class NamedEntityMap extends NamedNodeMap {

	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DocumentType $documentType,
			DOMNamedNodeMap $domNamedNodeMap) {
		if ($documentType->getEntities() != NULL) {
			throw new InvalidOperationException('The DocumentType already has a NamedEntityMap.');
		}
		
		parent::__construct($implementation, $domNamedNodeMap);
	}

}

?>
