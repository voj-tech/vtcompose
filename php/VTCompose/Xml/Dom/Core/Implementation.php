<?php

namespace VTCompose\Xml\Dom\Core;

use Closure;
use DOMImplementation;
use DOMNode;
use VTCompose\Collection\Dictionary;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotSupportedException;
use VTCompose\StringUtilities as StringUtils;
use VTCompose\Xml\Xsl\Transform;

/**
 * 
 *
 * 
 */
class Implementation {

	private $domImplementation;
	private $nodeRegistry;
	
	private $accessor;
	private $nodeAccessors;
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}

	/**
	 * 
	 *
	 * 
	 */
	public function __construct() {
		Transform::registerDomImplementation($this, $this->getAccessor());
	
		$this->domImplementation = new DOMImplementation();
		$this->nodeRegistry = new Dictionary();
		
		$this->nodeAccessors = new Dictionary();
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return bool 
	 */
	public function hasFeature($feature, $version) {
		switch (StringUtils::toLower($feature)) {
		case 'core':
			return $version == '1.0';
			
		case 'xml':
			return $version == '1.0' || $version == '2.0';
			
		default:
			return false;
		}
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param string 
	 * @return DocumentType 
	 * @throws Exception .
	 */
	public function createDocumentType($qualifiedName, $publicId, $systemId) {
		$domDocumentType = $this->domImplementation->createDocumentType($qualifiedName, $publicId, $systemId);
		if (!$domDocumentType) {
			throw new Exception('Unable to create a DOMDocumentType.');
		}
		
		return $this->getNode($domDocumentType);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param DocumentType 
	 * @return Document 
	 * @throws ArgumentException .
	 * @throws Exception .
	 */
	public function createDocument($namespaceUri, $qualifiedName, DocumentType $documentType = NULL) {
		if ($documentType != NULL) {
			$domDocument = $this->domImplementation->createDocument($namespaceUri, $qualifiedName,
					$this->getDomNode($documentType));
		} else {
			$domDocument = $this->domImplementation->createDocument($namespaceUri, $qualifiedName);
		}
		
		if (!$domDocument) {
			throw new Exception('Unable to create a DOMDocument.');
		}
		
		return $this->getNode($domDocument);
	}
	
	private function registerDomNode(DOMNode $domNode, Node $node) {
		if ($this->nodeRegistry->containsKey($domNode)) {
			throw new ArgumentException('The DOMNode is already registered.');
		}
		
		$this->nodeRegistry->addAt($domNode, $node);
	}
	
	private function getNode(DOMNode $domNode) {
		if ($this->nodeRegistry->containsKey($domNode)) {
			return $this->nodeRegistry[$domNode];
		}
		
		switch ($domNode->nodeType) {
		case XML_ELEMENT_NODE:
			return new Element($this, $domNode);
			
		case XML_ATTRIBUTE_NODE:
			return new Attribute($this, $domNode);
			
		case XML_TEXT_NODE:
			return new Text($this, $domNode);
			
		case XML_CDATA_SECTION_NODE:
			return new CDataSection($this, $domNode);
			
		case XML_ENTITY_REF_NODE:
			return new EntityReference($this, $domNode);
			
		case XML_ENTITY_NODE:
			return new Entity($this, $domNode);
			
		case XML_PI_NODE:
			return new ProcessingInstruction($this, $domNode);
			
		case XML_COMMENT_NODE:
			return new Comment($this, $domNode);
			
		case XML_DOCUMENT_NODE:
			return new Document($this, $domNode);
			
		case XML_DOCUMENT_TYPE_NODE:
			return new DocumentType($this, $domNode);
			
		case XML_DOCUMENT_FRAG_NODE:
			return new DocumentFragment($this, $domNode);
			
		case XML_NOTATION_NODE:
			return new Notation($this, $domNode);
			
		default:
			throw new Exception('DOMNode type is invalid.');
		}
	}
	
	private function getDomNode(Node $node) {
		if ($node->getImplementation() !== $this) {
			throw new ArgumentException('The Node does not belong to this Implementation.');
		}
		
		return $this->nodeAccessors[$node]('getDomNode');
	}
	
	/**
	 * @internal
	 */
	public function establishFriendshipWithNode(Node $node, Closure $nodeAccessor) {
		if ($node->getImplementation() != NULL) {
			throw new InvalidOperationException('The Node is already friend with an Implementation.');
		}
		
		$this->nodeAccessors->addAt($node, $nodeAccessor);
		$nodeAccessor('setImplementationAccessor', $this->getAccessor());
	}
	
	/**
	 * @internal
	 */
	public function establishFriendshipWithNodeList(NodeList $nodeList, Closure $nodeListAccessor) {
		if ($nodeList->getImplementation() != NULL) {
			throw new InvalidOperationException('The NodeList is already friend with an Implementation.');
		}
		
		$nodeListAccessor('setImplementationAccessor', $this->getAccessor());
	}
	
	/**
	 * @internal
	 */
	public function establishFriendshipWithNamedNodeMap(NamedNodeMap $namedNodeMap,
			Closure $namedNodeMapAccessor) {
		if ($namedNodeMap->getImplementation() != NULL) {
			throw new InvalidOperationException('The NamedNodeMap is already friend with an Implementation.');
		}
		
		$namedNodeMapAccessor('setImplementationAccessor', $this->getAccessor());
	}
	
	/**
	 * 
	 *
	 * Calling this method will effectively allow to delete the object from memory.
	 */
	public function unregister() {
		Transform::unregisterDomImplementation($this);
	}

}

?>
