<?php

namespace VTCompose\Xml\Dom\Core;

use DOMProcessingInstruction;

/**
 * 
 *
 * 
 */
class ProcessingInstruction extends Node {

	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return $this->getTarget();
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setNodeValue($nodeValue) {
		$this->setData($nodeValue);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return $this->getData();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::PROCESSING_INSTRUCTION;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setTextContent($textContent) {
		$this->setData($textContent);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTextContent() {
		return $this->getData();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTarget() {
		return $this->domNode->target;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setData($data) {
		$this->domNode->data = $data;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getData() {
		return $this->domNode->data;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation,
			DOMProcessingInstruction $domProcessingInstruction) {
		parent::__construct($implementation, $domProcessingInstruction);
	}

}

?>
