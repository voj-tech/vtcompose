<?php

namespace VTCompose\Xml\Dom\Core;

use Closure;
use DOMNode;
use DOMNodeList;
use Traversable;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\IEnumerable;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class NodeList implements IEnumerable {

	use Enumerable;
	
	private $domNodeList;
	
	private $implementation;
	
	private $accessor;
	private $implementationAccessor;
	
	private function wrapDomNode(DOMNode $domNode = NULL) {
		return $domNode != NULL ? $this->implementationAccessor->__invoke('getNode', $domNode) : NULL;
	}
	
	/**
	 * @internal
	 */
	public function getImplementation() {
		return $this->implementation;
	}
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}
	
	private function setImplementationAccessor(Closure $implementationAccessor) {
		$this->implementationAccessor = $implementationAccessor;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMNodeList $domNodeList = NULL) {
		$implementation->establishFriendshipWithNodeList($this, $this->getAccessor());
		
		$this->domNodeList = $domNodeList;
		
		$this->implementation = $implementation;
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return Node 
	 */
	public function item($index) {
		return $this->domNodeList != NULL ? $this->wrapDomNode($this->domNodeList->item($index)) : NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function first() {
		$node = $this->firstOrNull();
		if ($node == NULL) {
			$this->throwSequenceEmptyException();
		}
		
		return $node;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function firstOrNull() {
		return $this->item(0);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function last() {
		$node = $this->lastOrNull();
		if ($node == NULL) {
			$this->throwSequenceEmptyException();
		}
		
		return $node;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function lastOrNull() {
		$count = $this->count();
		return $count > 0 ? $this->item($count - 1) : NULL;
	}
	
	private function validateEmptyOrSingleElementSequence() {
		if ($this->count() > 1) {
			$this->throwSequenceContainsMoreThanOneElementException();
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function single() {
		$this->validateEmptyOrSingleElementSequence();
		return $this->first();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function singleOrNull() {
		$this->validateEmptyOrSingleElementSequence();
		return $this->firstOrNull();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		if ($this->domNodeList == NULL) {
			return;
		}
		
		foreach ($this->domNodeList as $index => $node) {
			yield $index => $this->wrapDomNode($node);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->domNodeList != NULL ? $this->domNodeList->length : 0;
	}

}

?>
