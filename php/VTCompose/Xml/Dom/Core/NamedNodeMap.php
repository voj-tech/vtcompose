<?php

namespace VTCompose\Xml\Dom\Core;

use Closure;
use DOMNode;
use DOMNamedNodeMap;
use Traversable;
use VTCompose\Collection\Enumerable;
use VTCompose\Collection\IEnumerable;
use VTCompose\Exception\NotImplementedException;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
abstract class NamedNodeMap implements IEnumerable {

	use Enumerable;
	
	private $domNamedNodeMap;
	
	private $implementation;
	
	private $accessor;
	private $implementationAccessor;
	
	private function wrapDomNode(DOMNode $domNode = NULL) {
		return $domNode != NULL ? $this->implementationAccessor->__invoke('getNode', $domNode) : NULL;
	}
	
	/**
	 * @internal
	 */
	public function getImplementation() {
		return $this->implementation;
	}
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}
	
	private function setImplementationAccessor(Closure $implementationAccessor) {
		$this->implementationAccessor = $implementationAccessor;
	}

	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMNamedNodeMap $domNamedNodeMap) {
		$implementation->establishFriendshipWithNamedNodeMap($this, $this->getAccessor());
		
		$this->domNamedNodeMap = $domNamedNodeMap;
		
		$this->implementation = $implementation;
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @return Node 
	 */
	public function setNamedItem(Node $node) {
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Node 
	 * @return Node 
	 */
	public function setNamedItemNS(Node $node) {
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Node 
	 */
	public function getNamedItem($name) {
		return $this->wrapDomNode($this->domNamedNodeMap->getNamedItem($name));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return Node 
	 */
	public function getNamedItemNS($namespaceUri, $localName) {
		return $this->wrapDomNode($this->domNamedNodeMap->getNamedItemNS($namespaceUri, $localName));
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Node 
	 */
	public function removeNamedItem($name) {
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return Node 
	 */
	public function removeNamedItemNS($namespaceUri, $localName) {
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param int 
	 * @return Node 
	 */
	public function item($index) {
		return $this->wrapDomNode($this->domNamedNodeMap->item($index));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function first() {
		$node = $this->firstOrNull();
		if ($node == NULL) {
			$this->throwSequenceEmptyException();
		}
		
		return $node;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function firstOrNull() {
		return $this->item(0);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function last() {
		$node = $this->lastOrNull();
		if ($node == NULL) {
			$this->throwSequenceEmptyException();
		}
		
		return $node;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function lastOrNull() {
		$count = $this->count();
		return $count > 0 ? $this->item($count - 1) : NULL;
	}
	
	private function validateEmptyOrSingleElementSequence() {
		if ($this->count() > 1) {
			$this->throwSequenceContainsMoreThanOneElementException();
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function single() {
		$this->validateEmptyOrSingleElementSequence();
		return $this->first();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return mixed 
	 */
	public function singleOrNull() {
		$this->validateEmptyOrSingleElementSequence();
		return $this->firstOrNull();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Traversable 
	 */
	public function getIterator(): Traversable {
		foreach ($this->domNamedNodeMap as $name => $node) {
			yield $name => $this->wrapDomNode($node);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function count(): int {
		return $this->domNamedNodeMap->length;
	}

}

?>
