<?php

namespace VTCompose\Xml\Dom\Core;

use DOMDocumentFragment;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class DocumentFragment extends Node {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return '#document-fragment';
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws InvalidOperationException .
	 */
	public function setNodeValue($nodeValue) {
		throw new InvalidOperationException('DocumentFragment node is not supposed to have a value.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::DOCUMENT_FRAGMENT;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getParentNode() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMDocumentFragment $domDocumentFragment) {
		parent::__construct($implementation, $domDocumentFragment);
	}

}

?>
