<?php

namespace VTCompose\Xml\Dom\Core;

use DOMEntity;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotImplementedException;

/**
 * 
 *
 * 
 */
class Entity extends Node {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws InvalidOperationException .
	 */
	public function setNodeValue($nodeValue) {
		throw new InvalidOperationException('Entity node is not supposed to have a value.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::ENTITY;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return Node 
	 */
	public function getParentNode() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNamespaceUri() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 */
	public function setPrefix($prefix) {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPrefix() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getLocalName() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getPublicId() {
		return $this->domNode->publicId;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getSystemId() {
		return $this->domNode->systemId;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNotationName() {
		return $this->domNode->notationName;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 * @todo Implement.
	 */
	public function getInputEncoding() {
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 * @todo Implement.
	 */
	public function getXmlEncoding() {
		throw new NotImplementedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 * @todo Implement.
	 */
	public function getXmlVersion() {
		throw new NotImplementedException();
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMEntity $domEntity) {
		parent::__construct($implementation, $domEntity);
	}

}

?>
