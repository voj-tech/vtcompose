<?php

namespace VTCompose\Xml\Dom\Core;

use DOMCdataSection;

/**
 * 
 *
 * 
 */
class CDataSection extends Text {
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return '#cdata-section';
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::CDATA_SECTION;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMCdataSection $domCDataSection) {
		parent::__construct($implementation, $domCDataSection);
	}

}

?>
