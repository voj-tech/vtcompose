<?php

namespace VTCompose\Xml\Dom\Core;

use VTCompose\Enum;

/**
 * 
 *
 * 
 */
final class NodeType extends Enum {

	/**
	 * 
	 *
	 * 
	 */
	const ELEMENT = 1;
	
	/**
	 * 
	 *
	 * 
	 */
	const ATTRIBUTE = 2;
	
	/**
	 * 
	 *
	 * 
	 */
	const TEXT = 3;
	
	/**
	 * 
	 *
	 * 
	 */
	const CDATA_SECTION = 4;
	
	/**
	 * 
	 *
	 * 
	 */
	const ENTITY_REFERENCE = 5;
	
	/**
	 * 
	 *
	 * 
	 */
	const ENTITY = 6;
	
	/**
	 * 
	 *
	 * 
	 */
	const PROCESSING_INSTRUCTION = 7;
	
	/**
	 * 
	 *
	 * 
	 */
	const COMMENT = 8;
	
	/**
	 * 
	 *
	 * 
	 */
	const DOCUMENT = 9;
	
	/**
	 * 
	 *
	 * 
	 */
	const DOCUMENT_TYPE = 10;
	
	/**
	 * 
	 *
	 * 
	 */
	const DOCUMENT_FRAGMENT = 11;
	
	/**
	 * 
	 *
	 * 
	 */
	const NOTATION = 12;

}

?>
