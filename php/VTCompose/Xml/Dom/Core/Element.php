<?php

namespace VTCompose\Xml\Dom\Core;

use DOMElement;
use VTCompose\Exception\InvalidOperationException;

/**
 * 
 *
 * 
 */
class Element extends Node {

	private $attributes;
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeName() {
		return $this->getTagName();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws InvalidOperationException .
	 */
	public function setNodeValue($nodeValue) {
		throw new InvalidOperationException('Element node is not supposed to have a value.');
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getNodeValue() {
		return NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return int 
	 */
	public function getNodeType() {
		return NodeType::ELEMENT;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return NamedNodeMap 
	 */
	public function getAttributes() {
		return $this->attributes;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return string 
	 */
	public function getTagName() {
		return $this->domNode->tagName;
	}
	
	/**
	 * @internal
	 */
	public function __construct(Implementation $implementation, DOMElement $domElement) {
		parent::__construct($implementation, $domElement);
		$this->attributes = new NamedAttributeMap($implementation, $this, $domElement->attributes);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @throws Exception .
	 */
	public function setAttribute($name, $value) {
		if (!$this->domNode->setAttribute($name, $value)) {
			throw new Exception('Unable to set the value of a DOMElement attribute.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param string 
	 * @throws Exception .
	 */
	public function setAttributeNS($namespaceUri, $qualifiedName, $value) {
		if ($this->domNode->setAttributeNS($namespaceUri, $qualifiedName, $value) === false) {
			$message = 'Unable to set the value of a DOMElement attribute with an associated namespace.'; 
			throw new Exception($message);
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return string 
	 */
	public function getAttribute($name) {
		return $this->domNode->getAttribute($name);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return string 
	 */
	public function getAttributeNS($namespaceUri, $localName) {
		return $this->domNode->getAttributeNS($namespaceUri, $localName);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @throws Exception .
	 */
	public function removeAttribute($name) {
		if (!$this->domNode->removeAttribute($name)) {
			throw new Exception('Unable to remove a DOMElement attribute.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @throws Exception .
	 */
	public function removeAttributeNS($namespaceUri, $localName) {
		if ($this->domNode->removeAttributeNS($namespaceUri, $localName) === false) {
			throw new Exception('Unable to remove a DOMElement attribute with an associated namespace.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Attribute 
	 * @return Attribute 
	 * @throws Exception .
	 */
	public function setAttributeNode(Attribute $attribute) {
		$domAttribute = $this->domNode->setAttributeNode($attribute->domNode);
		if ($domAttribute === false) {
			throw new Exception('Unable to add a DOMAttr.');
		}
		
		return $this->wrapDomNode($domAttribute);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Attribute 
	 * @return Attribute 
	 * @throws Exception .
	 */
	public function setAttributeNodeNS(Attribute $attribute) {
		$domAttribute = $this->domNode->setAttributeNodeNS($attribute->domNode);
		if ($domAttribute === false) {
			throw new Exception('Unable to add a DOMAttr with an associated namespace.');
		}
		
		return $this->wrapDomNode($domAttribute);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return Attribute 
	 */
	public function getAttributeNode($name) {
		$domAttribute = $this->domNode->getAttributeNode($name);
		return $domAttribute ? $this->wrapDomNode($domAttribute) : NULL;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return Attribute 
	 */
	public function getAttributeNodeNS($namespaceUri, $localName) {
		return $this->wrapDomNode($this->domNode->getAttributeNodeNS($namespaceUri, $localName));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Attribute 
	 * @return Attribute 
	 * @throws Exception .
	 */
	public function removeAttributeNode(Attribute $attribute) {
		$domAttribute = $this->domNode->removeAttributeNode($attribute->domNode);
		if ($domAttribute === false) {
			throw new Exception('Unable to remove a DOMAttr.');
		}
		
		return $this->wrapDomNode($domAttribute);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return NodeList 
	 */
	public function getElementsByTagName($tagName) {
		return new NodeList($this->getImplementation(), $this->domNode->getElementsByTagName($tagName));
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return NodeList 
	 */
	public function getElementsByTagNameNS($namespaceUri, $localName) {
		$domNodeList = $this->domNode->getElementsByTagNameNS($namespaceUri, $localName);
		return new NodeList($this->getImplementation(), $domNodeList);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function hasAttribute($name) {
		return $this->domNode->hasAttribute($name);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @return bool 
	 */
	public function hasAttributeNS($namespaceUri, $localName) {
		return $this->domNode->hasAttributeNS($namespaceUri, $localName);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param bool 
	 */
	public function setIdAttribute($name, $isId) {
		$this->domNode->setIdAttribute($name, $isId);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param bool 
	 */
	public function setIdAttributeNS($namespaceUri, $localName, $isId) {
		$this->domNode->setIdAttributeNS($namespaceUri, $localName, $isId);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Attribute 
	 * @param bool 
	 */
	public function setIdAttributeNode(Attribute $idAttribute, $isId) {
		$this->domNode->setIdAttributeNode($idAttribute->domNode, $isId);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return bool 
	 */
	public function hasAttributes() {
		return $this->domNode->hasAttributes();
	}

}

?>
