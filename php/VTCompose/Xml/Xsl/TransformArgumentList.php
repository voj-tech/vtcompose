<?php

namespace VTCompose\Xml\Xsl;

use VTCompose\Collection\Dictionary;
use VTCompose\Exception\NotSupportedException;

/**
 * 
 *
 * 
 */
class TransformArgumentList {

	private $parameters;
	private $transform;
	
	private $accessor;
	
	private function containsParameter($namespaceUri, $localName) {
		if (!$this->parameters->containsKey($namespaceUri)) {
			return false;
		}
		
		$parameters = $this->parameters[$namespaceUri];
		if (!$parameters->containsKey($localName)) {
			return false;
		}
		
		return true;
	}
	
	private function getParameters() {
		return $this->parameters;
	}
	
	/**
	 * @internal
	 */
	public function getTransform() {
		return $this->transform;
	}
	
	private function getAccessor() {
		if (!isset($this->accessor)) {
			$this->accessor = function($methodName, ...$parameters) {
				return $this->$methodName(...$parameters);
			};
		}
		
		return $this->accessor;
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param Transform 
	 */
	public function __construct(Transform $transform) {
		$transform->establishFriendshipWithArgumentList($this, $this->getAccessor());
		
		$this->parameters = new Dictionary();
		$this->transform = $transform;
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @param string 
	 */
	public function addParameter($namespaceUri, $localName, $parameter) {
		if (!$this->parameters->containsKey($namespaceUri)) {
			$this->parameters->addAt($namespaceUri, new Dictionary());
		}
		
		$this->parameters[$namespaceUri]->addAt($localName, $parameter);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return string 
	 */
	public function getParameter($namespaceUri, $localName) {
		if (!$this->containsParameter($namespaceUri, $localName)) {
			return NULL;
		}
		
		return $this->parameters[$namespaceUri][$localName];
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param string 
	 * @param string 
	 * @return string 
	 */
	public function removeParameter($namespaceUri, $localName) {
		if (!$this->containsParameter($namespaceUri, $localName)) {
			return NULL;
		}
		
		$parameters = $this->parameters[$namespaceUri];
		$parameter = $parameters[$localName];
		$parameters->removeAt($localName);
		return $parameter;
	}
	
	/**
	 * 
	 *
	 * 
	 */
	public function clear() {
		$this->parameters->clear();
	}

}

?>
