<?php

namespace VTCompose\Xml\Xsl;

use Closure;
use VTCompose\Collection\Dictionary;
use VTCompose\Collection\Set;
use VTCompose\Exception\ArgumentException;
use VTCompose\Exception\InvalidOperationException;
use VTCompose\Exception\NotSupportedException;
use VTCompose\Xml\Dom\Core\Document;
use VTCompose\Xml\Dom\Core\Implementation;
use VTCompose\Xml\Dom\Core\Node;
use XSLTProcessor;

/**
 * 
 *
 * 
 */
class Transform {

	private $xsltProcessor;
	private $parameterNames;
	private $domImplementation;
	
	private $argumentsAccessors;
	
	private static function getDomImplementationAccessors() {
		static $domImplementationAccessors = NULL;
		if ($domImplementationAccessors == NULL) {
			$domImplementationAccessors = new Dictionary();
		}
		
		return $domImplementationAccessors;
	}
	
	private static function domImplementationIsRegistered(Implementation $domImplementation) {
		return self::getDomImplementationAccessors()->containsKey($domImplementation);
	}
	
	private static function validateDomImplementationAsRegistered(Implementation $domImplementation) {
		if (!self::domImplementationIsRegistered($domImplementation)) {
			throw new InvalidOperationException('The DOM Implementation is unregistered.');
		}
	}
	
	private static function validateDomImplementationAsUnregistered(Implementation $domImplementation) {
		if (self::domImplementationIsRegistered($domImplementation)) {
			throw new InvalidOperationException('The DOM Implementation is registered.');
		}
	}
	
	private static function getDomImplementationAccessor(Node $node) {
		$domImplementation = $node->getImplementation();
		self::validateDomImplementationAsRegistered($domImplementation);
		
		$domImplementationAccessors = self::getDomImplementationAccessors();
		return $domImplementationAccessors[$domImplementation];
	}
	
	private function validateArgumentList(TransformArgumentList $arguments = NULL) {
		if ($arguments == NULL) {
			return;
		}
		
		if ($arguments->getTransform() !== $this) {
			throw new ArgumentException('The TransformArgumentList does not belong to this Transform.');
		}
	}
	
	private function loadDocumentIfNeeded($document) {
		if ($document instanceof Node) {
			return $document;
		}
		
		$filename = $document;
		$namespaceUri = '';
		$qualifiedName = '';
		$document = $this->getDomImplementation()->createDocument($namespaceUri, $qualifiedName);
		$document->load($filename);
		return $document;
	}
	
	private function addParameterName($namespaceUri, $localName) {
		if (!$this->parameterNames->containsKey($namespaceUri)) {
			$this->parameterNames->addAt($namespaceUri, new Set());
		}
		
		$this->parameterNames[$namespaceUri]->add($localName);
	}
	
	private function setParameters(TransformArgumentList $arguments = NULL) {
		foreach ($this->parameterNames as $namespaceUri => $parameterNames) {
			foreach ($parameterNames->toArray() as $localName) {
				if (!$this->xsltProcessor->removeParameter($namespaceUri, $localName)) {
					throw new TransformException('Unable to remove a parameter.');
				}
				
				$parameterNames->remove($localName);
			}
		}
		
		if ($arguments == NULL) {
			return;
		}
		
		$argumentsAccessor = $this->getArgumentsAccessors()[$arguments];
		foreach ($argumentsAccessor('getParameters') as $namespaceUri => $parameters) {
			foreach ($parameters as $localName => $parameter) {
				if (!$this->xsltProcessor->setParameter($namespaceUri, $localName, $parameter)) {
					throw new TransformException('Unable to set the value for a parameter.');
				}
				
				$this->addParameterName($namespaceUri, $localName);
			}
		}
	}
	
	private function getDomImplementation() {
		if (!isset($this->domImplementation)) {
			$this->domImplementation = new Implementation();
		}
		
		return $this->domImplementation;
	}
	
	private function getArgumentsAccessors() {
		if (!isset($this->argumentsAccessors)) {
			$this->argumentsAccessors = new Dictionary();
		}
		
		return $this->argumentsAccessors;
	}
	
	/**
	 * 
	 *
	 * 
	 */
	public function __construct() {
		$this->xsltProcessor = new XSLTProcessor();
		$this->parameterNames = new Dictionary();
	}
	
	private function __clone() {
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @throws NotSupportedException .
	 */
	public function __wakeup() {
		throw new NotSupportedException();
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @return TransformArgumentList 
	 */
	public function createArgumentList() {
		return new TransformArgumentList($this);
	}

	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @throws TransformException .
	 */
	public function load($styleSheet) {
		$styleSheet = $this->loadDocumentIfNeeded($styleSheet);
		
		$domImplementationAccessor = self::getDomImplementationAccessor($styleSheet);
		if (!$this->xsltProcessor->importStylesheet($domImplementationAccessor('getDomNode', $styleSheet))) {
			throw new TransformException('Unable to import a style sheet.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param TransformArgumentList 
	 * @return Document 
	 * @throws TransformException .
	 */
	public function transformToDocument($input, TransformArgumentList $arguments = NULL) {
		$this->validateArgumentList($arguments);
		$input = $this->loadDocumentIfNeeded($input);
		
		$domImplementationAccessor = self::getDomImplementationAccessor($input);
		
		$this->setParameters($arguments);
		
		$domDocument = $this->xsltProcessor->transformToDoc($domImplementationAccessor('getDomNode', $input));
		if (!$domDocument) {
			throw new TransformException('Unable to transform to a DOMDocument.');
		}
		
		return $domImplementationAccessor('getNode', $domDocument);
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param string 
	 * @param TransformArgumentList 
	 * @throws TransformException .
	 */
	public function transformToUri($input, $uri, TransformArgumentList $arguments = NULL) {
		$this->validateArgumentList($arguments);
		$input = $this->loadDocumentIfNeeded($input);
		
		$this->setParameters($arguments);
		
		$domNode = self::getDomImplementationAccessor($input)->__invoke('getDomNode', $input);
		if ($this->xsltProcessor->transformToURI($domNode, $uri) === false) {
			throw new TransformException('Unable to transform to URI.');
		}
	}
	
	/**
	 * 
	 *
	 * 
	 *
	 * @param mixed 
	 * @param TransformArgumentList 
	 * @return string 
	 * @throws TransformException .
	 */
	public function transformToXml($input, TransformArgumentList $arguments = NULL) {
		$this->validateArgumentList($arguments);
		$input = $this->loadDocumentIfNeeded($input);
		
		$this->setParameters($arguments);
		
		$domImplementationAccessor = self::getDomImplementationAccessor($input);
		$xmlString = $this->xsltProcessor->transformToXML($domImplementationAccessor('getDomNode', $input));
		if ($xmlString === false) {
			throw new TransformException('Unable to transform to XML.');
		}
		
		return $xmlString;
	}
	
	/**
	 * @internal
	 */
	public function establishFriendshipWithArgumentList(TransformArgumentList $arguments,
			Closure $argumentsAccessor) {
		if ($arguments->getTransform() != NULL) {
			$message = 'The TransformArgumentList is already friend with a Transform.';
			throw new InvalidOperationException($message);
		}
		
		$this->getArgumentsAccessors()->addAt($arguments, $argumentsAccessor);
	}
	
	/**
	 * @internal
	 */
	public static function registerDomImplementation(Implementation $domImplementation,
			Closure $domImplementationAccessor) {
		self::validateDomImplementationAsUnregistered($domImplementation);
		
		self::getDomImplementationAccessors()->addAt($domImplementation, $domImplementationAccessor);
	}
	
	/**
	 * @internal
	 */
	public static function unregisterDomImplementation(Implementation $domImplementation) {
		self::validateDomImplementationAsRegistered($domImplementation);
		
		self::getDomImplementationAccessors()->removeAt($domImplementation);
	}

}

?>
