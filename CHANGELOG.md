# Changelog

## [0.3.0] - 2023-09-12
### Changed
- Migrate to PHP 8.1.

## [0.2.0] - 2018-11-18
### Changed
- Migrate from PHP 5.6 to PHP 7.2.

## [0.1.2] - 2018-10-31
### Changed
- Throw the correct exception when the `VTCompose\String\String::substring()` method fails.

## [0.1.1] - 2017-12-09
### Changed
- Do not require that the PHP `mbstring.func_overload` directive is set to `3` or `7`.
- Ignore the `REDIRECT_STATUS` environment variable if it is set to `200`.
- Use HTTP instead of HTTPS in links to www.voj-tech.net in `README.md` since SSL has not been enabled on the
  website yet.

## [0.1.0] - 2017-12-03
Initial release.
